import z3

# define parameters
params = z3.Reals('a0 x00 x01 y00 y01')
a0, x00, x01, y00, y01 = params

# define probabilities
probs = z3.Reals('p000 p001 p010 p011 p100 p101 p110 p111')
p000, p001, p010, p011, p100, p101, p110, p111 = probs

# define observed margin probabilities
obs = z3.Reals('m00 m01 m10 m11')
m00, m01, m10, m11 = obs

# free parameter
prob_x_is_zero = z3.Real('prob_x_is_zero')

# constraints
param_constraints = z3.And(
    a0 >= 0, a0 <= 1,
    x00 >= 0, x00 <= 1,
    x01 >= 0, x01 <= 1,
    y00 >= 0, y00 <= 1,
    y01 >= 0, y01 <= 1,
)

prob_constraints = z3.And(
    p000 == a0 * x00 * y00,
    p001 == a0 * x00 * (1 - y00),
    p010 == a0 * (1 - x00) * y01,
    p011 == a0 * (1 - x00) * (1 - y01),
    p100 == (1 - a0) * x01 * y00,
    p101 == (1 - a0) * x01 * (1 - y00),
    p110 == (1 - a0) * (1 - x01) * y01,
    p111 == (1 - a0) * (1 - x01) * (1 - y01),
)

obs_constraints = z3.And(
    m00 == p000 + p010,
    m01 == p001 + p011,
    m10 == p100 + p110,
    m11 == p101 + p111,
)

obs_constraints = z3.And(
    m00 == a0 * x00 * y00 + a0 * (1 - x00) * y01,
    m01 == a0 * x00 * (1 - y00) + a0 * (1 - x00) * (1 - y01),
    m10 == (1 - a0) * x01 * y00 + (1 - a0) * (1 - x01) * y01,
    m11 == (1 - a0) * x01 * (1 - y00) + (1 - a0) * (1 - x01) * (1 - y01),
)

constraints = z3.And(
    param_constraints,
    # prob_constraints,
    obs_constraints,
    prob_x_is_zero == (x00 * a0) + (x01 * (1 - a0))
)

# define expression (see sec. 3.4 here: https://arxiv.org/pdf/1301.6698.pdf)
expression = z3.Exists(params, constraints)

# eliminate quantifiers
tactic = z3.Then(z3.Tactic('simplify'), z3.Tactic('qe'), z3.Tactic('simplify'))
bounds = tactic(expression)
print(bounds.as_expr())
