\documentclass[main.tex]{subfiles}

\begin{document}
	
\section{Measurement error and the LP formulation}
\label{sec:base}

Suppose that we are interested in the distribution of a discrete random
variable $X$ with support in $\mathcal{X}$, but instead of observing $X$
directly, we can only observe a discrete error-prone measurement, denoted $Y$,
with support in $\mathcal{Y}$. Without any assumptions about the measurement
distribution, $P(Y \mid X)$, we cannot say anything about the distribution of
$X$. On the opposite extreme, if $P(Y \mid X)$ is known and invertible, then
$P(X)$ is fully identifiable from observations of $Y$ \citep{rothman2008modern,
kuroki2014measurement}. Our interest is in between these two extremes, where we
assume certain properties of $P(X, Y)$, but not the whole distribution. For
example, we might assume that the measurement is more often correct than
incorrect, or that errors can only occur in one direction. We refer to these
assumptions collectively as the \textbf{measurement error model}. Our goal is
to use a measurement error model, along with observations of $Y$, to bound a
scalar function $\eta$ of the data distribution, which we refer to as the
\textbf{parameter of interest}. Common parameters of interest include marginals
of $P$ or expected values with respect to $P$, such as $\mathbb{E}[X]$.

% In the first setting we consider, we assume that there is no known
% relationship between proxy variables. To simplify the presentation in this
% setting, we first demonstrate the method assuming that there is only a single
% proxy variable $Y$. Clearly, without any assumptions about the noise
% distribution, $P(Y \mid X)$, we cannot say anything about the distribution of $X$.
% On the opposite extreme, it has been shown that if $P(Y \mid X)$ is known and
% invertible, then $P(X)$ is fully identifiable from observations of $Y$
% \cite{rothman2008modern}.

Our approach to bounding these parameters is to translate all modeling
assumptions into a set of constraints on $P(X,Y)$ and to bound the parameter of
interest by finding its maximum and minimum subject to these constraints.
Formally, let $\Delta^{d}$ be the $d$-dimensional simplex, let $\eta:
\Delta^{|\mathcal{X}|\times |\mathcal{Y}|} \to \mathbb{R}$ be the parameter of
interest, and let $\mathcal{M} \subseteq \Delta^{|\mathcal{X}| \times
|\mathcal{Y}|}$ be the set of distributions allowed under the modeling
assumptions. Then, assuming that $\mathcal{M}$ contains the true distribution
$P$, we can bound $\eta(P)$ as
%
\begin{align}
	\label{eq:bounds}
	\theta_L = \min_{\substack{Q\in\mathcal{M} \\ Q(Y)=P(Y)}} \eta(Q) \leq \eta(P) \leq \max_{\substack{Q\in\mathcal{M} \\ Q(Y)=P(Y)}} \eta(Q) = \theta_U.
\end{align}
%
The interval $[\theta_L,\theta_U]$ is referred to as the \textbf{partial
identification bounds}. If $\theta_L = \theta_U$, we say $\eta$ is
\textbf{fully identified} under model $\mathcal{M}$. Note that we explicitly
require $Q(Y) = P(Y)$ which we refer to as the \textbf{observed data
constraints}. In finite samples, this constraint will make reference to the
empirical distribution; however, we defer details on estimation to Appendix
\ref{app:estimation}. Until then, we will assume that the distribution of the
observed variables is known. Additionally, constraining $Q$ to be in
$\mathcal{M}$ enforces the constraint that $Q$ is a proper distribution (i.e.
is non-negative and sums to one) which we refer to as the \textbf{probability
constraints}.

In this work, we focus entirely on parameters $\eta$ that are linear in $P$.
This includes many common parameters such as all marginals and uncentered
moments of $P$, notably $P(X)$ and $\mathbb{E}[X]$. If, moreover, the
\emph{model} $\mathcal{M}$ can be written as a set of linear constraints, then
the upper and lower bounds in Equation \ref{eq:bounds} form linear programs
(LPs) which can be solved using any of a number of off-the-shelf LP solvers. By
the intermediate value theorem, it follows that if $\mathcal{M}$ can be
linearly expressed, the bounds obtained through these linear programs are
sharp, i.e. they exhaust the information in the observed data under the stated
assumptions. The primary aim of this work is to define and operationalize useful classes of
modeling assumptions that can be written as linear constraints and, thus, are
amenable to the LP approach.

% \subsection{Roadmap}
%
% The remainder of the paper is organized as follows: In Section
% \ref{sec:measurement_error_constraints}, we describe several simple, but common
% linear constraints directly on $P(X,Y)$, such as bounds on the error rate or
% unidirectional errors. In Section \ref{sec:iv}, we describe our main result: a
% class of instrumental variable-type models which produce linear constraints,
% allowing us to incorporate auxiliary variables, such as instrumental variables
% or additional measurements. In Section \ref{sec:causal-bounds}, we consider
% bounds on causal parameters, such as the average treatment effect (ATE), when
% treatment or outcome variables are measured with error. We show how to bound
% such parameters, incorporate common monotonicity assumptions, and adjust for
% observed confounders when estimating bounds. In Section \ref{sec:estimation},
% we show how to estimate the partial identification bounds from a finite sample
% and calculate confidence intervals for the partial identification bounds.
% Finally, in Section \ref{sec:experiments}, we apply this approach to data from
% public health and political science.
%
% In order for the eventual
% constrained optimization problem to be doubly convex, we must be able to express
% the target of inference as an \textbf{objective} that is linear in $\phi$. In
% this case, that is easy to do, as $\sum_y \phi_{xy}$.

\section{Error assumptions}
\label{sec:measurement_error_constraints}

We focus first on assumptions that directly constrain the parameters $P(X,Y)$.
Such assumptions, which we refer to as \textbf{measurement error constraints},
arise in a number of settings. For example, in settings where $X$ represents a
sensitive or stigmatized characteristic, such as drug use, and the measurement
$Y$ is obtained through participant self-report, it may be reasonable to assume
that $Y$ is never a false positive \citep{adams2019learning}. In the binary
setting, this assumption translates to the constraint $P(X=0,Y=1) = 0$ which,
importantly, is linear in $P(X,Y)$. In this section, we describe several such
constraints and give an example linear program based on a combination thereof.

For notational simplicity, let $\phi_{x,y} := P(X = x, Y = y)$ be the (unknown)
joint distribution of $X$ and $Y$. As described above, $\phi$ must satisfy the
probability and observed data constraints. That is, $\sum_{x,y} \phi_{x,y} = 1$,
$\phi_{x,y} \ge 0$ for all $x$ and $y$, and $\sum_{x} \phi_{x,y} = P(Y=y)$ for
all $y$.
% As mentioned above, we must make assumptions about the relationship of
% the unobserved variable $X$ and the observed proxy $Y$. We seek to avoid
% strong, parametric assumptions, in favor of weaker assumptions that may be
% justified by expert knowledge or domain research.
Below, we provide several useful linear measurement error
constraints\footnote{Each of these constraints can be softened by adding
a slack parameter which can be fixed or varied in a sensitivity analysis.}.

% TODO: relabel these so as not to overlap with later sections
\begin{enumerate}

  \addtolength{\itemindent}{0.5cm}
  \item[(A0)] Bounded error proportion:
    $\sum_{x \neq y} \phi_{x,y} \leq \epsilon$
  \item[(A1)] Unidirectional errors:
    $\sum_{y < x} \phi_{x,y} = 0$
  \item[(A2)] Symmetric error probabilities:\\
    $\phi_{x,y} = \phi_{x,y'}\,\,\,\forall\,\,|x - y| = |x - y'|$
  \item[(A3)] Error probabilities decrease by distance:\\
    $\phi_{x,y} \geq \phi_{x,y'}\,\,\,\forall\,\,|x - y| > |x - y'|$
  \item[(A4)] Equal expectations: $\sum_{x,y} (x - y) \phi_{x,y} = 0$
\end{enumerate}

Assumption (A0) may be reasonable when there is sufficient previous literature
to specify a range of plausible error rates, but not the exact measurement
distribution (e.g., see discussion of sensitivity analysis in
\citep{rothman2008modern}). Assumption (A1) may be used to represent positive
label only data, which is common in areas such as ecology
\citep{solymos2012conditional} and public health \citep{adams2019learning}.
Assumption (A2) represents a generalization of the zero-mean measurement error
assumption to discrete variables, commonly made in settings where the errors
are due to imprecision of the instrument. Assumption (A3) may be reasonable in
settings where the values of $X$ have a natural ordering and small errors are
more likely than large ones. Finally, (A4) applies when $Y$ represents a noisy,
but unbiased measurement of $X$. As with (A2), this may be reasonable when
errors are due to imprecision.

% Note that the target distribution $P(X=x) = \sum_{y} \phi_{x,y}$ and all
% constraints described thus far can be written as linear functions of $\phi$.
% If our parameter of interest is also a linear function of $\phi$, then the
% assumed constraints, in combination with the linear objective corresponding to
% our parameter of interest, form a linear program (LP) and bounds can be
% computed by both maximizing and minimizing the parameter of interest using one
% of a number of LP solvers. Parameters that are linear in $\phi$ include $P(X)$
% and all of its moments. By the intermediate value theorem, the objective can
% take any value between its constrained maximum and minimum values. It follows
% that for sets of assumptions that can be linearly expressed, the bounds
% obtained through this linear program are sharp, i.e. they exhaust the
% information in the observed data under the stated assumptions.


% \begin{figure}[t]
%     % \centering
% 	\begin{subfigure}[b]{0.49\textwidth}
% 			\includegraphics[width=\textwidth]{../output/figures/mrs_exp.pdf}
%     \end{subfigure}
%     \begin{subfigure}[b]{0.49\textwidth}
% 			\includegraphics[width=\textwidth]{../output/figures/mrs_norm.pdf}
% 	\end{subfigure}\hspace{1cm}
%   \caption{
%     Bounds on $E[X]$ under different measurement error assumptions and observed
%     proxy distributions, $P(Y)$.
%   }
%   \label{fig:mrs_bounds}
% \end{figure}

% \begin{framed}
% \subsection*{Application: Bounding $E[X]$ using a single proxy}
% \subsection*{Application: Bounding $E[X]$ using a single proxy}

We can now include any combination of these assumptions in the measurement
error model $\mathcal{M}$. As an example, suppose we are interested in bounding
the expected value of $X$ subject to (A0) and (A2). The resulting LP is
shown below.
%
\begin{align}
\label{eq:base}
\text{objective:~~~~~~~} \sum_{x,y} &x \phi_{x,y}\\\nonumber
\text{s.t.~~} \sum_x \phi_{x,y} &= P(Y = y) & & \forall\,\, y\\\nonumber
 \phi_{x,y} & \ge 0 & & \forall\,\,x,y\\\nonumber
 \sum_{x \neq y} \phi_{x,y} &\le \epsilon \\\nonumber
 \phi_{x,y} &\geq \phi_{x,y'} & &
                 \forall\,\, |x - y | < |x - y'| %\\\nonumber
 % \phi_{x,y} &= \phi_{x,y'} & &
                  % \forall\,\, |x - y| = |x - y'|
\end{align}
%
Note that we can easily add or remove constraints from $\mathcal{M}$, or vary
parameters like $\epsilon$, as a form of sensitivity analysis (e.g., see
Section \ref{sec:ohie}).
% to the choice of
% modeling assumptions\footnote{See \citet{imai2010treatment} or Section
% \ref{sec:ohie} for an example of this type of sensitivity analysis.}.
% Notably, in this formulation, no additional derivations are necessary to
% compute bounds under these various models.

% For two example proxy distributions, $P(Y)$, Figure \ref{fig:mrs_bounds} shows
% the resulting bounds on $E[X]$ under different combinations of (A0), (A2), and
% (A3). Bounds under each combination of assumptions were computed by simply
% solving a slightly different version of the LP in Equation \ref{eq:base},
% highlighting the ease with which we can perform sensitivity analysis without
% rederiving bounds under each new model. \hfill\qedsymbol
% \end{framed}

% As we will see in the example below, each of these assumptions can be weakened.
% Equality constraints can be converted to approximation assumptions, by
% specifying a parameter that defines how distant two quantities are permitted to
% be, and inequality constraints can be similarly amended by specifying a
% parameter that defines a unidirectional limit to violation of the inequality.
% These additional parameters can be varied as sensitivity parameters, when the
% sensitivity of inference to these assumptions should be examined.
Were multiple measurements $\mathbf{Y} \equiv \{Y_1,...,Y_K\}$ observed with no
assumptions made about the relationship between them, then the observed data
constraint would be expressed on $P(\mathbf{Y})$ and the target parameter would
be expressed as a linear function of the full distribution $P({X, \bf Y})$.
Each $Y_k$ would potentially be subject to its own measurement error
constraints, depending on what knowledge is available about the measurement
process. These constraints would be expressed on the marginals $P(X,Y_k)$,
which maintains linearity. In the following section, we consider scenarios
where auxiliary variables, such as instrumental variables, are also available.

% \subsection{Extending the LP approach to other parameters and models}
%
% In the remainder of this paper, we show how the LP approach described in this
% section can be extended to bound other parameters of interest and incorporate
% other modeling assumptions. In the Section \ref{sec:iv}, we show how to
% incorporate conditional independence assumptions, encoded in a graphical model,
% relating $X$ to other observed and latent variables beyond $Y$. We define a
% class of latent variable Bayesian networks which produce linear constraints and
% show how to relax the constraints imposed by models not in this class to
% produce valid outer bounds. In Section \ref{sec:causal-bounds}, we consider
% bounds on parameters of the distribution of $X$ under an intervention $A$. In
% this setting, our interest is in the potential outcome variable $X(a)$, defined
% as the value $X$ would have taken had we intervened to set $A=a$. Our goal,
% then, is to bound parameters of the distribution $P(X(a))$, including the
% average treatment effect of $A$ on $X$. We show how to bound such parameters
% and incorporate additional assumptions relating potential outcomes under
% different interventions.

\end{document}
