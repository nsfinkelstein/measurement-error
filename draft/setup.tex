\documentclass[main.tex]{subfiles}

\begin{document}

\section{The Constrained Optimization Approach}
\label{sec:setup}

Let $X \in \mathcal{X}$ be a discrete random variable of interest that cannot be
directly observed, and let $\bf Y \in \mathcal{Y}$ be the set of observed
discrete random variables that are not \textit{a prior} known to contain no
information on $X$. Variables in $\bf Y$ can be thought of as proxies for $X$,
in the sense that they contain information about its distribution. In
traditional approaches to the measurement error problem, there is only one such
proxy. It is assumed to have the same cardinality as $X$, and is thought to have
been observed specifically for the purposes of obtaining information about $X$.

In this section, we describe a constrained optimization view of extracting
information about $X$ from $\bf Y$, and discuss conditions under which the
bounds so obtained are sharp.

\subsection{Formulating the Optimization Problem}

We let $\phi_{x{\bf y}}$ denote the parameters of the joint distribution $P(X,
{\bf Y})$, such that $\phi_{x{\bf y}} =P(X = x, {\bf Y = y})$. The targets of
inference we consider can be expressed as functionals of $\phi$, denoted
$c(\phi)$. We note that any query about the marginal density of the unobserved
$X$ can be so expressed
because $P(X = x) = \sum_{\bf y}\phi_{x{\bf y}}$. Likewise, the assumptions we
consider can be expressed as constraints on functionals of $\phi$, denoted as
${\bf g}(\phi) \le 0$ or ${\bf h}(\phi) = 0$, depending on the nature of the
constraints. The feasible region of the target of inference can then be obtained
by finding all values of $c(\phi)$ such that $\phi$ satisfies the constraints.

If $c(\cdot)$ and ${\bf h}(\cdot)$ are linear, and ${\bf g}(\cdot)$  are convex,
then both maximizing and minimizing $c(\phi)$ subject to ${\bf g}(\phi) \le 0$
and ${\bf h}(\phi) = 0$ are convex optimization problems. For this reason, we
refer to such problems as \textit{doubly convex}. In this work, we focus
primarily on linear programs, which are convex optimization problems in which
${\bf h}(\cdot)$, ${\bf g}(\cdot)$ and $c(\cdot)$ are all linear. These are
arguably the most tractable form of constrained optimization and, as described
below, are in some cases amenable to symbolic solution. Problems of this form
are sufficiently expressive to obtain sharp bounds for a wide range of modeling
assumptions, and can be used to obtain outer bounds under any set of
assumptions.

We explore five categories of constraints on functionals of $\phi$. In Section
\ref{sec:base}, we introduce constraints imposed by \textbf{probability theory}
and the \textbf{observed data}, which are required for $\phi$ to be
interpretable as the parameters of the relevant joint distribution, as well as
constraint imposed by assumptions about the form of \textbf{measurement error}.
In Section \ref{sec:iv}, we consider distributions modeled by hidden variable
Bayesian networks, and the \textbf{graphical} constraints they impose on the
model parameters. Finally, in Section \ref{sec:causal-bounds}, we consider
\textbf{causal} constraints imposed by common assumptions relating potential
outcomes under different interventions.

\subsection{Sharpness of the bounds}

In doubly convex problems, it follows immediately from the intermediate value
theorem and the linearity of $c(\cdot)$ that $c(\phi)$ can take any value
between its constrained maximum and minimum values. These constrained optima can
both be obtained using any of a number of well-known convex optimization
algorithms. It follows that in such settings the feasible region of the target
can be sharply bounded by solving these constrained optimization problems. It is
possible to obtain sharp bounds even when the objective is not linear and the
model is not convex, as is demonstrated in Section \ref{sec:markov-chain},
but problems of this nature are in general more difficult to address.

Throughout this work, we examine targets of inference and modeling assumptions
that correspond exactly to doubly convex optimiztaion problems, which can be
used to obtain sharp bounds. We also illustrate that when the assumed model is
not itself convex, it is still always possible to formulate a doubly convex
optimization problem to obtain outer bounds.

\subsection{Symbolic Bounds for Certain Linear Programs}
\label{sec:symbolic_bounds}

When the constrained optimization problem corresponds to a linear program, then
without loss of generality, the objective may be expressed as $c^t \phi$, and
the constraints as $A\phi \ge b$. Depending on the assumptions made, elements of
$A$, $b$, and/or $c$ may be functionals of the observed data distribution. In
such cases, it is always possible to obtain bounds corresponding to a particular
data distribution by numerically optimizing the linear objective for that
distribution (e.g. using the simplex method).

However, in some settings it will be desireable to produce symbolic bounds -
that is, bounds that are simple functionals of the observed data distribution -
which obviate the need for practitioners to specify and solve linear programs.
The widely-used bounds on the average treatment effect in the instrumental
variable model show the utility of such symbolic solutions. In cases where
elements of either $c$ or $b$ may be functionals of the observed data
distribution, as in Section \ref{sec:base} and \cite{balke93ivbounds}, a vertex
enumeration algorithm may be used on the primal or dual program, respectively,
to produce symbolic bounds on the objective. However, if $A$, or both $c$ and
$b$, are functionals of the observed data, then there is no approach known to
the authors for obtaining symbolic bounds.

\end{document}
