\documentclass[main.tex]{subfiles}

\begin{document}

\section{Auxiliary variables}
\label{sec:iv}

In the previous section we relied on domain knowledge about the distribution
$P(X,Y)$ to partially identify the target parameter. In certain cases, we may
have auxiliary variables, such as additional measurements or sources of
variation in the measurement process, that give additional information about
$P(X,Y)$ \citep{carroll2006measurement}. In this section, we show how the
linear programming approach can be used to incorporate one important type of
auxiliary variable known as an \textbf{instrumental variable} (IV). We begin by
demonstrating how the LP approach can be used to compute bounds for the classic
IV model with measurement error, and then we generalize this to a broader class
of IV-type models, allowing us to incorporate other types of auxiliary
variables.

\subsection{The classic IV model}
\label{sec:classic-iv}

In the classic IV model\footnote{Several variations of the classic IV model
have been proposed; however, we refer to the version used in
\citep{balke93ivbounds}. For a review of others, see
\citet{swanson2018partial}.}, shown as a Bayesian network in Figure
\ref{fig:iv}, the observed variable $A$ is referred to as an instrument for the
relationship between $X$ and $Y$ which is, in turn, confounded by the
unobserved variable $\Lambda$ \citep{balke93ivbounds}. Like $X$ and $Y$, we
will assume that $A$ is discrete. For example, if we believe that the gender of
a study participant may only affect $Y$ through $X$, then participant gender
may be a valid IV.

% As shown by \citet{balke93ivbounds} for binary variables and
% \citet{bonet2001instrumentality} for general discrete variables, the classic IV
% model places a set of linear inequality constraints on the joint distribution
% $P(X,Y \mid A)$. Assuming, that our target parameter is linear in $\phi =
% P(A,X,Y)$ and that $P(A,Y)$ is known, then these constraints can be
% incorporated into the linear programming approach.

% Unfortunately, the exact form of these linear constraints is known only for a
% few special cases, most notably, when all variables are binary
% \citep{balke93ivbounds,bonet2001instrumentality,swanson2018partial}.
Previous work \citep{fine, balke93ivbounds, bonet2001instrumentality,
swanson2018partial} has shown that the constraints imposed by this model on the
conditional distribution $P(X, Y \mid A)$ are linear. Thus, for linear
parameters of $P(A,X,Y)$, we are able include the classic IV model as part of
$\mathcal{M}$. To express these constraints \emph{explicitly} as part of an LP,
we will rely on \textbf{potential outcomes}.
% To construct a linear constraint set for the general discrete variable setting,
% we will instead use the response function approach described in
% \citet{bonet2001instrumentality}, but will denote the response function
% variables using potential outcomes.
The potential outcome variable $X(a)$ represents the value $X$ would have taken
had we intervened to set $A = a$ \citep{rubin2005causal}. Let $\tilde{X} =
\{X(a) : a \in \mathcal{A}\}$ represent the set of potential outcome variables
for $X$ under different interventions on its parent $A$ and let $\tilde{Y} =
\{Y(x): x \in \mathcal{X}\}$ be similarly defined. The variable sets
$\tilde{X}$ and $\tilde{Y}$ are referred to as \textbf{response function
variables} \citep{balke1997thesis} since $\tilde{X}$ can be thought of as a
random function mapping values of $A$ to values of $X$. Finally, let $\psi$ be
the joint distribution over $\tilde{X}$ and $\tilde{Y}$ such that
$\psi_{\tilde{x},\tilde{y}} = P(\tilde{X}=\tilde{x},\tilde{Y}=\tilde{y})$.
% For an explicit example of this marginalization, see Section \ref{app:iv_lp}
% in the Supplement.
As observed in \citet{balke93ivbounds} and \citet{bonet2001instrumentality},
all independencies in the classic IV model are now given by $A \perp \tilde{X},
\tilde{Y}$ which can be written
%
\begin{align}
  \label{eq:iv-indep}
P(A=a,\tilde{X}=\tilde{x},\tilde{Y}=\tilde{y}) = P(A=a)\psi_{\tilde{x},\tilde{y}}.
\end{align}
%
% Since $P(A)$ is assumed to be known, we can enforce this constraint while
% maintaining linearity by simply using the RHS in all expressions.
Under the causal consistency assumption \citep{pearl2009causality}, which can
be concisely phrased as $A = a \land X(a) = x \implies X = x$, and the
independence assumption in Equation (\ref{eq:iv-indep}), $\psi$ is connected to
the conditional distribution $P(X, Y \mid A)$ by the linear map
\begin{align}
  \label{eq:iv-consistency}
P(X=x, Y=y \mid A=a) &= P(X(a) = x, Y(x) = y)\\\nonumber
&= \sum_{\tilde{x},\tilde{y}} \mathbb{I}(\tilde{y}(x) = y, \tilde{x}(a) = x)\psi_{\tilde{x},\tilde{y}}.
\end{align}
% where the RHS is a marginal of $\psi$.
% As the classic IV model imposes no other constraints on $\psi$, and
% marginalization is a linear operation, all constraints imposed by the model on
% $\psi$, and by extension on the target parameter, are linear.
% They can be expressed in terms of $\psi$ as
% \begin{align*}
% \sum_{\tilde{x},\tilde{y}} \mathbb{I}(y(x) = y, x(a) = x)\psi_{\tilde{x},\tilde{y}} = P(Y = y, X = x \mid A=a).
% \end{align*}
We can now enforce all constraints of the classic IV model by replacing
$P(A,X,Y)$ with Equation \ref{eq:iv-consistency} in the target parameter $\eta$
and the observed data constraints $P(A,Y) = \sum_x P(A,X=x,Y)$, which maintains
linearity in $\psi$. Additionally, all measurement error constraints, which are
expressed on the marginal $P(X,Y) = \sum_a P(A=a,X,Y)$, can now be expressed as
linear constraints on $\psi$ by substituting $\psi$ into these constraints
according to Equation \ref{eq:iv-consistency}.
% When $X$ is \emph{not observed}, it can be marginalized out of these
% constraints by summing over values of $x \in \mathcal{X}$, which maintains
% linearity in $\psi$. The resulting linear equality constraints connect the
% unknown parameters of $\psi$ to the known observed data distribution,
% $P(Y,A)$.
% These constraints can now be used to bound linear functionals of the
% distribution $P(A,X,Y)$, as in LP (\ref{eq:base}).
As an example, suppose $\mathcal{M}$ combines the classic IV model with
Assumption (A0). Then we can obtain bounds on $\mathbb{E}[X]$ using the
following LP:
% We can now proceed exactly as in Section
% \ref{sec:measurement_error_constraints}, optimizing with respect to $\psi$
% rather than $\phi$.
% Alternatively, the measurement error constraints can be
% enforced on the potential outcome variables under different interventions
% (e.g., constrain $P(X(a),Y(a))$ for all $a$), representing a belief that these
% constraints hold in the observed data as well as under various interventions.
% As an example, the following linear program returns bounds for $\mathbb{E}[X]$
% under the classic IV model:
%
\begin{align}
\label{eq:base}
\text{objective:~} &\sum_{a,\tilde{x},\tilde{y}} x(a) P(A=a)\psi_{\tilde{x},\tilde{y}}\\\nonumber
\text{s.t.~} &\sum_{x,\tilde{x},\tilde{y}} \mathbb{I}(\tilde{y}(x) = y,\tilde{x}(a)=x)\psi_{\tilde{x},\tilde{y}} = P(Y = y \mid A=a)\\\nonumber
 &\sum_{x \neq y,a,\tilde{x},\tilde{y}} \mathbb{I}(\tilde{y}(x) = y,\tilde{x}(a)=x) \psi_{\tilde{x},\tilde{y}}P(A=a) \le \epsilon \\\nonumber
 &\psi_{\tilde{x},\tilde{y}} \ge 0.
\end{align}
%
% Additionally, since $\phi=P(X,Y)$ is linearly related to $\psi$, the
% measurement error constraints from the previous section relating $X$ to $Y$ are
% still linear in $\psi$ and can be combined with the constraints imposed by the
% classic IV model.

While the classic IV model has proven useful in a wide range of applications,
there remain many useful auxiliary variables that are not covered by this
model. For example, in survey settings, the question order or perceived gender
of the surveyor can affect the observed responses, but both are independent of
the true value \citep{catania1996effects,huddy1997gender}. Question order and
surveyor gender are not classic IVs, but they can still provide information
about the relationship between $X$ and $Y$. We now generalize the linearity
result for the classical IV model to a class of IV-type models that includes
this setting, as well as several others.

% A common alternative is to use auxiliary variables to we describe how
% assumptions encoded in a graphical model can be used to further constrain our
% target parameter. In particular, we describe a class of graphs that result in
% linear constraints on the target parameter and, thus, is amenable to the linear
% programming approach introduced in the previous section. This class includes
% the common instrumental variable (IV) model, shown in Figure \ref{fig:iv} (a),
% as well as the various extensions of this model shown in Figure
% \ref{fig:gen_iv}.
%
% Suppose that we assume a latent variable Bayesian network $\mathcal{G} =
% (\mathbf{V},\mathbf{E})$, where $\mathbf V$ and $\mathbf E$ represent the
% vertices and edges of the network, respectively. For a variable $V \in
% \mathbf{V}$, let $Pa_\mathcal{G}(V)$ be the parents of $V$ in $\mathcal{G}$
% and $Ch_\mathcal{G}(V)$ be the children of $V$ in $\mathcal{G}$. Additionally,
% we refer to the set of observed and unobserved variables with known cardinality
% as \textbf{endogenous variables}, denoted by $\mathbf{O}$, and the set of
% unobserved variables with unknown cardinality (typically latent confounders) as
% \textbf{exogenous variables}, denoted by $\mathbf{U}$. For example, consider
% the Bayesian network in Figure \ref{fig:iv} (a) in which $A$, $X$, and $Y$ are
% endogenous and $U$ is exogenous. In this model $A$ is commonly referred to as
% an \emph{instrumental variable} (IV), and the model is referred to as the IV
% model. The independencies encoded in this graph, namely $A \perp U$ and $A
% \perp Y \mid A,U$, place constraints on the joint distribution $P(A,U,X,Y)$
% which, in turn, places constraints on the target parameter. We refer to the
% target parameter given by a graphical model as \textbf{graphical constraints}. As before, our goal is to maximize and minimize the target parameter $\psi$ subject to these constraints.
%
% In general, the independence constraints imposed by a Bayesian network on the
% joint distribution of variables in the graph are non-linear. For example, the
% simple Markov chain model shown in Figure \ref{fig:iv} (b) yields quadratic
% constraints on $P(A,X,Y)$ (see Proposition \ref{prop:binary_bounds} for more
% details). This makes optimizing over the constraint set difficult as, in
% general, quadratic programming is NP-hard \citep{pardalos1991quadratic}.
% Further, the complete set of latent variable graphical models that impose
% linear constraints on $P$ is unknown; however, in the remainder of this
% section, we describe a class of graphs, including commonly used graphs such as
% the IV model, that do yield linear constraints. We will proceed by first illustrating how the basic IV model produces linear constraints on $P$ and then generalizing this result to a class of graphs using results from \citep{fine} and \citep{evans}.
%
% \subsection{Constructing linear constraints from the IV model}
% To construct this class, we start by considering the IV model shown in Figure
% \ref{fig:iv} (a) which is known to place linear constraints on $P$
% \citep{balke93ivbounds,bonet2001instrumentality}. In order to arrive at linear
% constraints, we will not optimize directly over the joint distribution $\phi =
% P(A,U,X,Y)$ as we did in Section \ref{sec:base}. Instead, we will optimize over
% an equivalent potential outcome distribution. Recall that a potential outcome
% variable $X(a)$ represents the value $X$ would have taken had we intervened to
% set $A = a$. Then, let $\tilde{X} = (X(a))_{a \in \mathcal{A}}$ and $\tilde{Y}
% = (Y(x))_{x \in \mathcal{X}}$ be the vectors of potential outcome variables for
% $X$ and $Y$ given their endogenous parents. Finally, let $\psi$ be the joint
% distribution over $\tilde{X}$, and $\tilde{Y}$ such that
% $\psi_{\tilde{x},\tilde{y}} = P(\tilde{X}=\tilde{x},\tilde{Y}=\tilde{y})$.
%
% Under the consistency assumption, $\psi$ is connected to the distribution over
% endogenous variables by the linear map $P(x, y \mid a) = P(X(a) = x, Y(x) =
% y)$, where the last term is obtained by marginalizing all other variables in
% $\tilde{X}$ and $\tilde{Y}$ out of the distribution $\psi$. For an explicit
% example of this marginalization, see Section \ref{app:iv_lp} in the
% Supplement. As observed in \citep{bonet2001instrumentality} and
% \citep{balke93ivbounds}, all independencies in IV graph are now given by $A
% \perp \tilde{X}, \tilde{Y}$ which can be written as
% %
% \begin{align}
% \psi_{a,\tilde{x},\tilde{y}} = P(A=a)\sum_{a'} \psi_{a',\tilde{x},\tilde{y}}\,\,\,\forall\,a,\tilde{x},\tilde{y},
% \end{align}
% %
% which is linear in $\psi$ since $P(A)$ is identified from the data. As
% $\mathcal{G}$ imposes no other constraints on $\psi$, and marginalization is a
% linear operation, all constraints on the conditional distribution $P(x, y \mid
% a)$ are similarly linear.

%TODO: IV-type is not well defined
\subsection{General IV models}

\begin{figure}[t]
  \centering {\small
    \include{iv-graph}
  }
  \caption{ (a) represents the classic IV model, (b) - (e) represent general IV
    models, and (f) represents a simple model not covered
    by Definition 1. In all graphs, black variables represent observed variables
    and blue variables represent unobserved variables.} 
  \label{fig:iv}
\end{figure}

Before defining our general class of IV-type models, we establish some notation:
Let $\mathcal{G} = (\mathbf{V},\mathbf{E})$ be a latent variable Bayesian
network where $\mathbf{V}$ and $\mathbf{E}$ represent the vertices and edges of
the network, respectively. For a variable $V \in \mathbf{V}$, let
$Pa(V)$ be the parents of $V$ in $\mathcal{G}$ and
$Ch(V)$ be the children of $V$ in $\mathcal{G}$. Equipped with this
notation, we define the following class of general IV models:

\theoremstyle{definition}
\begin{definition}[General IV model]
	\label{def:gen_iv}
	%
	The latent variable Bayesian network $\mathcal{G} =
(\mathbf{V},\mathbf{E})$ is a \textbf{general IV model} if there exists an
unobserved variable $\Lambda \in \mathbf{V}$ such that: (1) all descendants of
$\Lambda$, denoted $\mathbf{B}$, are children of $\Lambda$, (2) all \emph{observed} non-descendants of
$\Lambda$, denoted $\mathbf{A}$, have at most one child and that child is in
$\mathbf{B}$, and (3) all \emph{unobserved} non-descendants of $\Lambda$ are
parents of exactly one variable in $\mathbf{B}$, denoted $B$, and one variable in $\mathbf{A}$, denoted $A$ and, if $A$ has a child, it must be $B$.
	%
\end{definition}
%
\begin{figure}[t]
    \centering
	% \begin{subfigure}[b]{0.49\textwidth}
	\includegraphics[width=.8\columnwidth]{../output/figures/general_iv_model_diagram.png}
    % \end{subfigure}
  \caption{
    Illustration of a general IV model. Unobserved variables are shown in blue. The variables in $\mathcal{B}$ must all be children of $\Lambda$. The variables in $\mathbf{A}$ are referred to as instruments, must be observed, and cannot be children of $\Lambda$.
  }
  \label{fig:gen_iv}
\end{figure}
%
In such graphs, let $\mathbf{B}$ be the children of $\Lambda$ and let
$\mathbf{A}$ be the observed non-descendants of $\Lambda$, which we refer to as
the \textbf{instruments}. An illustration of a general IV model is shown in
Figure \ref{fig:gen_iv}. This class of graphs trivially includes the classic IV
model in Figure \ref{fig:iv} (a) with $\mathbf{B} = \{X,Y\}$ and $\mathbf{A} =
\{A\}$, but also extends the classic IV model in several important ways. First,
we can now include instruments for $Y$ as shown in Figure \ref{fig:iv} (b). As
described above, this may occur when some aspect of the measurement process is
randomized, such as the order of responses in a survey or the gender of an
in-person surveyor (e.g., see \citep{catania1996effects,huddy1997gender}).
Second, we can include multiple independent instruments as shown in Figure
\ref{fig:iv} (c) (e.g., see \citep{angrist1991does,poderini2019exclusivity}).
Finally, this extends the classic IV model to allow for instruments in
$\mathbf{A}$ to be confounded with elements of $\mathbf{B}$. This includes
graphical models such as those shown in Figures \ref{fig:iv} (d) and (e). In
particular, Figure \ref{fig:iv} (e) can be used to represent a model where $A$
is a proxy for the true unobserved instrument $U$.

By Proposition 5 in \citet{evans2016margins}, for any general IV graph $\mathcal
G$, it is possible to find a new graph $\mathcal G'$ that represents the same
model over $\{\bf A, B\}$ such that every instrument in $\mathcal G'$ is
randomized, according to the following procedure:
%
\begin{procedure}[Randomize Instruments]
  \label{proc:randomize}
  %
  Remove each unobserved variable with children $A \in \bf A$ and $B \in
  \bf B$, and make $A$ a parent of $B$ (if it is not already).
  %
\end{procedure}

The new graph $\mathcal G'$ is more convenient to reason about, as the
distribution of $P({\bf B(a)})$ is identified as $P({\bf B \mid A = a})$. Since
we are interested in parameters of $P(\mathbf{A},\mathbf{B})$, we can without
loss of generality reason about the modified graph $\mathcal G'$ rather than
about the original graph $\mathcal G$. In the following section, we will see
that differences between $\mathcal G$ and $\mathcal G'$ become relevant when we
consider causal parameters.

% \begin{proposition}
% \end{proposition}

% As demonstrated with the classic IV model in Section \ref{sec:classic-iv}, we
% can place the true variables of interest, denoted $\bf B_U$, on the graph as
% hidden variables in $\bf B$, and include their proxies as observed random variables
% in $\bf B$.
We now give our main result, which allows us to use LPs for partial
identification in general IV models with measurement error. Let $\mathbf{B_O}
\subseteq \mathbf{B}$ and $\mathbf{B_U} \subseteq \mathbf{B}$ be the set of
observed and unobserved variables in $\mathbf{B}$, respectively. Then we have
the following proposition
% andenote all observed variables in $\bf B$ as $\bf B_O$ and assume
% that all variables in $\bf A$ and $\bf B$ have known bounded cardinality.
% The following proposition licenses the use of linear programs for partial
% identification in general IV models with measurement error.

% We now give our main result which relies heavily on results from \citet{fine}
% and \citet{evans2016margins} and which we prove in the supplemental material.
%
\begin{proposition}\label{prop:general_iv}
	%
	For any graph $\mathcal{G}$ in the class of general IV graphs, the
constraints imposed by the observed data $P({\bf A, B_O})$ and the model
$\mathcal G$ on $P(\mathbf{A},\mathbf{B})$ can be represented linearly.
  % 
\end{proposition}
% 
% In such a graph, all variables in $\bf X$ are mutually confounded by $\Lambda$
% and we refer to the variables in $\bf A$ as \textbf{instruments}. Note that
% this class of graphs trivially includes the classic IV model (Figure
% \ref{fig:iv} (a)) but also extends the classic IV model in several important
% ways. First, we can now include multiple instruments as shown in Figure
% \ref{fig:gen_iv} (a) (e.g., see
% \citep{angrist1991does,poderini2019exclusivity}). Second, we can include
% instruments for both $X$ and its proxy $Y$ as shown in Figure \ref{fig:gen_iv}
% (b). As described above, this may occur when some aspect of the measurement
% process is randomized, such as the order of responses in a survey or the gender
% of an in-person surveyor (e.g., see
% \citep{becker1995effect,catania1996effects}). Finally, instruments in
% $\mathbf{A}$ may be confounded with their children and, if an instrument is
% confounded with its child, it need not have a directed edge to that child. This
% includes graphical models such as those shown in Figures \ref{fig:gen_iv} (c)
% and (d). In particular, Figure \ref{fig:gen_iv} (d) can be used to represent a
% model where $A$ is a proxy for the true unobserved instrument.

% We can write
% distributions in this model as $P({\bf X} \mid {\bf A})P({\bf A})$. In our
% settings, $P({\bf A})$ is assumed to be known since $\bf A$ is observed, so
% constraints on the joint distribution of instruments imposed by the model will
% not play a role in bounding our targets of inference. It follows that all
% relevant constraints imposed by the model are linear.
%
% Examples of graphs in this class include the IV model in Figure \ref{fig:iv}
% (a), as well as the models shown in Figures \ref{fig:gen_iv} (a) and (b). In
% particular, the model shown in Figure \ref{fig:gen_iv} (a) represents a
% scenario where we have separate instruments, $A$ and $B$, for the true
% measurement $X$ and the noisy proxy $Y$.
% 
Recalling that bounds can be obtained by minimizing and maximizing a linear
functional of $P(\mathbf{A},\mathbf{B})$ over constraints imposed by the
observed data and the model, this leads directly to the corollary:
%
\begin{corollary} 
%
	For any model $\mathcal{M}$ comprised of a discrete general IV model
$\mathcal G$ and any number of linear measurement error constraints, sharp
bounds may be obtained for any linear function of $P(\mathbf{A},\mathbf{B})$ by
solving an LP.
%
\end{corollary}
% The consequence of Proposition \ref{prop:general_iv} is that the linear
% programming approach can be used to calculate partial identification bounds
% under all general IV models.

To explicitly express the set of linear constraints imposed by a general IV
model, we can generalize the response function approach described for the
classic IV model. For each variable $B \in \mathbf{B}$, we will define the set
of potential outcomes of $B$ under different joint settings of its parents in
$\mathbf{A} \cup \mathbf{B}$ -- i.e., the response function variable for $B$.
Formally, for each variable $B \in \mathbf{B}$, let $\tilde{B} = \{B(p): p \in
\text{support}(Pa(B) \cap (\mathbf{A}\cup\mathbf{B}))\}$ where
$\text{support}(\mathbf{X})$ is the joint support of variables in $\mathbf{X}$.
Further, let $\mathbf{\tilde{B}} = \{\tilde{B}: B \in \mathbf{B}\}$ be the
collection of all $\tilde{B}$.
% Let $\bf \tilde{X}$ denote the potential outcomes of the children of
% $\Lambda$, $\bf X$, under every setting of the instruments $\bf A$.
As before, let $\psi_{\bf \tilde b} = P(\bf \tilde B = \bf \tilde b)$ denote
the joint distribution over all such potential outcome variables. Because
$P(\mathbf{A})$ is assumed to be known, the only relevant independency imposed
by the graph is given by ${\bf A} \perp {\bf \tilde B}$. This is a
generalization of the independence constraint captured in Equation
(\ref{eq:iv-indep}), and can be written
%
\begin{align}
  \label{eq:general-iv-indep}
	P(\bf A = \bf a,\bf \tilde B = \bf \tilde b) =
  P(\bf A = \bf a)\psi_{\bf \tilde b} \quad \forall\,\bf a, \bf \tilde b.
\end{align}
%
% In the classic IV case, Equation (\ref{eq:iv-consistency}) constrains the
% parameters of $\psi$ by setting a marginal of $\psi$ equal to a conditional in
% the observed data distribution.
Finally, we must linearly link $\psi$ to the observed full data distribution
$P(\mathbf{A},\mathbf{B})$. Because we assume that all instruments have been
randomized according to Procedure \ref{proc:randomize} and by the causal
consistency assumption, we have $P({\bf B(a)}) = P({\bf B \mid A = a})$ which
links the full data distribution to a potential outcome distribution. This
potential outcome distribution, $P(\mathbf{B(a)})$ is, in turn, a linear
function of $\psi$.
% equality imposes a linear constraint on $\psi$, as $P({\bf B(a)})$ is a
% marginal of $\psi$ in the sense that it is a sum over the parameters of $\psi$.
% However, it is not a marginal distribution in the traditional sense of being
% the distribution of a subset of the random variables over which $\psi$ is the
% joint distribution. Recall that $\psi$ is the distribution over $\bf \tilde B$.
% For $B \in \bf B$ that has parents not in $\bf A$, $B({\bf a}) \not\in \bf
% \tilde B$. As a result, $\bf B({\bf a})$ must be linked to $\bf \tilde B$
% through consistency. 
In the classic IV model, this is link is relatively transparent as demonstrated
in Equation \ref{eq:iv-consistency}. We will use the following proposition to
generalize Equation \ref{eq:iv-consistency} to general IV models.

% \begin{proposition}
%   \label{prop:marginalization}
%   Consider a set of outcome variables $\bf Z \subseteq B$ and a set of
%   treatment variables $\bf T \subseteq \{A, B\} \setminus Z$. Then
% %
% {\small
% \begin{align}
%   \label{eq:general-marginalization}
%   &P({\bf Z(t) = z})\\&\nonumber\quad=
%   \sum_{{\bf a, \tilde b}}
%   \mathbb{I}[g_Z({\bf\tilde b, \{T, A\} =\{t, a\}}) = z \mid z \in {\bf z}] \psi_{\bf \tilde b}P(\mathbf{A} = {\bf a})
% \end{align}
% }
% where $g_Z({\bf \tilde b, I = i})$ is defined as
% {\small
% ~~\begin{align*}
%   &g_V({\bf v, I = i}) =\\
%   &\begin{cases}
%     {{\bf \tilde b}_{Z({\bf i}_{pa(Z)})}} & \text{if~} pa(Z) \subseteq {\bf I}\\
%     g_Z({\bf \tilde b},
%     xch({\bf I}) =
%     \{g_J({\bf \tilde b, I = i}) \mid J \in xch({\bf I})\}) & \text{otherwise},
%   \end{cases}
% \end{align*}
% }
% ${\bf x}_{\bf Y}$ indicates the values assigned to $\bf Y$ in ${\bf x}$, and
% $xch({\bf I}) \equiv {\bf I} \cup \big(\bigcap_{I \in {\bf I}} ch(I)\big)$.
% \end{proposition}

\begin{proposition}
  \label{prop:marginalization}
  Consider a set of outcome variables $\bf Z \subseteq B$ and a set of
  treatment variables $\bf T \subseteq \{A, B\} \setminus Z$ in a general IV
  model with randomized instruments. Then
%
\begin{align}
  \label{eq:general-marginalization}
  &P({\bf Z(t) = z})\\&\nonumber\quad=
  \sum_{\mathbf{v}:\mathbf{v}_\mathbf{Z} = \mathbf{z}}
  P(\mathbf{A} = \mathbf{v_A})
  P\left(B(\mathbf{t}_{Pa(B)},\mathbf{v}_{Pa(B) \setminus \mathbf{T}})=\mathbf{v}_B : B\in\mathbf{B}\right)
\end{align}
%
where for the set of values $\mathbf{v}$ and set of variables $\mathbf{S}$, $\mathbf{v}_\mathbf{S}$ is the values of $\mathbf{S}$ in $\mathbf{v}$ and 
%
\begin{align}
	&P\left(B(\mathbf{t}_{Pa(B)},\mathbf{v}_{Pa(B) \setminus \mathbf{T}})=\mathbf{v}_B : B\in\mathbf{B}\right)\\\nonumber
	&\quad= \sum_{\mathbf{\tilde{b}}} \psi_{\mathbf{\tilde{b}}} \prod_{B \in \mathbf{B}} \mathbb{I}\left(\tilde{b}(\mathbf{t}_{Pa(B)},\mathbf{v}_{Pa(B) \setminus \mathbf{T}})=\mathbf{v}_B\right).
\end{align}
\end{proposition} 
%
Using this proposition, we can linearly map $\psi$ to the full data
distribution according to
%
\begin{align*}
	% \label{eq:gen-iv-consistency}
  P({\bf B = b} \mid {\bf A = a}) = 
  \sum_{\mathbf{\tilde{b}}} \psi_{\mathbf{\tilde{b}}} \prod_{B \in \mathbf{B}} \mathbb{I}\left(\tilde{b}(\mathbf{a}_{Pa(B)},\mathbf{b}_{Pa(B) \setminus \mathbf{A}})=\mathbf{b}_B\right).
\end{align*}
%
As in the classic IV case, we can enforce the constraints of the general IV
model by substituting $\psi$ for $P(\mathbf{A},\mathbf{B})$ in the target
parameter, observed data constraints, and measurement error constraints
according to this equation.
% linear constraints on $\psi$ in terms of
% the observed data distribution $P({\bf B_O \mid A})$ may be obtained by summing
% equality constraints in which the observed variables all take the same
% values, i.e. $\bf A = a$ and $\bf B_O = b_o$.
% which we again enforce implicitely.
% Finally, under the consistency assumption, $\psi$ can be linearly mapped to
% $\phi = P(\mathbf{A},\mathbf{B})$ via $P(\mathbf{A} = \mathbf{a},
% \mathbf{B}=\mathbf{b}) = P(\mathbf{A} =
% \mathbf{a})P(\mathbf{\tilde{B}}(\mathbf{a},\mathbf{b}) = \mathbf{b})$. The RHS
% is again a marginal of $\psi$ and thus the general IV model imposes linear
% constraints on $\phi$. As described for the classic IV model, constraints in the
% presence of unobserved members of $\bf B$ can be obtained by summing all the
% constraints in which the observed variables take the same values, yielding
% constraints on .
% we can now write the
% partial identification bounds for our target parameter as solutions to a linear
% program that optimizes with respect to $\psi$.
For the purposes of illustration, Appendix \ref{app:lps} contains linear programs for the
models shown in Figures \ref{fig:iv} (b) - (e).

In Appendix \ref{sec:nonlinear}, we present a procedure to obtain non-sharp
measurement-error bounds for any graphical model $\mathcal G$, by relaxing the
model until it is in the class of general IV models. In addition, we provide
sharp bounds for the important case of Figure \ref{fig:iv} (f), which is not a
general IV model.

% \subsection{Graphs with non-randomized instruments}
%
% In the basic IV model (Figure \ref{fig:iv} (a)), the instrument $A$ is assumed
% to be unconfounded with $X$; however, unconfoundedness is a strong
% assumption that does not hold in a variety settings. Instead, we may be willing
% to make a relaxed assumption that the confounders for $A$ and $X$ are
% independent of the confounders for $X$ and $Y$, as shown in Figure
% \ref{fig:gen_iv} (c).
% %
% % We now present sufficient conditions for a graph $\mathcal G$ outside
% % the class of instrumental graphs describe above to represent a model over
% % endogenous variables equivalent to that of a graph $\mathcal G'$ inside the
% % class. Any such $\mathcal G$ can be represented by the very same linear
% % constraints as $\mathcal G'$, thereby extending the class of graphical models
% % corresponding to linear constraints on $P({\bf X} \mid {\bf A})$.
% %
% We can extend the class of graphs defined in Proposition \ref{prop:general_iv}
% to include confounded instruments using the following special case of
% Proposition 5 in \citep{evans}. An alternative proof of this proposition is
% presented in the supplement.
% %
% \begin{proposition}
% \label{prop:confounded_instrument}
% %
%   Suppose a vertex $A$ in a Bayesian network $\mathcal{G}$ has no observed or
%   unobserved parents and has a single child $B$. Then the model for the
%   variables in $\mathcal{G}$ is unchanged if an unobserved common parent of $A$
%   and $B$ is added to the graph, or if the unobserved common parent is added and
%   the edge from $A$ to $B$ is removed.
% %
% \end{proposition}
% %
% This proposition has two important consequences: first, instruments in
% $\mathbf{A}$ may be confounded with their children and, second, if an
% instrument is confounded with its child, it need not have a directed edge to
% that child. This result broadens the set of graphical models for which the
% constraints on the observed data distribution can be expressed linearly in
% $\psi$ to include the graphs such as those shown in Figures \ref{fig:gen_iv}
% (c) and (d). In particular, Figure \ref{fig:gen_iv} (d) can be used to represent a model where $A$ is a proxy for the true unobserved instrument.

% \begin{figure}[t]
%   \centering {\small
%     \include{gen-iv-graph}
%   }
%   \caption{ (a) and (b) are classical instrumental graphs as described in
%     Proposition \ref{prop:general_iv}, (c) and (d) are graphs whose constraints
%     on the observed data distribution can also be represented linearly by
%     Proposition \ref{prop:confounded_instrument}. }
%   \label{fig:gen_iv}
% \end{figure}

% \subsection*{Application: Bounding $E[X]$ in the IV model}
%
% To demonstrate the use of graphical constraints, we will extend our example
% from Section \ref{sec:base} to include an additional binary instrument $A \in
% \{0,1\}$ and we will assume the graphical model in Figure \ref{fig:iv} (a). For
% a complete description of the resulting LP, see Appendix \ref{app:iv_lp}.
% Assume also that the observed conditional distributions $P(Y \mid A=a)$ for $a
% = 0,1$ are the two marginal distributions shown in Figure \ref{fig:mrs_bounds}
% and that $P(A=0) = \frac{1}{2}$. Then, using \emph{only} the constraints
% encoded in the graph, we get the numerical bounds $E[X] \in [0.31,4.69]$. These
% bounds can be made substantially tighter by including additional measurement
% error constraints, but it is worth noting that we can achieve non-trivial
% bounds by relying only on graphical constraints. \hfill\qedsymbol

\end{document}
