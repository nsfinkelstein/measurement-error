\documentclass[main.tex]{subfiles}

\begin{document}

\section{Causal Bounds Under Measurement Error On Outcome}

We consider the scenario in which a set of treatments $\bf A$ is known to only
affect the proxy $Y$ through the unobserved outcome of interest $X$. In some
cases, we consider a denote a second set of treatments $\bf B$ that satisfy the
same condition, to indicate that the two interventions considered need not
necessarily be on the same variables. In this scenario, we derive
assumption-free bounds on the proportion of the population affected. We then
derive bounds on an important causal parameter - the Average Treatment Effect
(ATE) - under various common assumptions made in the causal inference and
measurement error literature.

We note that Balke's linear programming approach can be used to derive bounds on
causal parameters in similar scenarios, for a very limited class of models.
While our derivation of the bounds in this section often use linear programming,
the similarity is superficial. Whereas Balke's approach restricts the class of
models for which it can be used by placing linearity requirements on the model
constraints and the objective, our approach does not. This is because the
linearity requirements we impose are on the joint distributions $P(Y({\bf a}),
Y({\bf b}))$ or $P(X({\bf a}), Y({\bf a}))$, which are stochastic matrices and
will always satisfy the linearity requirements.

In addition, due to the structure of these linear programs, we are able to
derive fast symbolic procedures for obtaining our bounds for arbitrary
cardinalities, in contrast to the vertex enumeration approach, which becomes
prohibitively slow at higher cardinalities or with larger graphs.

\subsection{Assumption Free Bounds on the Proportion Affected}
\label{sec:assumption-free}

In some settings it may be helpful to have a lower bound on the proportion of
the population that would have different outcomes under different treatments.
This proportion can be represented by $P(X({\bf a}) \neq X({\bf b}))$. We note
that by the exclusion criteria, $Y({\bf a}) \neq Y({\bf b}) \implies X({\bf a})
\neq X({\bf b})$, from which it trivially follows that
$P(Y({\bf a}) \neq Y({\bf b})) \le P(X({\bf a}) \neq X({\bf b}))$. To obtain a
lower bound on the $P(X({\bf a}) \neq X({\bf b}))$, then, it will suffice to
obtain a lower bound on $P(Y({\bf a}) \neq Y({\bf b}))$. Intuitively, this is
the smallest amount of mass that can be moved to get from $P(Y({\bf a}))$ to
$P(Y({\bf b}))$, and as such can be seen as a measure of the distance between
the two distributions.

For notational convenience, we will let $\lambda^Y_{ij} = P(Y({\bf a}) = x_i,
Y({\bf b}) = x_j)$. In many of the linear programs in this section, we will use
the following base constraints implied by the observed data and the rules of
probability. We will collectively denote this set of constraints by $(C_0)$:

\begin{align}
  \label{eq:c0}
  (C_0) =
  \bigg\{
  \sum_{i} \lambda^Y_{ij} =  P(Y({\bf b}) = x_j),~~~~
  \sum_{j} \lambda^Y_{ij} =  P(Y({\bf a}) = x_i),~~~~
  \lambda^Y_{ij} \ge 0
  \bigg\}
\end{align}

Then the lower bound on $P(Y({\bf a}) \neq Y({\bf b}))$ can be obtained by
solving the linear program:

\begin{align}
  \label{eq:lp0}
  \min \sum_{i \neq j} \lambda_{ij} \text{~~s.t.~~} (C_0)
\end{align}

This problem can be solved analytically. The proof is deferred to an Appendix,
but the intuition is that the only mass that must move is \textit{excess} mass,
i.e. from values of $x$ for which $P(Y({\bf a}) = x) > P(Y({\bf b}) = x)$, and
is made up of the difference. This amount of mass will necessarily be sufficient
to fill in the dearth of mass for values of $x$ for which $P(Y({\bf a}) = x) <
P(Y({\bf b}) = x)$.

\begin{proposition}
  \label{prop:proportion-affected}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$. Then:
  \[
    P(X({\bf a}) \neq X({\bf b}))
    \ge
    \frac{1}{2} \sum_i | P(Y({\bf a}) = x_i) - P(Y({\bf b}) = x_i)|
  \]
\end{proposition}

If $\bf B$ is taken to be the empty set, this proposition will provide a lower
bound on the proportion of people affected by enacting the intervention $\bf A =
a$. This may provide a valuable check on an assumption that the intervention is
intended to be low-impact with respect to $X$.

\subsection{Average Treatment Effect under Monotonicity
  Assumptions}
\label{sec:causal-monotonicity}

In this section, we consider the average treatment effect (ATE) under three
common monotonicity assumptions.

\begin{enumerate}
\item[(A1)] \textit{No Underreporting}: $Y \ge X$
\item[(A2)] \textit{Positive Effect of Truth on Proxy}:
  $x \ge x' \implies Y(x) \ge Y(x')$ 
\item[(A3)] \textit{Positive Effect of Treatment on Truth}:
  ${\bf a} \ge {\bf a'} \implies X({\bf a}) \ge X({\bf a'})$ 
\end{enumerate}

In the remainder of this section, we derive bounds under each assumption
individually, and under most interesting combinations of these assumptions.

\subsubsection*{Bounds under (A1)}

Under the ``No Underreporting Assumption'', we can use the expected value
$E[Y]$ to bound $E[X]$ from above, as $P(Y \ge X) = 1 \implies
E[Y] \ge E[X]$. The idea is that if $Y \neq X$, then under this assumption it
must be that $X < Y$. It follows that if there is any measurement error, $E[X]<
E[Y]$, and if there is none, then of course $E[X] = E[Y]$. The same reasoning
holds for counterfactual random variables, such that, for example, $E[X({\bf
  a})] \le E[Y({\bf a})]$.

We have no analogous lower bound on $E[X({\bf a})]$. In particular, no observed
data can falsify the hypothesis that $P(X({\bf a}) = x_0) = 1$. This leaves us
with the following bounds.

\begin{proposition}
  \label{prop:bounds-a1}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$, and
  assume (A1). Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \in
    \big[x_0 - E[{Y({\bf b})}], E[{Y({\bf a})}] - x_0\big]
\]
 \end{proposition}

 In other words, the ATE is smallest when $E[X({\bf a})]$ is smallest and
 $E[X({\bf b})]$ is largest, and largest when the reverse holds.

\subsubsection*{Bounds under (A2)}

Here we will introduce an formulate of the ATE that will be of use throughout
this section. For notational convenience, we will let $\lambda^X_{ij} = P(X({\bf
  a}) = x_i, X({\bf b}) = x_j)$, and $\xi_{ij} = x_j - x_i$. Then

\begin{align}
  \label{eq:alt-ate}
  E[X({\bf a}) - X({\bf b})] = \sum_{ij}\xi_{ij} \lambda^X_{ij}
\end{align}

We can reframe this equality as a pair of inequalities that do not make
reference to the magnitude of a change. This will be important, because
Assumption (A2) gives us insight into the direction, but not the magnitude, of
the change between $X({\bf a})$ and $X({\bf b})$. Let $\delta = \min |\xi_{ij}|$
and $\eta = \max |\xi_{ij}|$.

\begin{align}
  \label{eq:alt-ate-ie}
  \delta \sum_{i < j} \lambda^X_{ij}
  -
  \eta \sum_{i > j} \lambda^X_{ij}
  \le
  E[X({\bf a}) - X({\bf b})]
  \le
  \eta \sum_{i < j} \lambda^X_{ij}
  -
  \delta \sum_{i > j} \lambda^X_{ij}
\end{align}

Note that $\delta$ and $\eta$ are known. If we could obtain upper and lower
bounds on $\sum_{i < j} \lambda^X_{ij}$ and $\sum_{i > j} \lambda^X_{ij}$ from
observed data, then those could be appropriately substituted in to
(\ref{eq:alt-ate-ie}) to obtain bounds on the ATE. Fortuntately, under (A2),
such bounds can be got.

Under this assumption, $Y(x) < Y(x') \implies x < x'$, and $Y(x) > Y(x')
\implies x > x'$. Again, the same holds for counterfactual random variables,
such that $Y({\bf a}) < Y({\bf b}) \implies X({\bf a}) < X({\bf b})$, and
$Y({\bf a}) > Y({\bf b}) \implies X({\bf a}) > X({\bf b})$. These relationships
yield the following inequalities:

\begin{align}
  \label{eq:x-increase-bound}
  \sum_{i < j} \lambda^Y_{ij}
  \le \sum_{i < j} \lambda^X_{ij}
  \le 1 - \sum_{i > j} \lambda^Y_{ij}\\
  \label{eq:x-decrease-bound}
  \sum_{i > j} \lambda^Y_{ij}
  \le \sum_{i > j} \lambda^X_{ij}
  \le 1 - \sum_{i < j} \lambda^Y_{ij}
\end{align}

The lower bounds come from the logical implications referenced in the preceding
paragraph. The upper bounds come from the restriction that $\sum_{i \neq j}
\lambda^X_{ij} \le 1$, combined with the lower bounds. Substituting these bounds
into (\ref{eq:alt-ate-ie}), such that the upper and lower bounds on the ATE take
their largest possible values, we obtain:

\begin{align}
  \label{eq:a2-ate-bounds}
  \delta \sum_{i < j} \lambda^Y_{ij}
  -
  \eta (1 - \sum_{i < j} \lambda^Y_{ij})
  \le
  E[X({\bf a}) - X({\bf b})]
  \le
  \eta (1 - \sum_{i > j} \lambda^Y_{ij})
  -
  \delta \sum_{i > j} \lambda^Y_{ij}
\end{align}

Of course, $\sum_{i < j} \lambda^Y_{ij}$ and $\sum_{i > j} \lambda^Y_{ij}$ are
not point-identified from observed data. However, we note that each of the
bounds on the ATE expressed in (\ref{eq:a2-ate-bounds}) are linear in
$\lambda^Y_{ij}$. As such, the bounds on the ATE can be found by solving the
following linear programs. 

\begin{align}
  \label{eq:lp1}
  &\min -\eta + (\delta + \eta)\sum_{i < j} \lambda^Y_{ij}
  \text{~~s.t.~~} (C_0)\\
  &\max ~\eta - (\delta + \eta) \sum_{i > j} \lambda^Y_{ij}
  \text{~~s.t.~~} (C_0)
\end{align}

We now note that the lower bound is clearly achieved when $\sum_{i < j}
\lambda^Y_{ij}$ is minimized subject to ($C_0$), and the upper bound is clearly
achieved when $\sum_{i > j} \lambda^Y_{ij}$ is minimized subject to ($C_0$),
regardless of the values of $\delta$ and $\eta$. The intuition here is clear.

We now present the following analytic solution to minimizing $\sum_{i < j}
\lambda_{ij}$. We provide an intuitive explanation for this solution here; more
detail can be found in the proof. First, we note that both minima are achieved
when the smallest possible total amount of mass moves between $P(Y({\bf a}))$
and $P(Y({\bf b}))$. In any other scenario, it is always possible to replace
mass that increases, and therefore contributes to $\sum_{i < j} \lambda_{ij}$
with mass that stays the same. It follows from our analysis in section
\ref{sec:assumption-free} that mass will only move \textit{from} values of $x_i$
for which $P(Y({\bf a}) = x_i) > P(Y({\bf b}) = x_i)$ \textit{to} values of
$x_j$ for which $P(Y({\bf a}) = x_j) < P(Y({\bf b}) = x_j)$.

We therefore recognize that any values $j$ for which $P(Y({\bf a}) = x_j) <
P(Y({\bf b}) = x_j)$ must have mass move to it. If there is insufficient mass
with $i > j$ to cover the amount of mass that must move, i.e. if $\sum_{i > j}
P(Y({\bf a}) = x_i) - P(Y({\bf b}) = x_j) \le P(Y({\bf b}) = x_j) - P(Y({\bf b})
= x_j)$, and if we are to move the smallest possible amount of mass, then it
clearly follows that there must be an \textit{increase} in mass to $x_j$ at
least equal to the deficit:

\[
  \big(P(Y({\bf b}) = x_j) - P(Y({\bf b}) = x_j)\big)
  - \big(\sum_{i > j} P(Y({\bf a}) = x_i) - P(Y({\bf b}) = x_j)\big)
\]

For notational convenience, we let $\gamma$ be the the minimal value of
$\sum_{i<j} \lambda_{ij}$, and $\beta$ be the minimal value of $\sum_{i > j}
\lambda_{ij}$. $\gamma$ can be found by iteratively accounting for these
deficits. An iterative approach is necessary, because once mass is moved to
account for one deficit, it cannot be then used again to account for another.
Our procedure is given below - the proof of correctness is in the appendix. Note
that the same procedure can be used to minimize $\gamma$ and $\beta$; all that's
required is to transpose the margins of the stochastic matrix. The procedure is
given in Algorithn \ref{alg:gamma}.

\begin{algorithm}[h!]
	\caption{Calculating $\gamma$}
  \label{alg:gamma}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $P(Y({\bf a})), P(Y({\bf b}))$
		\Statex \hspace{-6.5mm} \textbf{Output:} $\gamma$
    \State \textbf{for} $j$ \textbf{in} $0, \dots, cardinality(X)$
    \State \hspace{3mm} \textbf{for} $i$ \textbf{in} $j, \dots, cardinality(X)$
    \State \hspace{6mm} $\lambda_{ij} = \min\{
    \newline ~~~~~~~~~~~~~
    P(Y({\bf a}) = x_i) - P(Y({\bf b}) = x_i) - \sum_{j' < j} \lambda_{ij'},
    \newline ~~~~~~~~~~~~~
    P(Y({\bf b}) = x_j) - P(Y({\bf a}) = x_j) - \sum_{i' < i} \lambda_{i'j}
    \newline ~~~~~~~~~~~
    \}$
    \State $\gamma =
    \frac{1}{2} \sum_i | P(Y({\bf a}) = x_i) - P(Y({\bf b}) = x_i)| -
    \sum_{i > j} \lambda_{ij}$
	\end{algorithmic}
\end{algorithm}

The intuition behind this procedure is as follows. As noted above, the minimum
of the mass that increases must occur if the smallest total possible mass
changes. By definition, this is the smallest possible change, minus the maximum
possible decrease conditional on the smallest possible change. Our procedure
finds the maximum decrease by ensuring that no further decrease is possible
without a decrease in the diagonal (i.e. an increase in the amount of change).
We show this through a case-analysis of $\delta$ moves. Finally, we subtract
this amount from the minimal mass change, found above.

\begin{proposition}
  \label{prop:bounds-a2}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$, and
  assume (A2). Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \in
    \big[
    -\eta + (\delta + \eta)\gamma,
    \eta - (\delta + \eta)\beta
    \big]
\]
\end{proposition}

\subsubsection*{Bounds under (A3)}

Under (A3), we know that any change between $Y({\bf a})$ and $Y({\bf a'})$
corresponds to an increase in $X({\bf a})$ relative to $X({\bf a'})$. Noting
that every increase is an increase of at least $\delta$, and at least $\omega$
of the population increases, we can bound the ATE from below by $\delta
\omega(Y({\bf a}), Y({\bf a'}))$. However, we can do slightly better than that.
Let $a_1 < a_2$ be between $a'$ and $a$, inclusive. Then (A3) also tells us that
any change between $Y({\bf a_1})$ and $Y({\bf a_2})$ corresponds to an increase
between $X({\bf a_1})$ and $X({\bf a_2})$. Moreover, by monotonicity, we know
that the same change is guaranteed to take place between $X({\bf a})$ and
$X({\bf a'})$ as well, as

\[
  a' \le a_1 < a_2 \le a
  \implies X({\bf a'}) \le X({\bf a_1}) \le X({\bf a_2}) \le X({\bf a})
\]

This yields the following bounds.

\begin{proposition}
  \label{prop:bounds-a3}
  Let $\bf A$ be a set causally irrelevant to $Y$ given $X$, and
  assume (A2). Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \ge
    \delta
    \max_{a' \le a_1 < a_2 \le a}
    \omega(Y({\bf a_1}), Y({\bf a_2}))
\]
\end{proposition}

\subsubsection*{Bounds under (A1) and (A2)}

We note that the bounds under (A2) can be improved if (A1) is assumed as well.
Under (A2) alone, we saw that we had no information about the degree of an
increase or a decrease. When we add (A1), we obtain some information about this.
To see why, we consider that under (A1), we know that all those with $Y({\bf b}
= x_j)$ cannot not have $X({\bf b}) - X({\bf a}) > \xi_{0j}$, as $X({\bf b}) \le
x_j$ by the no underreporting assumption, and it is trivially true that $X({\bf
  a}) \ge x_0$. Similarly, those with $Y({\bf a}) = x_j$ could not have $X({\bf
  a}) - X({\bf b}) > \xi_{0j}$ for the same reason.

To take advantage of this information, we formulate alternative bounds based on
equation (\ref{eq:alt-ate}):

\begin{align}
  \label{eq:alt-ate-ie2}
  \delta \sum_{i,j > i} \lambda^X_{ij}
  -
  \sum_{k} \xi_{k0} \sum_{j < k} \lambda^X_{kj}
  \le
  E[X({\bf a}) - X({\bf b})]
  \le
  \sum_{k} \xi_{k0} \sum_{i < k} \lambda^X_{ik}
  -
  \delta \sum_{i, j < j} \lambda^X_{ij}
\end{align}

These bounds come from the fact that all those who decrease from $P(Y({\bf a}) =
x_i)$ have a decrease less than or equal to $\xi_{i0}$, and similarly for those
who increase to $P(Y({\bf b}) = x_j)$.

Relative to the bounds derived under (A2) alone, we now need to bound
$\sum_{j < i} \lambda^X_{ij}$ for fixed $i$, and $\sum_{i < j} \lambda^X_{ij}$
for fixed $j$, in terms of the $\lambda^Y$. We make use of the following bounds:

\begin{align}
  \label{eq:a1-a2-bounds-x-decrease}
  \sum_{k} \xi_{k0} \sum_{j < k} \lambda^X_{kj}
  \le
  \sum_{k} \xi_{k0} \sum_{j \le k} \lambda^Y_{kj}
  \\
  \label{eq:a1-a2-bounds-x-increase}
  \sum_{k} \xi_{k0} \sum_{i < k} \lambda^X_{ik}
  \le 
  \sum_{k} \xi_{k0} \sum_{i \le k} \lambda^Y_{ik}
\end{align}

We describe the intuition for the first upper bound - the second follows the
same idea. For each $k$, it is only possible that those who begin at that $k$ or
above decrease by $\xi_{k}$. In addition, we know those who \textit{increased}
in $Y$ cannot have decreased in $X$. Therefore we must assume that all those who
did not increase in $Y$, i.e. stayed the same or decreased, decreased by the
maximum amount possible, i.e. the full amount from their highest possible value
of $X({\bf a})$ (which is their observed $Y({\bf a})$ by (A1)).

Substituting these, along with (5) and (6), into \ref{eq:alt-ate-ie2}, we
obtain:

\begin{align}
  \label{eq:a1-a2-ate-bounds}
  \delta \sum_{i < j} \lambda^Y_{ij}
  -
  \sum_{k} \xi_{k0} \sum_{j \le k} \lambda^Y_{kj}
  \le
  E[X({\bf a}) - X({\bf b})]
  \le
  \sum_{k} \xi_{k0} \sum_{i \le k} \lambda^Y_{ik}
  -
  \delta \sum_{i > j} \lambda^Y_{ij}
\end{align}

These expressions of the bounds are again linear in $\lambda^Y$, and can
therefore by obtained by solving the linear programs:

\begin{align}
  \label{eq:lp2}
  &\min
  \delta \sum_{i < j} \lambda^Y_{ij}
  -
  \sum_{k} \xi_{k0} \sum_{j \le k} \lambda^Y_{kj}
  \text{~~s.t.~~} (C_0)\\
  &\max
  \sum_{k} \xi_{k0} \sum_{i \le k} \lambda^Y_{ik}
  -
  \delta \sum_{i > j} \lambda^Y_{ij}
  \text{~~s.t.~~} (C_0)
\end{align}

To simplify notation, we let $\gamma$ and $\beta$ be defined as before. Then we
let $\gamma_k = \sum_{i \le k} \lambda^Y_{ik}$ and $\beta_k = \sum_{j \le k}
\lambda^Y_{kj}$, be the optimal values of the $\lambda^Y_{ij}$ consistent with
the data, indicating the proportion of the population for which the move
between values of $Y$ is consistent with an increase (decrease) of $\xi_k$ in
$X$ respectively.

Sketch of next steps: by ``delta moves'', it always makes sense to maximize the
value of larger $\gamma_k$s before smaller ones - each increase in $\gamma_k$
comes at the cost of at most the same increase in $\beta$ and decrease in a
$\gamma_{k'}$ with $k' \le k$. Because $\k - k' \ge \delta$, this trade is
always worthwhile. Likewise it always makes sense to maximize even the smallest
$\gamma_k$ at the expense of increase $\beta$. Note that this does not include
$\gamma_0$, as $P(Y({\bf b}) = x_0)$ implies that there cannot have been an
increase. Therefore, using $\gamma$ and $\beta$ from the above, we present the
following procedure to obtain $\gamma_k$, and correspondingly, $\beta_k$.

\begin{algorithm}[h!]
	\caption{Calculating $\gamma_k$}
  \label{alg:lambda-values}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $P(Y({\bf a})), P(Y({\bf b}))$
		\Statex \hspace{-6.5mm} \textbf{Output:} $\gamma_k$
    \State \textbf{for} $j$ \textbf{in} $cardinality(X) - 1, \dots, 1$
    \State \hspace{3mm} \textbf{for} $i$ \textbf{in} $j, \dots, 0$
    \State \hspace{6mm} $\lambda_{ij} = \min\{
      P(Y({\bf b}) = x_j) - \sum_{i < i' \le j} \lambda_{i'j},
      P(Y({\bf a}) = x_i) - \sum_{j < j'} \lambda_{ij'}
    \}$
    \State \textbf{for} $k$ \textbf{in} $1, \dots, cardinality(X)$
    \State \hspace{3mm} $\gamma_k = \sum_{i \le k} \lambda_{ik}$
	\end{algorithmic}
\end{algorithm}

We use these values to present the following proposition.

\begin{proposition}
  \label{prop:bounds-a2}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$, and
  assume (A1) \& (A2). Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \in
    \big[
    \delta \gamma - \sum_k \xi_{k0} \beta_k,
    \sum_k \xi_{k0} \gamma_k - \delta \beta
    \big]
\]
\end{proposition}

\subsubsection*{Bounds under (A2) and (A3)}

The combination of assumptions (A2) and (A3) implies that any change between
$Y({\bf a})$ and $Y({\bf a'})$ must be an increase, as increasing $\bf A$ can
only increase $X$, and increasing $X$ can only increase $Y$. Note that this
renders this combination of assumptions easily falsifiable; if some portion of
the population must decrease to move from $P(Y({\bf a}))$ to $P(Y({\bf a'}))$,
at least one of the two cannot hold.

If we refer to the bounds obtained under assumption (A2) alone, we can see that
the negative term in the lower bound disappears, as by assumption $\sum_{i > j}
\lambda^X_{ij} = 0$. It's also true that $\sum_{i > j} \lambda^Y_{ij} = 0$,
which renders our upper bound on $\sum_{i < j} \lambda^X_{ij}$ in
(\ref{eq:x-increase-bound}) to be equal to 1 by construction. Intuitively, this
is because there is nothing we could see - short of falsifying this combination
of assumptions - that could convince us that the full population hand't
increased by the full amount possible. This yields the following bounds.

\begin{proposition}
  \label{prop:bounds-a2-a3}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$, and
  assume (A2) \& (A3). Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \in
    \big[
      \delta \gamma,
      \eta
    \big]
\]
\end{proposition}

\subsubsection*{Bounds under (A1), (A2), and (A3)}

If we add assumption (A1) to the reasoning above, it becomes possible to create
a less trivial upper bound. In particular, we can use the bound from
(\ref{eq:a1-a2-bounds-x-increase}), yielding the following bound.

\begin{proposition}
  \label{prop:bounds-a1-a2-a3}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$, and
  assume (A2) \& (A3). Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \in
    \big[
      \delta \gamma,
      \sum_k \xi_{k0}\gamma_k
    \big]
\]
\end{proposition}

These are identical to the bounds under just (A1) and (A2), except that $\beta$
is now assumed to be $0$.

\subsection{The Limited Errors Assumption}
\label{sec:limited-errors}

It is commonly assumed that there is a known upper bound on the proportion of
the population measured with error, denoted here by $\lambda$ . If we extend
this assumption to be that at least $\lambda$ of the population will be measured
without error under any intervention, we can use it to bound $E[X({\bf a})]$
given the distribution $P(Y({\bf a}))$. This too can can be expressed as a
linear program. In this case, we treat $P(Y({\bf a}), X({\bf a}))$ as a
stochastic matrix with only one margin, $P(Y({\bf a}))$, known. In this case,
let $\phi^{\bf a}_{ij} = P(X({\bf a}) = x_i, Y({\bf a}) = x_j)$. Then the linear
programming problem can be expressed as follows:

\begin{align}
  \label{eq:lp-error-limit}
\text{optimize~~} \sum_i x_i \sum_{j} \phi^{\bf a}_{ij}
\text{~~s.t.~~}
  \sum_{i} \phi^{\bf a}_{ij} =  P(Y({\bf a}) = x_j)
  ~~\text{and}~~
  \sum_{i \neq j} \phi^{\bf a}_{ij} \le \lambda
  ~~\text{and}~~
  \phi^{\bf a}_{ij} \ge 0
\end{align} 

The same can be done for the intervention $\bf B = b$ to obtain bounds on
$E[X({\bf b})]$. Note that there is no constraint in the optimization problems
used to bound $E[X({\bf a})]$ and $E[X({\bf b})]$ that mentions any parameter
from the other problem. Therefore the optima of the contrast can be obtained by
solving these problems seperately, and taking the appropriate difference.

We note, again, that this linear program may be solved using a simple procedure,
outlined below. The idea behind this procedure is that that expected value is
maximized when the full permitted error moves from ``as low as possible'' in the
distribution to ``as high as possible''. Let $cardinality(X) = m$, then:

\begin{algorithm}[h!]
	\caption{Calculating $\max E[X({\bf a})]$}
  \label{alg:lambda-values}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $P(Y(a)), \lambda$
		\Statex \hspace{-6.5mm} \textbf{Output:} $\max E[X({\bf a})]$
    \State $\phi^{\bf a} = {\bf 0}$
    \State \textbf{for} $i$ \textbf{in} $0, \dots, n$
    \State \hspace{3mm} $\phi^{\bf a}_{mi} = \min\{
        P(Y({\bf a}) = x_i), \lambda - \sum_{j < i}\phi^{\bf a}_{mj}
    \}$
    \State \hspace{3mm} $\phi^{\bf a}_{ii} =
      P(Y({\bf a}) = x_i) - \phi^{\bf a}_{mi}$
    \State $\max E[X({\bf a})] = \sum_i x_i \sum_{j} \phi^{\bf a}_{ij}$
	\end{algorithmic}
\end{algorithm}

The same procedure can be used to calculate $\min E[X({\bf a})]$, but instead of
mass being assigned to $\phi^{\bf a}_{mi}$ at step 3., it is assigned to
$\phi^{\bf a}_{0i}$. Using values obtained in this way, we present the following
bounds.

\begin{proposition}
  \label{prop:bounds-error-limit}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$, and
  assume there is a known upper bound to the proportion of the population
  measured with error. Let $\min E[X({\bf a})]$ et. al be given as by the
  procedure above. Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \in
    \big[
      \min E[X({\bf a})] - \max E[X({\bf b})],
      \max E[X({\bf a})] - \min E[X({\bf b})]
    \big]
\]
\end{proposition}

\subsubsection*{Adding Assumption (A1)}

Next, we note that (A1) implies the following two constraints, which can be
added to the optimization problem described in (\ref{eq:lp-error-limit}):

\begin{align}
  \sum_{i > j} \phi_{ij}^{\bf a} = 0
  ~~\text{and}~~
  \sum_{i > j} \phi_{ij}^{\bf b} = 0
\end{align}

If either of these sums were not equal to zero, it would be indicative of
underreporting, $X > Y$. It is easy to see that this does not affect our
calculation of the minimum possible value of $E[X({\bf a})]$ and $E[X({\bf b})]$
described above, as at no time in those calculations is under-reporting implied
(i.e. is $\phi^{\bf a}_{ij}$ set above $0$ for any $i > j$). However, it is
also easy to see that in computing the maximum, we assume underreporting is
possible. Using the same reasoning as was used when considering (A1) alone, we
note that as soon as (A1) is involved, the expected value of $X$ is bounded from
above by the expected value of $Y$. This yields the following proposition:

\begin{proposition}
  \label{prop:bounds-error-limit-a1}
  Let $\bf A$ and $\bf B$ be sets causally irrelevant to $Y$ given $X$, and
  assume there is a known upper bound to the proportion of the population
  measured with error, and assume (A1) as well. Let $\min E[X({\bf a})]$ et. al
  be given as by the procedure above. Then:
  \[
    E[{X({\bf a}) - X({\bf b})}] \in
    \big[
      \min E[X({\bf a})] - E[Y({\bf b})],
      E[Y({\bf a})] - \min E[X({\bf b})]
    \big]
\]
\end{proposition}

\subsubsection*{Adding Assumptions (A2) and (A3) - Outer Bounds}

In this section we provide additional constraints implied by (A2) and (A3) that
will yield outher bounds on the objective under these assumptions. In the
following section, we provide sharp bounds at the cost of an increased number of
parameters, and therefore a more costly optimization problem.

We now assume that we know the effect of treatment on $X$ is positive, i.e.
assume (A3). A consequence of this assumption is the following linear
constraint:

\begin{align}
 \sum_{i \le k, j} \phi^{\bf a}_{ij} \ge \sum_{i \le k,j} \phi^{\bf b}_{ij}
    \text{~~ for all~} k
\end{align}

To see that this constraint follows from (A3), we make an inductive argument. It
is clear that $P(X({\bf a}) = x_0) \le P(X({\bf b}) = x_0)$, as otherwise it
must be the case that some proportion of the population decreased to $x_0$,
violating (A3). Now assuming the condition holds for $k - 1$, we show that it
must hold for $k$. We note that because $X({\bf a}) \ge X({\bf b})$ by
assumption, the mass in $P(Y({\bf b}) = x_k)$ must come from $x_i$ with $i \le
k$ . If $i < k$, this mass is already included in $\sum_{i < k}P(Y({\bf a}) =
x_i)$, but not yet in $\sum_{i < k}P(Y({\bf b}) = x_i)$ . If $i = k$, then the
mass will be added to both $\sum_{i \le k}P(Y({\bf b}) = x_i)$ and $\sum_{i \le
  k}P(Y({\bf a}) = x_i)$. This shows that $P(Y({\bf b}) = x_k) - P(Y({\bf a}) =
x_k) \le \sum_{i < k} P(Y({\bf a}) = x_k) - P(Y({\bf b}) = x_k)$, i.e. the mass
added to the right hand side at step $k$ cannot make up its deficit w.r.t the
left hand side at step $k - 1$, which yields the conclusion.

It is important to note that while (A3) is sufficient for this constraint to
hold, it is not necessary. There are distributions for which the constraint
holds, but (A3) does not. It follows that the bounds that result from solving
this linear program will not be sharp - better bounds can in theory be attained.
For a set of variables with fixed levels and of fixed cardinalities, the linear
program can be solved symbolically through Balke's method.

When (A2) is added to (A3), as we saw earlier they jointly imply that $Y({\bf
  a}) \ge Y({\bf b})$. This allows us to add another constraint, by exactly the
same argument as above (which was based on $X({\bf a}) \ge X({\bf b})$), but
used across columns of the stochastic matrices rather than rows.

\begin{align}
 \sum_{i, j \le k} \phi^{\bf a}_{ij} \ge \sum_{i, j \le k} \phi^{\bf b}_{ij}
    \text{~~ for all~} k
\end{align}

As before, (A2) and (A3) are jointly sufficient for this condition, but not
necessary for it.

As noted above, it is possible to combine any of the constraints implied by
these assumptions to obtain bounds either numerically, or - for fixed levels of
$X$ - symbolically through Balke's method. We conjecture that procedures for
generating these solutions symbolically are possible, as above, but they are
more complicated than procedures we have seen to this point. In the interest of
space, we do not derive them here.

\subsubsection{Assumptions (A2) and (A3) - Sharp Bounds}

Assumptions (A2) and (A3) cannot be expressed as restrictions on the
distributions $P(X({\bf a}), Y({\bf a}))$ and $P(X({\bf b}), Y({\bf b}))$, as
they are, in some sense, about the distributions $P(X({\bf a}), X({\bf b}))$ and
$P(Y({\bf a}), Y({\bf b}))$. In Section \ref{sec:causal-monotonicity}, we were
able to consider only the distribution $P(Y({\bf a}), Y({\bf b}))$, and the
shadow these assumptions cast upon it. However, that is no longer possible, as
the ``limited errors'' assumption clearly requires the consideration of joints
involving both $X$ and $Y$. We therefore consider the joint distribution of all
four counterfactual random variables. This explodes the number of parameters in
the naively frame linear program from $2m^2$ to $m^4$.

For notational conveneience, we will let $\phi_{hijk} = P(X({\bf a}), X({\bf
  b}), Y({\bf a}), Y({\bf b}))$. In the interest of space, will not express the
objectives, or the constraints of (A1) and the limited errors assumption, in
terms of this joint, as they can all be trivially expressed by summing out the
two irrelevant dimensions. Then we can express necessary and sufficient
conditions for (A2) and (A3) respectively:

\begin{align}
 \sum_{h,i > h,j,k} \phi_{h,i,j,k} = 0\\
 \sum_{h,i > h,j,k < j} \phi_{hijk} = 0
\end{align}

Again, for $X$ and $Y$ with fixed levels, this may be solved symbolically using
Balke's method, though for large numbers of parameters it will be very slow. It
can also be solved numerically. We do not provide a procedure for solving it,
though we conjecture it is possible to create one.

\subsection{Partial Identification of $Y({\bf a})$}

In the derivations above, we have assumed that $Y({\bf a})$ and $Y({\bf b})$
have identified densities. However, in some settings these densities may be
\textit{partially identified}. Counterfactual densities under the IV model have
long been known to be partially identified; recent work has further demonstrated
that counterfactual densities can be partially identified under a much broader
class of models.

This partial identification may be incorporated into the bounds in several ways.
We first consider the linear programming formulations above. In all of them, we
can see that there is a constraint that has the following form:

\begin{align}
  \sum_{\mathcal Z} P(Y({\bf a}) = x_i, Z = z) = P(Y({\bf a}) = x_i)
\end{align}

In other words, we consider the marginal distribution to be fixed. If the
marginal distribution is partially rather than point identified, we would need
to replace this equality constraint with the following constraints, where $\min
P(Y({\bf a}) = x_i)$ represents a lower bound on $P(Y({\bf a}) = x_i)$, and
likewise for $\max$.

\begin{align}
  \sum_{\mathcal Z} P(Y({\bf a}) = x_i, Z = z) &\le \max P(Y({\bf a}) = x_i)\\
  \sum_{\mathcal Z} P(Y({\bf a}) = x_i, Z = z) &\ge \min P(Y({\bf a}) = x_i)\\
  \sum_{\mathcal Z, \mathcal Y} P(Y({\bf a}) = y, Z = z) &= 1
\end{align}

The first two inequality constraints follow from the bounds on the
density of the counterfactual random variable. The final equality constraint is
now necessary, because we no longer have fixed margins to ensure the joint
integrates to 1.

Next, we consider bounds derived in the absence of an explicit linear program,
such as those in Proposition \ref{prop:bounds-a1}. We have seen in Section
\ref{sec:limited-errors} that these bounds could have been derived using a
larger linear program. However, it is simpler to note that the bounded
distribution of $Y({\bf a})$ implies a simple set of bounds on $E[Y({\bf a})]$.
These bounds can be substituted into the bounds in the proposition to obtain
bounds on the ATE under partial identification of the counterfactual
distribution.

Finally, we consider bounds that are indeed characterized as linear programs,
but for which symbolic procedures have been provided. In these cases, the linear
program may be adapted as described above. However, it is also possible to adapt
the procedure. We leave further details on this to future work.

\end{document}
