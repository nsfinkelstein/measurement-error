\documentclass[main.tex]{subfiles}

\begin{document}

\section{Empirical example: the Oregon Health Insurance Experiment}
\label{sec:ohie}

To demonstrate our approach, we analyzed the effect of Medicaid enrollment on
mental health outcomes using public data from the Oregon Health Insurance
Experiment (OHIE) \citep{finkelstein2012oregon}\footnote{The full dataset can
be found at
\url{https://www.nber.org/programs-projects/projects-and-centers/oregon-health-insurance-experiment}.}. In 2008, the state of Oregon expanded Medicaid
coverage using a lottery to determine who would become eligible for enrollment.
This randomization created a natural experiment, allowing researchers to study
the effects of Medicaid coverage on healthcare usage and health outcomes. For
complete details on the OHIE, see \citep{finkelstein2012oregon}. In one such
study, \citep{baicker2018oregon} found, among other things, that Medicaid
enrollment reduced depression as measured by the Patient Health Questionnaire
(PHQ-9) taken approximately two years after the lottery.

The PHQ-9 is a nine question survey measuring various depressive symptoms on a
0 to 3 point scale. The total of these points is frequently used as a measure
of overall depression, with a score above 10 serving as a cutoff for moderate
to severe depression \citep{kroenke2001phq}. However, as a measurement of
diagnosable depression, PHQ-9 scores are subject to various forms of
measurement error. For example, \citet{kroenke2001phq} acknowledged that scores
between 10 and 15 represent a ``gray zone'', with much lower precision than
scores above 15. \citet{torous2015utilizing} found that observed PHQ-9 scores
were sensitive to the way the survey was administered, with the average score
reported on a mobile app 3 points higher than average scores reported to a live
surveyor. In this section, we estimate bounds on the effect of Medicaid
enrollment on depression and use our method to test the sensitivity of these
estimates to different combinations of measurement error assumptions.

\subsection{Target parameter and modeling assumptions}

Our target parameter is the ATE of Medicaid enrollment on the presence of
moderate to severe depression as measured by the PHQ-9 score. We restrict our
analysis to single person households resulting in a sample of size $N=9599$
lottery enrollees. Following \citet{kroenke2001phq}, we categorized the PHQ-9
scores into 5 bins representing no (0-4), mild (5-9), moderate (10-14),
moderately severe (15-20), and severe ($> 20$) depression. As in
\citet{baicker2018oregon}, we treat winning the enrollment lottery as a binary
instrumental variable for Medicaid enrollment. Let $A \in \{0,1\}$ represent
winning the lottery, let $T \in \{0,1\}$ represent Medicaid enrollment, let $X
\in \{1,...,5\}$ represent a person's true depression category, and let $Y \in
\{1,...,5\}$ represent the measured PHQ-9 category. Then, the ATE is given by
$P(X(T=1) > 2) - P(X(T=0) > 2)$.

% TODO: The following describes the LATE and contrasts with the ATE, but this
% seemed unnecessary However, the analysis in \citet{baicker} focused on the
% local average treatment effect (LATE), or the effect among people who would
% have enrolled in Medicaid had they won the lottery, but not otherwise. Under
% the assumptions made in \citep{baicker}, the LATE is fully identified from the
% observed data, but may not accurately reflect the effect of Medicaid
% enrollement in the full population (i.e., the ATE) if, for example, the people
% who do not enroll in Medicaid are those less likely to benefit from
% enrollment. Instead, we use our method to bound the ATE, which is only
% partially identified, even under no measurement error.

For all analyses, we assume the general IV model shown in Figure \ref{fig:epsilons} where $\Lambda$ represents and unobserved confounder.
%
% \begin{figure}[h]
% \centering
% \begin{tikzpicture}[>=stealth, node distance=1.0cm]
%   \begin{scope}[]
%     \tikzstyle{vertex} = [
%       draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt
%     ]
%
%     \tikzstyle{edge} = [
%       ->, black, very thick
%     ]
%
%     \node[vertex, circle] (z) {$A$};
%     \node[vertex, circle] (a) [right of=z] {$T$};
%     \node[vertex, circle, blue] (x) [right of=a] {$X$};
%     \node[vertex, circle] (y) [right of=x] {$Y$};
%     \node[vertex, circle, blue] (u) [above of=x, yshift=-0.2cm] {$\Lambda$};
%
%     \draw[edge] (z) to (a);
%     \draw[edge] (a) to (x);
%     \draw[edge] (x) to (y);
%     \draw[edge] (u) to (a);
%     \draw[edge] (u) to (x);
%     \draw[edge] (u) to (y);
%
%     % \node[below of=a, yshift=0.3cm, xshift=.5cm] (l) {$(c)$};
%   \end{scope}
% \end{tikzpicture}
% \end{figure}
%
Additionally, we make the following monotonicity assumption reflecting the
belief that enrolling in Medicaid cannot increase depressive symptoms
%
\begin{align}
	P(X(T=0) = x, X(T=1) = x') = 0 \quad \forall \,\, x < x'.
\end{align}
%

Even with no measurement error, the ATE is only partially identified under
these assumptions. Our goal is to test the sensitivity of the ATE partial
identification bounds to various combinations of the following measurement
error assumptions:
%
\begin{enumerate}
  \addtolength{\itemindent}{0.5cm}
	\item[(EB)] $P(|X(a) - Y(a)| > 0) \leq \epsilon \quad \forall \,\, a$
	\item[(Exp)] $\mathbb{E}[X(a)] = \mathbb{E}[Y(a)] \quad \forall\,\,a$
	\item[(Sym)] $P(X(a)=x,Y(a)=y) = P(X(a)=x,Y(a)=y')$\\$ \qquad\qquad \forall\,\,a,|x - y| = |x - y'|$
	\item[(Mon)] $P(X(a) = x, Y(a) = y) > P(X(a) = x, Y(a) = y')$\\$\qquad\qquad \forall\,\,a, |x - y| < |x - y'|$
	% \item[(EB2)] $P(|X(a) - Y(a)| > 1) \leq \epsilon/10 \quad \forall \,\, a$
\end{enumerate}
%
All of these assumptions are versions of the measurement error assumptions
listed earlier in Section \ref{sec:measurement_error_assumptions}. Assumption
(EB) is version (A1) and says that the total proportion of errors is less than
$\epsilon$. The sensitivity parameter $\epsilon$ can be thought of as a total
error budget, and we vary $\epsilon$ across the grid $[0.00,0.001,...,0.04]$ to
test how large $\epsilon$ can be before the partial identification set includes
zero. Assumption (Exp) is a version of (A2) and says that the measured PHQ-9
category is an unbiased measurement of the true depression category. Assumption
(Sym) is a version of (A2) and says that an error of magnitude $k$ upwards is
as likely as an error of magnitude $k$ downwards. Finally, assumption (Mon) is
a version of (A5) and says that small errors are more likely than large errors.
Note that all assumptions were applied under both settings of the instrumental
variable $A$, reflecting the belief that they apply regardless of whether the
person wins the lottery or not.
% Finally,
% assumption (EB2) bounds errors of more than a single category to make up no
% more than 10\% of the total error budget.
Importantly, our goal in this work is not to argue that any particular
combination of these assumptions is ``correct'' in the context of the OHIE, a
task we leave to subject-matter experts. Instead, our goal is to demonstrate
how these assumptions may be flexibly applied to test the sensitivity of one's
conclusions.

\subsection{Results}

\begin{figure}[t]
    \centering
	% \begin{subfigure}[b]{0.49\textwidth}
	\includegraphics[width=\columnwidth]{../output/figures/ohie_sensitivity_grid_plot.png}
    % \end{subfigure}
  \caption{
    Partial identification bounds on the ATE of Medicaid enrollment on depression under different measurement error assumptions and error budgets $\epsilon$. The dark and light blue regions represent the point estimate and 95\% confidence interval, respectively, for the partial identification bounds under the weakest set of assumptions (EB only). Confidence intervals for the other models were omitted to avoid cluttering the plot.
  }
  \label{fig:ohie}
\end{figure}

\begin{figure}[t]
\begin{subfigure}[b]{0.39\columnwidth}
	\centering
	\begin{tikzpicture}[>=stealth, node distance=1.0cm]
	  \begin{scope}[]
	    \tikzstyle{vertex} = [
	      draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt
	    ]

	    \tikzstyle{edge} = [
	      ->, black, very thick
	    ]

	    \node[vertex, circle] (t) {$T$};
	    \node[vertex, circle] (a) [above of=t]{$A$};
	    \node[vertex, circle, blue] (x) [right of=t] {$X$};
	    \node[vertex, circle] (y) [right of=x] {$Y$};
	    \node[vertex, circle, blue] (u) [above of=x] {$\Lambda$};

	    \draw[edge] (a) to (t);
	    \draw[edge] (t) to (x);
	    \draw[edge] (x) to (y);
	    \draw[edge] (u) to (t);
	    \draw[edge] (u) to (x);
	    \draw[edge] (u) to (y);

	    % \node[below of=a, yshift=0.3cm, xshift=.5cm] (l) {$(c)$};
	  \end{scope}
	\end{tikzpicture}
\end{subfigure}
%
\begin{subfigure}[b]{0.59\columnwidth}
    \centering
	\begin{tabular}{l c}\hline
		Model & Max. budget \\\hline\hline
		EB only & 1.4\%\\
		EB+Exp & 2.1\%\\
		EB+Exp+Mon & 2.4\%\\
		EB+Exp+Sym & 2.8\%\\\hline
	\end{tabular}
\end{subfigure}
  \caption{
    The figure on the left shows the assumed genral IV model. The table on the right shows the maximum error budgets for each measurement error model.
  }
  \label{fig:epsilons}
\end{figure}

The estimated partial identification bounds under different measurement error
models and error budgets $\epsilon$ are shown in Figure \ref{fig:ohie}. The
method used to estimate confidence intervals is described in Appendix
\ref{app:estimation}. In all cases, as $\epsilon$ grows, the upper bound
approaches and eventually reaches zero. A quantity of interest is the
$\epsilon$ at which the upper bound \emph{hits} zero, which we refer to as the
\textbf{maximum error budget}. The maximum error budget for each measurement
error model is shown in table in Figure \ref{fig:epsilons}. Unsurprisingly,
using only the (EB) assumption resulted in the lowest maximum error budget
whereas combining the (EB), (Exp), and (Sym) assumptions doubled this maximum
error budget. However, no measurement error model resulted in a maximum error
budget of more than 2.8\% highlighting the potential sensitivity of these
results to measurement error.

\end{document}