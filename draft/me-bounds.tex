\documentclass[main.tex]{subfiles}

\begin{document}

\section{Examining Measurement Error}

We are interested in the class of models such that there exists any nonempty set
of ancestors of $Y$ that are not ancestors of $X$. We call this set $\bf B$.
This is quite a large class of models. In particular, it is permitted
that $\bf B$ is confounded with $X$. We further assume $P(Y = y, {\bf B = b})$;
a subset of the positivity assumption on observed data.

We begin by positing a new graph $\mathcal G'$ in which all variables on
any path between $\bf B$ and $Y$ not in $\bf B$ are latent-projected out of the
graph. This is done simply so that $Y$ can be written in terms of the structural
equation $f_Y^{\mathcal G'}(X, {\bf B}, \epsilon_Y)$, without substantively
changing the causal structure.

\begin{remark}[AMEA function calls]
It follows from the Absence of Measurement Error Assumption (AMEA) that for all
tuples $(x, {\bf b}, \epsilon_y)$ corresponding to the observed data,
$f_Y^{\mathcal G'}(x, {\bf b}, \epsilon_y) = x$. 
\end{remark}

In the remainder of this section, we derive lower bounds on measurement error
for such models under no assumptions, and then under some assumptions.

\subsection{Falsifying the AMEA}

TODO: Can $X$ and $Y$ also be thought of as sets?

First, we consider graphs in which $Y$ is not confounded with $\bf B$, but all
other confounding is permitted, e.g.:

\[X \rightarrow Y \leftarrow B, X \leftrightarrow B, X \leftrightarrow Y\]

Under the AMEA, $X = Y$, so the positivity assumption on $P(y, {\bf b})$ is
sufficient to ensure that if we intervene to set $\bf B$ to some value of $\bf
b^*$ with positive support in the observed data, every function call
$f_Y^{\mathcal G'}(x, {\bf b^*}, \epsilon_y)$ will have been made in the
observed data. From this we can deduce that under the AMEA, for every function
call under intervention $f_Y^{\mathcal G'}(x, {\bf b^*}, \epsilon_y) = x$.

This means that $Y$ and $X$ keep the same distribution under intervention on
$\bf B$, and because $X$ is not a descendent of $\bf B$, this distribution
cannot change under intervention on $\bf B$. Therefore, if in such a model we
see that the distribution of $Y$ changes under intervention on $\bf B$, we have
falsified the AMEA.

\begin{proposition}[Level 1 Falsification]
  Let there be no confounding between $Y, {\bf B}$. Then if $P(Y({\bf
    b})) \neq P(Y)$ for any $\bf b$, the AMEA is falsified.
\end{proposition}

If the AMEA is not falsified by this proposition, it may nevertheless be false.
In this case it may be the case that for every pair of interventions on every
sets $\bf B, C$, the changes in the distributions of the potential outcomes
relative to the observed data induced by the intervention, exactly ``cancel''.
(Or it may be the case that there are no changes under intervention, but there
is still measurement error). In other words, for all $y$:

\[
  P(Y({\bf b}) = y \land Y({\bf c}) \neq y) =
  P(Y({\bf b}) \neq y \land Y({\bf c}) = y))
\]

Because we know that $X$ is not changed by intervention on $\bf B$, and all
calls to the structural equation of $Y$ in the observed data and therefore under
intervention must yield $X$, we known that there should be no portion of the
population whose value of $Y$ changes. Therefore if we can bound
$P(Y({\bf b}) = y \land Y({\bf c}) \neq y)$ away from zero for any $y, \bf b,
c$, the AMEA is falsified.

First, we present bounds (proof below):

\begin{proposition}
$P(Y({\bf b}) = y \land Y({\bf c}) \neq y)$ is bounded from below by 
  \[1 - \sum_y \bigg(
  1 - \sum_{(y_1, y_2) \neq (y, y)} \max \xi(Y({\bf b}) = y_1 \land Y({\bf c}) = y_2)
  \bigg)\] with $\xi$ as in the UAI draft. We denote these new bounds by $\eta$.
\end{proposition}

We use this bound to obtain a stronger test for falsification:

\begin{proposition}[Level 2 Falsification]
  Let there be no confounding between $X, Y$ or $Y, {\bf B}$ or $Y, {\bf C}$,
  where $\bf C$ has the same properties as $\bf B$. If there exist a $y$ such
  that 

  \[\eta(Y({\bf b}) = y \land Y({\bf c}) \neq y) > 0 \]

  then the AMEA is falsified.
\end{proposition}

TODO: Come up with an example where level 1 cannot falsify and level 2 can -
seems simplest one is two correlated parents of $Y$.

Finally, we examine the case where $\bf B$ and $Y$ are permitted to be
confounded. This complicates the analysis in two ways. First, $Y({\bf B})$ no
longer has an identified density. Second, the AMEA no longer implies that every
tuple $X, \bf B, \epsilon_Y$ seen after intervention on $\bf B$ is also seen in
the observational data. In particular, there may be pairs of $\bf b, \epsilon_Y$
not see in the observed data that come about after intervention, even under the
positivity assumption.

However, the proportion of the population for which these new tuples occurs is
bounded by $P({\bf B \neq b})$ - everyone with $\bf B = b$ has a tuple seen in
the observed data. Therefore, if the minimum proportion of the population that
needs to change to move from $P(Y)$ to the partially identified $P(Y({\bf B}))$
exceeds $P({\bf B \neq b})$, the AMEA is falsified.

\begin{proposition}[Level1 Falsifiability Under Confounding]
  Under arbitrary confounding, the AMEA is falsified if there exists a $\bf B,
  b$ such that $\gamma(Y, Y({\bf b})) > P({\bf B \neq b})$, or a $\bf B, C, b,
  c$ such that $\gamma(Y({\bf b}), Y({\bf c})) > P(\bf B \neq b \lor C \neq c)$.
\end{proposition}

An example of when this might come up is:

\[X \rightarrow Y \leftarrow B \leftarrow C, Y \leftrightarrow B, X \leftarrow
  C, X \leftrightarrow B\]

Then we can still use $B$ to falsify the AMEA. $C$ cannot be used directly
because it is an ancestor of $X$, but it can be used as an instrument for $B$.

There is a similar analogue to the Level2 Falsifiability claim above. Note $\bf
C$ can be the emptyset

\begin{proposition}[Level 2 Falsifiability under confounding]
  If there exist a $y$ such that 

  \[\eta(Y({\bf b}) = y \land Y({\bf c}) \neq y) > P(\bf B \neq b \lor C \neq c) \]

  then the AMEA is falsified.
\end{proposition}

In other words, if there exist any specific $y$ for which the change in its
density exceeds the change permitted by the introduction of new arguments to the
structural equation, then the AMEA is falsified.

\subsection{Lower Bounds on Measurement Error}

TODO: See what assumptions like monotonicty and limited errors buy here and
below.

We now reason about lower bounds on measurement error in distributions in which
the AMEA has been falsified.

We first consider a model with no confounding between $X$ and $\bf B$ or $Y$ and
$\bf B$, e.g. $X \rightarrow Y \leftarrow B, X \leftrightarrow Y$. Imagine that
when we set $\bf B = b$, the distribution changes such that at least $\gamma$ of
the population must have switched values of $Y$. This $\gamma$ all came from the
population with $\bf B \neq b$. Because there is no confounding, and $B$ is
indepedent of $X$ (only related through the collider $Y$), we can deduce from
this that at least $\frac{\gamma}{P(\bf B \neq b)}$ of the overall population,
in terms of the natural assignments of $X$ and $\epsilon_Y$, is such that the
value of $Y$ is a function of $B$. Now, in the best case, it is a function of
$B$ such that it only returns the truth when $B$ takes on its least likely
value. This leads us to the following proposition:

\begin{proposition}[Lower bound on Measurement Error in Unconfounded Case]
  Let $\gamma$ represent the smallest portion of the population that would have
  to change values of $Y$ to move from $P(Y)$ to $P(Y({\bf b}))$, and let ${\bf b^*}
  = \texttt{argmin}_{\bf b} P({\bf B = b})$. Then the proportion of measurement error is
  bounded from below by:

  \[\frac{\gamma}{P({\bf B \neq b})} \times P({\bf B = b^*})\]
\end{proposition}

Note that these bounds can be combined in the following way for different values
of ${\bf b}$. In the above proposition, we can only see the proportion of the
population with $\bf B \neq b$ for which $Y$ is a function of $B$. To improve on
these bounds, we can also look for the proportion of the population with $\bf B
= b$ that changes upon intervention.

\begin{proposition}[Improved Lower bound on Measurement Error in Unconfounded Case]
  Let $\gamma'$ represent the smallest portion of the population that would have
  to change values of $Y$ to move from $P(Y \mid {\bf B = b})$ to $P(Y({\bf b'})
  \mid {\bf B = b})$, and let ${\bf b^*} = \texttt{argmin}_{\bf b} P({\bf B =
    b})$. Then the proportion of measurement error is bounded from below by:
  
  \[
    \bigg(
    \frac{\gamma'}{P({\bf B = b})} +
    \frac{\gamma}{P({\bf B \neq b})}\bigg) \times P({\bf B = b^*})
  \]
\end{proposition}

We now investigate lower bounds when confounding is allowed between $\bf B$ and
$X$.

[...]

We investigate lower bounds when confounding is permitted between $\bf B$ and
$\bf Y$.

[...]

Finally, we investigate lower bounds on measurement error under arbitrary
confounding.

[...]

\subsection{Upper Bounds on Resistence to Measurement Errors}

In this section, we provide an upper bound on the proportion of the population
that will always report the true value, regardless of the value of $\bf B$. This
may be of use if some values of $\bf B$ are cheaper, but may increase
measurement error. A low upper bound on the proportion of the population that
will be truthful under all possible intervensions may dissuade an analyst from
universally adopting the cheaper value of $\bf B$ in ongoing studies.

We present the following bounds, for any values of $\bf B$ and $\bf C$ that are
not ancestors of $X$. Note that the distribution of $Y$ under intervention need
not be point-identified. The proof of this proposition is in th appendix.

\begin{proposition}[Upper bound on proportion of truthers]
\begin{align*}
P(truthers) \le
  \sum_y \bigg(
  1 - \sum_{(y_1, y_2) \neq (y, y)} \max \xi(Y({\bf b}) = y_1 \land Y({\bf c}) = y_2)
  \bigg)
\end{align*}
\end{proposition}

\end{document}
