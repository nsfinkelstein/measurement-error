\documentclass[main.tex]{subfiles}

\begin{document}

\section{Proofs}

\subsubsection*{Proof of Proposition 1}

First, the constraints places on $P(\mathbf{A},\mathbf{B})$ by fixing the
marginal $P(\mathbf{A},\mathbf{B_O})$ are trivially linear. Next, as simple
consequence of Proposition 5 of \citet{evans2016margins}, whenever two random
variables $A$ and $B$ in a graph $\mathcal G$ share a common unobserved parent,
and $A$ has no other parents, the model corresponding to the graph is unchanged
by removing the common parent and adding an edge from $A$ to $B$ if one does
not already exist. As a result, a graph $\mathcal G'$ obtained by applying
Procedure \ref{proc:randomize} to any general IV graph $\mathcal G$ will
represent the same model as $\mathcal G$.

In $\mathcal G'$, each instrument is randomized with respect to its child in
$\bf B$. $\Lambda$ can be thought of as a selector such that each variable in
$\bf B$ takes a value determined by $\Lambda$ and its parents in $\bf \{A,
B\}$. Consider the \textit{equivalence class} formed by all values of $\Lambda$
that lead to the same settings of $\bf B$ for each setting of $\bf A$ of
$[\lambda] = \{\lambda' : P(B \mid \Lambda = \lambda, A=a) = P(B \mid \Lambda =
\lambda', A=a)~~~ \forall ~~ a)$. Then, by \citep{fine}, when variables are
discrete, the conditional distributions $P({\bf B \mid A})$ can be represented
linear combinations of parameters. See Appendix A of \citet{chaves2017bell} for
a common representation of these linear constraints, in which the linear
combination is of probabilities that $\Lambda$ takes values in various
equivalence classes. Note that is equivalent to the response function
variable representation we use as a joint setting of the response function
variables indicates membership in a particular equivalence class of $\Lambda$.
Finally, because $P(\mathbf{A},\mathbf{B_O})$, and by extension
$P(\mathbf{A})$, is known, $\mathcal{G}'$ places linear constraints on
$P(\mathbf{A},\mathbf{B})$.

% Suppose that all possible assignments for $\bf A$ are denoted by $\{{\bf a}_1,
% \dots, {\bf a}_N\}$. To link the distribution over equivalence class of
% $\Lambda$ to $\psi$, we observe that all values of $\Lambda$ in the same
% equivalence class will have ${\bf B}({\bf a}_i)$ take the same values for any
% $i$. Therefore the probability that $\Lambda$ takes a value in the equivalence
% class in which each instrument assignment ${\bf a}_i$ leads to ${\bf B = b}_i$
% can be expressed as $P({\bf B(a}_i) = {\bf b}_i~~\forall~i)$. By the same
% argument used in the proof of Proposition \ref{prop:marginalization}, this
% probability is linear in $\psi$, concluding the proof.

\subsubsection*{Proof of Proposition 2}

First, we recall that any graph $\mathcal G$ in general instrumental variable
model is equivalent to $\mathcal G'$ in which $P({\bf \tilde B = \tilde b, A}) =
P({\bf A})\psi_{\bf \tilde b}$. Then, 
%
\begin{align*}
  P({\bf Z(t) = z})
  &= \sum_{\bf v:v_Z=z} P({\bf B(t) = v_B} \mid {\bf A = v_A}) P({\bf A = v_A})\\
  &= \sum_{\bf v:v_Z=z} P({\bf B(t, v_{V \setminus T}) = v_B} \mid {\bf A = v_A}) P({\bf A = v_A})\\
  % \label{eq:prop2}
  &= \sum_{\bf v:v_Z=z} P({\bf B(t, v_{V \setminus T}) = v_B}) P({\bf A = v_A}).
\end{align*}
%
The second equality is by the causal consistency assumption, and the last one is
by the independence property of $\mathcal G'$. Next, we note that intervention
on variables other than the parents of $B$ is irrelevant given intervention on
its parents, yielding
\begin{align*}
  P({\bf B(t, v) = v_B}) =
  P({B({\bf t}_{Pa(B)}, {\bf v}_{Pa(B) \setminus {\bf T}}) = {\bf v}_B} : B \in {\bf B}).
\end{align*}
Finally, by definition of $\mathbf{\tilde{B}}$, each $B({\bf t}_{Pa(B)}, {\bf v}_{Pa(B) \setminus {\bf T}})$ is in $\bf
\tilde B$, such that $P({B({\bf t}_{Pa(B)}, {\bf v}_{Pa(B) \setminus {\bf T}}) =
  v_b} : B \in {\bf B})$ is simply the probability $\bf \tilde B$ takes a value
$\bf \tilde b$ in which $B({\bf t}_{Pa(B)}, {\bf v}_{Pa(B) \setminus {\bf T}}) =
{\bf v}_B$ for all $B \in \bf B$, concluding the proof.

\subsubsection*{Proof of Proposition 6}

Following the approach in Balke and Pearl, we derive bounds on $\pi$ by
translating our assumptions into constraints on $\pi$ and then find sharp
upper (lower) bounds by maximizing (minimizing) $\pi_1$ subject to these
constraints. We begin with the equality

\begin{align}
  \label{eq:pi}
    \pi_1 = \frac{p_1 - q_{1|0}}{q_{1|1} - q_{1|0}}.
\end{align}

This function is discontinuous at $q_{1|1} = q_{1|0}$ (which is disallowed
by assumption), but continuous above and below this line. To derive the
bounds in Proposition \ref{prop:binary_bounds}, we take the union of the sharp
bounds when $q_{1|1} > q_{1|0}$ and when $q_{1|1} < q_{1|0}$. Consider first
the case when $q_{1|1} > q_{1|0}$. For each value $a$ of $A$ we have

\begin{align*}
    p_{1|a} = q_{1|0}(1-\pi_{1|a}) + q_{1|1}\pi_{1|a}
\end{align*}

Combining this with Equation \ref{eq:pi}, we can find the sharp upper
bound by solving the following (non-linear) optimization problem:

\begin{align*}
    \max_{q_{1|1} > q_{1|0}} \quad & \frac{p_1 - q_{1|0}}{q_{1|1} - q_{1|0}}\\
    \textrm{s.t.} \quad & p_{1|a} = q_{1|0}(1-\pi_{1|a}) + q_{1|1}\pi_{1|a}\,\,\,\forall a\\
    & 0 \leq q_{1|0},q_{1|1},\pi_{1|a} \leq 1\,\,\,\forall a
\end{align*}

To solve this optimization problem, we will fix $q_{1|1}$ and optimize with
respect to $q_{1|0}$ and then optimize the resulting function with respect
to $q_{1|1}$. That is, let

\begin{align*}
    g(q_{1|1}) = \max_{q_{1|0}} \quad & \frac{p_1 - q_{1|0}}{q_{1|1} - q_{1|0}}\\
    \textrm{s.t.} \quad & p_{1|a} = q_{1|0}(1-\pi_{1|a}) + q_{1|1}\pi_{1|a}\,\,\,\forall a\\
    & 0 \leq \pi_{1|a} \leq 1\,\,\,\forall a\\
    & 0 \leq q_{1|0} < q_{1|1}
\end{align*}

In this case, all constraints are satisfied so long as $0 \leq q_{1|0} \leq
\min_a p_{1|a}$ and the maximum is achieved when $q_{1|0} = 0$. Thus,
$g(q_{1|1}) = \frac{p_1}{q_{1|1}}$. Next, we solve

\begin{align*}
    \max_{q_{1|1}} \quad & g(q_{1|1}) = \frac{p_1}{q_{1|1}}\\
    \textrm{s.t.} \quad & p_{1|a} = q_{1|1}\pi_{1|a}\,\,\,\forall a\\
    & 0 \leq \pi_{1|a} \leq 1\,\,\,\forall a
\end{align*}

In this case, all constraints are satisfied so long as $\max_a p_{1|a} \leq
q_{1|1} \leq 1$ and the maximum value that satisfies this constraint is
$\frac{p_1}{\max_a p_{1|a}}$. Applying similar reasoning to the minimization
problem, we get a minimum value of $\frac{p_1 - \min_a p_{1|a}}{1 - \min_a
  p_{1|a}}$. Thus, when $q_{1|1} > q_{1|0}$, we have the following sharp
bounds on $\pi_1$ 

\begin{align}
    \frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}} \leq \pi_1 \leq \frac{p_1}{\max_a p_{1|a}}
\end{align}

Finally, we repeat this derivation for $q_{1|1} < q_{1|0}$ and take the
union of these two sets of bounds to get the bounds in Proposition
\ref{prop:binary_bounds}.

% \begin{proof}[Proof of lower bound of proportion affected, Proposition \ref{prop:proportion-affected}]
% From the exclusion restriction, it follows that: 

% \[\bf Y(a) \neq Y(b) \implies X(a) \neq X(b)\]

% Therefore the following constraint is implied by the causal model:

% \[P({\bf Y(a) \neq Y(b)}) \le P({\bf X(a) \neq X(b)})\]

% By definition, 
% Then we denote the Wasserstein distance between the distributions of $Y(a)$ and
% $Y(b)$ as $\omega(Y(a), Y(b))$, and we note that by construction.

% \[\omega({\bf Y(a), Y(b)}) \le P({\bf Y(a) \neq Y(b)})\]

% This yields the following lower bound on the proportion of the population who
% take different values under the two interventions

% \[\omega(Y(a), Y(b)) \le P({\bf X(a) \neq X(b)})\]
% \end{proof}

\end{document}
