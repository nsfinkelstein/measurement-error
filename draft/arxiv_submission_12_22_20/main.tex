\documentclass[twoside]{article}

% \usepackage{aistats2021}
% If your paper is accepted, change the options for the package
% aistats2021 as follows:
%
\usepackage{aistats_arxiv}
%
% This option will print headings for the title of your paper and
% headings for the authors names, plus a copyright note at the end of
% the first column of the first page.

% If you set papersize explicitly, activate the following three lines:
%\special{papersize = 8.5in, 11in}
%\setlength{\pdfpageheight}{11in}
%\setlength{\pdfpagewidth}{8.5in}

% If you use natbib package, activate the following three lines:
%\usepackage[round]{natbib}
%\renewcommand{\bibname}{References}
%\renewcommand{\bibsection}{\subsubsection*{\bibname}}

% If you use BibTeX in apalike style, activate the following line:
%\bibliographystyle{apalike}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfiles}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage{tikz}
\usepackage{centernot}
\usepackage{framed}
\usepackage{authblk}

\usetikzlibrary{shapes,backgrounds}

% \usepackage{xr}
% \externaldocument{appendix}

\usepackage{algorithm}
\usepackage{algorithmicx}

\usepackage{amsthm}       % hyperlinks
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{corollary}{Corollary}
\newtheorem{proposition}{Proposition}
% \newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{assumption}{Assumption}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}


\begin{document}

% If your paper is accepted and the number of authors is large, the
% style will print as headings an error message. Use the following
% command to supply a shorter version of the authors names so that
% they can be used as headings (for example, use only the surnames)
%
%\runningauthor{Surname 1, Surname 2, Surname 3, ...., Surname n}

% \twocolumn[

\title{Partial Identifiability in Discrete Data With Measurement Error}

% \aistatsauthor{ Noam Finkelstein\footnote{Equal Contribution} \And Roy Adams \And  Suchi Saria \And Ilya Shpitser }

% \aistatsaddress{Johns Hopkins University \\ noam@jhu.edu \And Johns Hopkins University \\ radams47@jhu.edu \And Johns Hopkins University \\ \& Bayesian Health \And Johns Hopkins University}

\author[1]{Noam Finkelstein\thanks{Equal Contribution}}
\author[1]{Roy Adams$^{*}$}
\author[1,2,4]{Suchi Saria}
\author[1,3]{Ilya Shpitser}
\affil[1]{\footnotesize Department of Computer Science, Johns Hopkins University}
\affil[2]{\footnotesize Department of Applied Mathematics and Statistics, Johns Hopkins University}
\affil[3]{\footnotesize Department of Biostatistics, Johns Hopkins University}
\affil[4]{\footnotesize Bayesian Health}
\date{}

\maketitle

% ]
% \twocolumn

\begin{abstract} 
	
  When data contains measurement errors, it is necessary to make assumptions
  relating the observed, erroneous data to the unobserved true phenomena of
  interest. These assumptions should be justifiable on substantive grounds, but
  are often motivated by mathematical convenience, for the sake of exactly
  identifying the target of inference. We adopt the view that it is preferable
  to present bounds under justifiable assumptions than to pursue exact
  identification under dubious ones. To that end, we demonstrate how a broad
  class of modeling assumptions involving discrete variables, including common measurement error and
  conditional independence assumptions, can be expressed as linear constraints
  on the parameters of the model. We then use linear programming techniques to
  produce sharp bounds for factual and counterfactual distributions under
  measurement error in such models. We additionally propose a procedure for
  obtaining outer bounds on non-linear models. Our method yields sharp bounds in
  a number of important settings -- such as the instrumental variable scenario
  with measurement error -- for which no bounds were previously known.
\end{abstract}

% Introduction
\input{intro}

% Problem setup and definitions
% \subfile{setup}

% Bounds on the marginal of the unobserved variable with no other assumptions
\input{base}

% Bounds on the marginal when there is an auxiliary variable
\input{iv}

% Bounds on the causal effect
\input{causal-bounds}

% \subfile{symbolic-bounds}

% \subfile{examples}

% Application to IV with measurement error on outcome
% \subfile{application}

% Background
\input{background}

% Conclusion
\input{conclusion}

% \section*{Broader impact} 

% We believe this work will be of practical use to scientists with imperfectly
% measured data, who cannot credibly impose sufficiently strong assumptions to
% obtain full identification. Our results allow for substantive inference in the
% absence of such assumptions. We believe these results will especially be of use
% to public health officials and social scientists, who rely on surveys and other
% means of self-reporting, which is especially susceptible to arbitrary
% measurement error. We also believe our view of measurement error --- as
% inference on a latent variable with known cardinality and one or more proxies
% in a causal model --- will help drive a more general theory of non-parametric
% partial identification of the distributions of variables with measurement
% error. We do not believe that there is much risk that this work will have a
% negative social impact except insofar as any statistical technique can be
% misused.

% \begin{ack}
% \end{ack}
\bibliographystyle{plain}
\bibliography{references}

\appendix
\onecolumn
\input{prop_1_proof}
\input{combinations_with_markov}
\input{prop_2_proof}
\input{iv_lp}
\input{causal_lp}


\end{document}
