% \documentclass[main.tex]{subfiles}
%
% \begin{document}

\section{Graphical constraints}
\label{sec:iv}

In the previous section we relied on domain knowledge about the joint
distribution of $X$ and its proxy $Y$. In this section, we describe how
assumptions encoded in a graphical model can be used to further constrain our
target parameter. In particular, we describe a class of graphs that result in
linear constraints on the target parameter and, thus, is amenable to the linear
programming approach introduced in the previous section. This class includes the common instrumental variable (IV) model, shown in Figure \ref{fig:iv} (a), as well as the various extensions of this model shown in Figure \ref{fig:gen_iv}.

Suppose that we assume a latent variable Bayesian network $\mathcal{G} =
(\mathbf{V},\mathbf{E})$, where $\mathbf V$ and $\mathbf E$ represent the
vertices and edges of the network, respectively. For a variable $V \in
\mathbf{V}$, let $Pa_\mathcal{G}(V)$ be the parents of $V$ in $\mathcal{G}$
and $Ch_\mathcal{G}(V)$ be the children of $V$ in $\mathcal{G}$. Additionally,
we refer to the set of observed and unobserved variables with known cardinality
as \textbf{endogenous variables}, denoted by $\mathbf{O}$, and the set of
unobserved variables with unknown cardinality (typically latent confounders) as
\textbf{exogenous variables}, denoted by $\mathbf{U}$. For example, consider
the Bayesian network in Figure \ref{fig:iv} (a) in which $A$, $X$, and $Y$ are
endogenous and $U$ is exogenous. In this model $A$ is commonly referred to as
an \emph{instrumental variable} (IV), and the model is referred to as the IV
model. The independencies encoded in this graph, namely $A \perp U$ and $A
\perp Y \mid A,U$, place constraints on the joint distribution $P(A,U,X,Y)$
which, in turn, places constraints on the target parameter. We refer to the
target parameter given by a graphical model as \textbf{graphical constraints}. As before, our goal is to maximize and minimize the target parameter $\psi$ subject to these constraints. 

In general, the independence constraints imposed by a Bayesian network on the
joint distribution of variables in the graph are non-linear. For example, the
simple Markov chain model shown in Figure \ref{fig:iv} (b) yields quadratic
constraints on $P(A,X,Y)$ (see Proposition \ref{prop:binary_bounds} for more
details). This makes optimizing over the constraint set difficult as, in
general, quadratic programming is NP-hard \cite{pardalos1991quadratic}.
Further, the complete set of latent variable graphical models that impose
linear constraints on $P$ is unknown; however, in the remainder of this
section, we describe a class of graphs, including commonly used graphs such as
the IV model, that do yield linear constraints. We will proceed by first illustrating how the basic IV model produces linear constraints on $P$ and then generalizing this result to a class of graphs using results from \cite{fine} and \cite{evans}.

\subsection{Constructing linear constraints from the IV model}
To construct this class, we start by considering the IV model shown in Figure
\ref{fig:iv} (a) which is known to place linear constraints on $P$
\cite{balke93ivbounds,bonet2001instrumentality}. In order to arrive at linear
constraints, we will not optimize directly over the joint distribution $\phi =
P(A,U,X,Y)$ as we did in Section \ref{sec:base}. Instead, we will optimize over
an equivalent potential outcome distribution. Recall that a potential outcome
variable $X(a)$ represents the value $X$ would have taken had we intervened to
set $A = a$. Then, let $\tilde{X} = (X(a))_{a \in \mathcal{A}}$ and $\tilde{Y}
= (Y(x))_{x \in \mathcal{X}}$ be the vectors of potential outcome variables for
$X$ and $Y$ given their endogenous parents. Finally, let $\psi$ be the joint
distribution over $\tilde{X}$, and $\tilde{Y}$ such that
$\psi_{\tilde{x},\tilde{y}} = P(\tilde{X}=\tilde{x},\tilde{Y}=\tilde{y})$.

Under the consistency assumption, $\psi$ is connected to the distribution over
endogenous variables by the linear map $P(x, y \mid a) = P(X(a) = x, Y(x) =
y)$, where the last term is obtained by marginalizing all other variables in
$\tilde{X}$ and $\tilde{Y}$ out of the distribution $\psi$. For an explicit
example of this marginalization, see Section \ref{app:iv_lp} in the
Supplement. As observed in \cite{bonet2001instrumentality} and
\cite{balke93ivbounds}, all independencies in IV graph are now given by $A
\perp \tilde{X}, \tilde{Y}$ which can be written as
%
\begin{align}
\psi_{a,\tilde{x},\tilde{y}} = P(A=a)\sum_{a'} \psi_{a',\tilde{x},\tilde{y}}\,\,\,\forall\,a,\tilde{x},\tilde{y},
\end{align}
%
which is linear in $\psi$ since $P(A)$ is identified from the data. As
$\mathcal{G}$ imposes no other constraints on $\psi$, and marginalization is a
linear operation, all constraints on the conditional distribution $P(x, y \mid
a)$ are similarly linear.

\subsection{Graphs involving multiple instruments}
This linearity result can be generalized to more complex graphs
involving multiple instruments using the following proposition adapted
from \cite{fine}:

\begin{proposition}[Fine's Theorem] \label{prop:general_iv}
	%
	Let $\mathcal{G} = (\mathcal{V},\mathcal{E})$ be a latent variable Bayesian
  network. Suppose there exists an exogenous latent variable $\Lambda \in
  \mathcal{V}$ such that (1) all descendants of $\Lambda$ are children of
  $\Lambda$ and (2) all non-descendants of $\Lambda$ are observed, have exactly
  one child, and that child is in $Ch_\mathcal{G}(\Lambda)$. Let the children of
  $\Lambda$ be denoted by $\bf X$, and the non-descendants of $\Lambda$ by $\bf
  A$. Then all constraints imposed by $\mathcal G$ on $P({\bf X} \mid {\bf A})$
  are linear.
	%
\end{proposition}

In such a graph, all variables in $\bf X$ are mutually confounded by $\Lambda$
and we refer to the variables in $\bf A$ as \textbf{instruments}. Note that
this class of graphs trivially includes the basic IV model (Figure \ref{fig:iv}
(a)) but also extends the basic IV model in two important ways. First, we can
now include multiple instruments as shown in Figure \ref{fig:gen_iv} (a) (e.g.,
see \cite{angrist1991does,poderini2020exclusivity}). Second, we can include
instruments for both $X$ and its proxy $Y$, which may occur when some aspect of
the measurement process is randomized, such as the order of responses in a
survey or the gender of an in-person surveyor as shown in Figure
\ref{fig:gen_iv} (b) (e.g., see \cite{becker1995effect,catania1996effects}).

% We can write
% distributions in this model as $P({\bf X} \mid {\bf A})P({\bf A})$. In our
% settings, $P({\bf A})$ is assumed to be known since $\bf A$ is observed, so
% constraints on the joint distribution of instruments imposed by the model will
% not play a role in bounding our targets of inference. It follows that all
% relevant constraints imposed by the model are linear.
%
% Examples of graphs in this class include the IV model in Figure \ref{fig:iv}
% (a), as well as the models shown in Figures \ref{fig:gen_iv} (a) and (b). In
% particular, the model shown in Figure \ref{fig:gen_iv} (a) represents a
% scenario where we have separate instruments, $A$ and $B$, for the true
% measurement $X$ and the noisy proxy $Y$.

To derive the set of linear constraints imposed by such a graph, we can
generalize the procedure described for the IV model as follows. Let $K$ be the
number of variables in ${\bf X} = Ch(\Lambda)$. Then, for each variable $X \in
\mathbf{X} = (X_i)_{i=1}^{K}$, let $\tilde{X}$ be the potential outcomes of $X$
under each joint setting of its endogenous parents and let $\mathbf{\tilde{X}}
= (\tilde{X}_i)_{i=1}^{K}$ be the set of all such potential outcomes.
% Let $\bf \tilde{X}$ denote the potential outcomes of the children of
% $\Lambda$, $\bf X$, under every setting of the instruments $\bf A$.
As before, let $\psi_{\bf a,\bf \tilde x} = P(\bf A = \bf a, \bf \tilde X = \bf \tilde x)$ denote the joint distribution over the instruments and
potential outcomes. Because $P(\mathbf{A})$ is assumed to be known, the only relevant independency imposed by the graph is given by ${\bf A} \perp {\bf \tilde X}$, which can be written as
%
\begin{align}
	\psi_{\bf a,\bf \tilde x} = P(\bf A= \bf a)\sum_{\bf a'} \psi_{\bf a',\bf \tilde x}\,\,\,\forall\,\bf a, \bf \tilde x,
\end{align}
%
which is linear in $\psi$. Finally, $\psi$ can be linearly mapped to the distribution over endogenous
variables $\mathbf{O} = \{\mathbf{A},\mathbf{X}\}$ as $P(\mathbf{O} = {\bf o}) = P({\bf A} = {\bf a}, \tilde{X}_1({\bf
o}_{Pa(X_1)}) = x_1,...,\tilde{X}_K({\bf o}_{Pa(X_K)}) = x_K)$
% $P({\bf X
% = x} \mid {\bf A = a}) = P(X_1(a_{Pa_\mathcal{G}(X_1)}) = x_1,...,X_{|\bf
% X|}(a_{Pa_\mathcal{G}(X_{|\bf X|})}) = x_{|\bf X|})$.
where ${\bf o}_{Pa(X_i)}$ are the values of
$X_i$'s parents in $\bf o$. The right hand side is obtained through
the (linear) marginalization of all other potential outcomes out of $\psi$ and thus the constraints imposed by the model on the distribution of endogenous variables are linear. This class of graphs already contains several useful models, but we will next expand this class further to include graphs with non-randomized instruments.

\subsection{Graphs with non-randomized instruments}

In the basic IV model (Figure \ref{fig:iv} (a)), the instrument $A$ is assumed
to be unconfounded with $X$; however, unconfoundedness is a strong
assumption that does not hold in a variety settings. Instead, we may be willing
to make a relaxed assumption that the confounders for $A$ and $X$ are
independent of the confounders for $X$ and $Y$, as shown in Figure
\ref{fig:gen_iv} (c). 
%
% We now present sufficient conditions for a graph $\mathcal G$ outside
% the class of instrumental graphs describe above to represent a model over
% endogenous variables equivalent to that of a graph $\mathcal G'$ inside the
% class. Any such $\mathcal G$ can be represented by the very same linear
% constraints as $\mathcal G'$, thereby extending the class of graphical models
% corresponding to linear constraints on $P({\bf X} \mid {\bf A})$.
% 
We can extend the class of graphs defined in Proposition \ref{prop:general_iv}
to include confounded instruments using the following special case of
Proposition 5 in \cite{evans}. An alternative proof of this proposition is
presented in the supplement.
% 
\begin{proposition}
\label{prop:confounded_instrument}
%
  Suppose a vertex $A$ in a Bayesian network $\mathcal{G}$ has no observed or
  unobserved parents and has a single child $B$. Then the model for the
  variables in $\mathcal{G}$ is unchanged if an unobserved common parent of $A$
  and $B$ is added to the graph, or if the unobserved common parent is added and
  the edge from $A$ to $B$ is removed.
%
\end{proposition}
%
This proposition has two important consequences: first, instruments in
$\mathbf{A}$ may be confounded with their children and, second, if an
instrument is confounded with its child, it need not have a directed edge to
that child. This result broadens the set of graphical models for which the
constraints on the observed data distribution can be expressed linearly in
$\psi$ to include the graphs such as those shown in Figures \ref{fig:gen_iv}
(c) and (d). In particular, Figure \ref{fig:gen_iv} (d) can be used to represent a model where $A$ is a proxy for the true unobserved instrument.

Once we have expressed the modeling constraints as linear constraints relating
the parameters $\psi$ to the observed data conditional distributions $P({\bf x}
\mid {\bf a})$, we can now proceed exactly as in Section \ref{sec:base},
optimizing with respect to $\psi$ rather than $\phi$. Since $\phi$ is linearly
related to $\psi$, the observed data constraint and all measurement error
constraints from the previous section are still in linear in $\psi$ and can be
composed with the graphical constraints in this section. Alternatively, the
measurement error constraints can be expressed for each potential outcome,
representing a belief that these constraints hold in the observed data as well
as under various interventions.

\begin{figure}[t]
  \centering {\small
    \include{iv-graph}
  }
  \caption{ (a) - (c) show example Bayesian networks. Black variables represent
    observed variables, grey variables represent unobserved variables we wish to
    make inferences on, and red variables represent unobserved variables with
    unknown cardinality. (d) shows a single world intervention graph (SWIG)
    corresponding to graph (c) under the intervention $Z=z$ and $A=a$. }
  \label{fig:iv}
\end{figure}

\begin{figure}[t]
  \centering {\small
    \include{gen-iv-graph}
  }
  \caption{ (a) and (b) are classical instrumental graphs as described in
    Proposition \ref{prop:general_iv}, (c) and (d) are graphs whose constraints
    on the observed data distribution can also be represented linearly by
    Proposition \ref{prop:confounded_instrument}. }
  \label{fig:gen_iv}
\end{figure}

\subsection*{Application: Bounding $E[X]$ in the IV model}

To demonstrate the use of graphical constraints, we will extend our example
from Section \ref{sec:base} to include an additional binary instrument $A \in
\{0,1\}$ and we will assume the graphical model in Figure \ref{fig:iv} (a). For
a complete description of the resulting LP, see Appendix \ref{app:iv_lp}.
Assume also that the observed conditional distributions $P(Y \mid A=a)$ for $a
= 0,1$ are the two marginal distributions shown in Figure \ref{fig:mrs_bounds}
and that $P(A=0) = \frac{1}{2}$. Then, using \emph{only} the constraints
encoded in the graph, we get the numerical bounds $E[X] \in [0.31,4.69]$. These
bounds can be made substantially tighter by including additional measurement
error constraints, but it is worth noting that we can achieve non-trivial
bounds by relying only on graphical constraints. \hfill\qedsymbol

\subsection{Computing bounds for non-linear models}

Unfortunately, many relevant models do not fall into the model class described
above. In this section, we describe how non-sharp outer bounds can be derived
for such cases. The only complete procedure for identifying all constraints
implied by Bayesian networks on the distribution of a subset of their vertices
is an application of quantifier elimination \cite{gieger}, which is infeasibly
slow for many problems. When constraints are known to exist, for example by
Evans' e-separation criterion \cite{evans2012eseparation}, their exact form may
not be known and may not be linear. When constraints are known, but are not
linear, it may be possible to derive sharp bounds analytically. For example,
the following proposition, proven in the supplementary material, gives sharp
bounds for a three variable Markov chain (Figure \ref{fig:iv} (b)) over binary
variables.
%
\begin{proposition}
    \label{prop:binary_bounds}
	%
    Let $X$ and $Y$ be binary variables such that $X \not\independent Y$, let
    $A$ be a discrete variable such that $A \independent Y | X$ and $P(A=a) >
    0$ for all $a$, and let $p_y = P(Y=y)$ and $p_{y \mid a} = P(Y=y\mid A=a)$.
    Then we have the following sharp bounds on $P(X=1)$:
	%
    \begin{align}
	  %       P(X=1) &\in [l,u] \cup [1-u,1-l]\\ \nonumber
	  % l &= \max_a \frac{P(Y=1) - P(Y=1|A=a)}{1 - P(Y=1|A=a)}\\ \nonumber
	  % u &= \min_a \frac{P(Y=1)}{P(Y=1|A=a)}
	  P(X=1) &\in \bigcup_{y\in\{0,1\}} \left[\frac{p_y - \min_a p_{y \mid a}}{1 - \min_a p_{y \mid a}},\frac{p_y}{\max_a p_{y\mid a}}\right]
    \end{align}
\end{proposition}
%
Such analytical bounds, however, are not typically available. In these cases,
\emph{non-sharp} bounds can be derived for any graph by first repeatedly
appealing to Proposition \ref{prop:confounded_instrument} and then adding
adding a latent confounder that meets the criteria of Proposition
\ref{prop:general_iv}. Specifically, any latent variable Bayesian network
$\mathcal{G}$ can be converted to a new graph $\mathcal{G}'$ that meets the
conditions of Proposition \ref{prop:general_iv} through the following steps:
% 
\begin{enumerate}
\item For any latent confounder $U$ with exactly two children $A$ and $B$ such
  that $Pa(A) = \{U\}$ and $Ch(A) = \{B\}$ or $Ch(A) = \emptyset$, add an edge
  from $A$ to $B$ if it does not exist and remove $U$ from the graph.
\item Add a latent confounder $\Lambda$, and an edge from $\Lambda$ to each
  variable $V$ in the graph for which $Pa(V) \neq \emptyset$.
\end{enumerate}
%

\begin{figure}[t]
  \centering {\small
    \include{graph-relaxation}
  }
  \caption{ A non-linear graphical model $\mathcal G$ and an application of the
    linear relaxation procedure, resulting in the linear relaxed model $\mathcal
    G'$. }
  \label{fig:graph_relaxation}
\end{figure}

An example application of this procedure is shown in Figure
\ref{fig:graph_relaxation}. Because modifying the graph according to
Proposition \ref{prop:confounded_instrument} does not change the constraints on
$P({\bf X} | {\bf A})$, Step 1 of this procedure does not change the constraint
set. Further, adding an additional latent confound can only \emph{remove}
independencies from the graph, thus Step 2 represents relaxations of the
constraints on $\psi$. As a result, applying the LP approach to the resulting
model will result in \emph{outer bounds} on the true partial identification set
whose tightness will depend on how many edges were added in steps 2 and 3. In
the following section, we extend our earlier discussion of potential outcomes
to consider partial identification bounds for causal parameters and constraints
relating multiple potential outcomes.

% Bounds obtained through convex optimization are only sharp, then, in models
% where all constraints are known and can be expressed linearly. For example, if
% we assume that $A$, $X$, and $Y$ form a Markov chain, as shown in Figure
% \ref{fig:iv} (b), then the resulting constraints are quadratic and the
% procedure described in this work cannot be used to produce sharp
% bounds\footnote{In fact, no sharp bounds are currently known for this case}.
% In such cases, however, it may be possible to compute non-sharp bounds by
% relaxing certain graphical constraints. Continuing our example, we can compute
% non-sharp bounds for Figure \ref{fig:iv} (b) by relaxing the model to Figure
% \ref{fig:iv} (b). In the following section, we extend our discussion of
% potential outcomes to consider partial identification bounds for causal
% parameters and constraints relating multiple potential outcomes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uncomment to include the markov chain case
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subfile{markov_chain}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





% \subsubsection{Combining Theorem \ref{thm:binary_bounds} with other common
% assumptions}
%
% In the presence of additional assumptions, the bounds in Theorem
% \ref{thm:binary_bounds} can be further refined. In this section we present a
% few such refinements for common measurement error assumptions.
%
% \begin{corollary}[Bounded misclassification probability]
%   If, in addition to the assumptions made in Theorem \ref{thm:binary_bounds},
%   we assume $p(Y=1|X=0) + p(Y=0|X=1) < 1$, we have the following sharp bounds
%
%     \begin{align*}
%         \pi_1 \in \left[\frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}},\frac{p_1}{\max_a p_{1|a}}\right]
%     \end{align*}
%   \end{corollary}
%
% \begin{corollary}[Unidirectional errors]
%   If, in addition to the assumptions made in Theorem \ref{thm:binary_bounds},
%   we assume $p(Y=1|X=0) = 0$, we have the following sharp bounds
%
%     \begin{align*}
%         \pi_1 \in \left[p_1,\frac{p_1}{\max_a p_{1|a}}\right]
%     \end{align*}
%   \end{corollary}
%
% \begin{corollary}[Label independent noise]
%   If, in addition to the assumptions made in Theorem \ref{thm:binary_bounds},
%   we assume $p(Y=1|X=0) = p(Y=1 | X=1)$, we have the following sharp bounds
%
%     \begin{align*}
%       \pi_1 \in \left[
%       p_1,\frac{p_1 - p^*}{1 - 2p^*}
%       \right] \cup \left[
%       1-\frac{p_1 - p^*}{1 - 2p^*},1-p_1
%       \right]
%     \end{align*}
%
%     where $p^* = \min \left\{\min_a p_{1|a},1-\max_a p_{1|a}\right\}$.
%   \end{corollary}

%   TODO: Re-introduce entropic bounds section
%   \subsubsection{Entropic Bounds in Non-binary case}
%
%   In the non-binary case, it is much more difficult to obtain bounds on
%   $P(X=x')$ as solving quadratic programs is, in general, NP-hard [CITATION].
%   However, it is easy to obtain entropy based bounds. We start with the data
%   processing inequality, and obtain an upper bound on $H(A \mid X)$ in terms
%   of the observed data.
%
% \begin{align}
%   I(A:X) &\ge I(A:Y)\\
%   H(A) - H(A \mid X) &\ge I(A:Y)\\
%   H(A) - I(A:Y) &\ge H(A \mid X)
% \end{align}
%
% The left hand side is a function of the observed data distribution. This
% constraint says the entropy of $A$ conditional on $X$ has to be smaller than
% some value. Intuitively, if the value of the left hand side is small, there
% cannot be a lot of entropy left in $A$ once we know $X$, i.e. $X$ must be
% highly predictive of $A$. This means that, modulo the label-switching issue,
% the distributions of $A$ and $X$ must be fairly similar. If, on the other
% hand, the left hand side of the above is large, than the distribution of $X$
% need not be similar to that of $A$.
%
% We note that these results are all valid if $A$ is a child of $X$ rather than
% a parent, so these bounds may be used when a latent variable has two
% measurements. This is because both graphs imply the same statistical
% properties, and no counterfactual reasoning is used in the derivation of these
% bounds.

% \end{document}
