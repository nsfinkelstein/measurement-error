% \documentclass[main.tex]{subfiles}
%
% \begin{document}
	
\section{The basic measurement error problem and the LP formulation}
\label{sec:base}

Suppose that we are interested in the distribution of a discrete random
variable $X \in \mathcal{X}$, but instead of observing $X$ directly, we can
only observe a discrete noisy proxy, denoted $Y \in \mathcal{Y}$. Clearly,
without any assumptions about the proxy distribution, $P(Y|X)$, we cannot say
anything about the distribution of $X$. On the opposite extreme, if $P(Y|X)$ is
known and invertible, then $P(X)$ is fully identifiable from observations of
$Y$ \cite{rothman2008modern}.

% In the first setting we consider, we assume that there is no known
% relationship between proxy variables. To simplify the presentation in this
% setting, we first demonstrate the method assuming that there is only a single
% proxy variable $Y$. Clearly, without any assumptions about the noise
% distribution, $P(Y|X)$, we cannot say anything about the distribution of $X$.
% On the opposite extreme, it has been shown that if $P(Y|X)$ is known and
% invertible, then $P(X)$ is fully identifiable from observations of $Y$
% \cite{rothman2008modern}.

Our interest is in between these two extremes, where we assume certain
properties of $P(Y|X)$, but not the whole distribution. Our goal is to bound
functions of $P$, referred to as \emph{parameters of interest}, under these
assumptions. Our approach is to translate the modeling assumptions into a set
of constraints on $P(X,Y)$ and bound the parameter of interest by finding its
maximum and minimum subject to these constraints. Formally, let $\Delta^{d}$ be
the $d$-dimensional simplex, let $\eta: \Delta^{|\mathcal{X}|\times |\mathcal{Y}|} \to \mathbb{R}$
be the parameter of interest, and let $\mathcal{M} \subseteq
\Delta^{|\mathcal{X}| \times |\mathcal{Y}|}$ be the set of distributions
allowed under the modeling assumptions. Then, assuming that $\mathcal{M}$
contains the true distribution $P_0$, we can bound $\eta(P_0)$ as
%
\begin{align}
	\min_{P\in\mathcal{M}} \eta(P) \leq \eta(P_0) \leq \max_{P\in\mathcal{M}} \eta(P)
\end{align}
%
We will show how to construct $\mathcal{M}$ such that this optimization problem
is tractable and the bounds produced are sharp.

%
% In order for the eventual
% constrained optimization problem to be doubly convex, we must be able to express
% the target of inference as an \textbf{objective} that is linear in $\phi$. In
% this case, that is easy to do, as $\sum_y \phi_{xy}$.
For notational simplicity, let $\phi_{xy} := P(X = x, Y = y)$ be the joint
distribution of $X$ and $Y$. By construction, $\phi$ must satisfy the the
\textbf{probability constraints} $\sum_{xy} \phi_{xy} = 1$, and for all $x, y$,
$\phi_{xy} \ge 0$. In addition, $\phi$ must match the marginal for the observed
proxy $Y$, so that for all $y$, $\sum_{x} \phi_{xy} = P(Y=y)$. These are called
the \textbf{observed data constraints}.

As mentioned above, we must make assumptions about the relationship of the
unobserved variable $X$ and the observed proxy $Y$. We seek to avoid strong,
parametric assumptions, in favor of weaker assumptions that may be justified by
expert knowledge or domain research. Below, we provide examples of several such
commonly made assumptions. We call constraints on $\phi$ relating the proxies
to the unobserved variables the \textbf{measurement error constraints}
\footnote{ Each of these constraints can easily be soften by adding a slack
parameter which can, in turn, be varied in a sensitivity analysis. }.

% TODO: relabel these so as not to overlap with later sections
\begin{enumerate}
  \item[(A0)] Bounded error proportion:
    $\sum_{x \neq y} \phi_{xy} \leq \epsilon$
  \item[(A1)] Unidirectional errors:
    $\sum_{y < x} \phi_{xy} = 0$
  \item[(A2)] Symmetric error probabilities:\\
    $~~~~~\phi_{xy} = \phi_{xy}\,\,\,\forall\,\,|x - y| = |x - y'|$
  \item[(A3)] Error probabilities decrease by distance:\\
    $~~~~~\phi_{xy} \geq \phi_{xy'}\,\,\,\forall\,\,|x - y| > |x - y'|$
\end{enumerate}

Assumption (A0) may be reasonable when there is sufficient previous literature to specify a range of plausible error rates, but not the exact proxy distribution (e.g., see discussion of sensitivity analysis in \cite{rothman2008modern}). Assumption (A1) may be used to represent positive label only data, which is common in areas such as ecology \cite{solymos2012conditional} and public health \cite{adams2019learning}. Assumption (A2) represents a generalization of the zero-mean measurement error assumption -- commonly made in settings where the errors are due to imprecision of the instrument -- to discrete variables. Finally, Assumption (A3) may be reasonable in settings where the values of $X$ have a natural ordering and small errors are more likely than large ones.

Note that the target distribution $P(X=x) = \sum_{y} \phi_{xy}$ and all
constraints described thus far can be written as linear functions of $\phi$. If
our parameter of interest is also a linear function of $\phi$, then the assumed
constraints, in combination with the linear objective corresponding to our
parameter of interest, form a linear program (LP) and bounds can be computed by
both maximizing and minimizing the parameter of interest using one of a number
of LP solvers. Parameters that are linear in $\phi$ include $P(X)$ and all of
its moments. By the intermediate value theorem, the objective can take any
value between its constrained maximum and minimum values. It follows that for
sets of assumptions that can be linearly expressed, the bounds obtained through
this linear program are sharp, i.e. they exhaust the information in the
observed data under the stated assumptions.

\begin{figure}[t]
    % \centering
	\begin{subfigure}[b]{0.49\textwidth}
			\includegraphics[width=\textwidth]{figures/mrs_exp.pdf}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
			\includegraphics[width=\textwidth]{figures/mrs_norm.pdf}
	\end{subfigure}\hspace{1cm}
  \caption{
    Bounds on $E[X]$ under different measurement error assumptions and observed
    proxy distributions, $P(Y)$.
  }
  \label{fig:mrs_bounds}
\end{figure}

% \begin{framed}
% \subsection*{Application: Bounding $E[X]$ using a single proxy}
\subsection*{Application: Bounding $E[X]$ using a single proxy}

To demonstrate this approach, suppose we are interested in bounding the
expected value of a variable $X \in \{0,..,5\}$ from observations of $Y \in
\{0,...,5\}$. We will consider bounds under (A2), (A3), and a weaker version of
(A0) wherein there is a known upper bound, $\epsilon = 0.01$, on the proportion
of observations that are mistaken by more than $2$ points (errors of $2$ points
or less are unconstrained by this assumption). The resulting LP is shown below.
%
\begin{align}
\label{eq:base}
\text{objective:~~~~~~~} \sum_{x,y} &x \phi_{xy}\\\nonumber
\text{constraints:~~} \sum_x \phi_{xy} &= P(Y = y) \\\nonumber
 \phi_{xy} & \ge 0 \\\nonumber
 \sum_{|x - y| > 2} \phi_{xy} &\le \epsilon \\\nonumber
 \phi_{xy} &\geq \phi_{xy'} & &
                 \forall |x - y | < |x - y'| \\\nonumber
 \phi_{xy} &= \phi_{xy'} & &
                  \forall |x - y| = |x - y'|
\end{align}
%
For two example proxy distributions, $P(Y)$, Figure \ref{fig:mrs_bounds} shows
the resulting bounds on $E[X]$ under different combinations of (A0), (A2), and
(A3). Bounds under each combination of assumptions were computed by simply
solving a slightly different version of the LP in Equation \ref{eq:base},
highlighting the ease with which we can perform sensitivity analysis without
rederiving bounds under each new model. \hfill\qedsymbol
% \end{framed}

% As we will see in the example below, each of these assumptions can be weakened.
% Equality constraints can be converted to approximation assumptions, by
% specifying a parameter that defines how distant two quantities are permitted to
% be, and inequality constraints can be similarly amended by specifying a
% parameter that defines a unidirectional limit to violation of the inequality.
% These additional parameters can be varied as sensitivity parameters, when the
% sensitivity of inference to these assumptions should be examined.
Were multiple proxies $\mathbf{Y} \equiv \{Y_1,...,Y_K\}$ observed with no
assumptions made about the relationship between them, each proxy would be
subjected to its own observed data constraints and potentially its own
measurement error constraints, depending on what knowledge is available about
the error process. The objective, and each of the other constraints, would then
simply be expressed on the margins of the full distribution $P({X, \bf Y})$,
which maintains linearity.

\subsection{Extending the LP approach to other parameters and models}

In the remainder of this paper, we show how the LP approach described in this
section can be extended to bound other parameters of interest and incorporate
other modeling assumptions. In the Section \ref{sec:iv}, we show how to
incorporate conditional independence assumptions, encoded in a graphical model,
relating $X$ to other observed and latent variables beyond $Y$. We define a
class of latent variable Bayesian networks which produce linear constraints and
show how to relax the constraints imposed by models not in this class to
produce valid outer bounds. In Section \ref{sec:causal-bounds}, we consider
bounds on parameters of the distribution of $X$ under an intervention $A$. In
this setting, our interest is in the potential outcome variable $X(a)$, defined
as the value $X$ would have taken had we intervened to set $A=a$. Our goal,
then, is to bound parameters of the distribution $P(X(a))$, including the
average treatment effect of $A$ on $X$. We show how to bound such parameters
and incorporate additional assumptions relating potential outcomes under
different interventions.

% \end{document}
