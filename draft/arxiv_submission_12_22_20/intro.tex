% \documentclass[main.tex]{subfiles}
%
% \begin{document}

\section{Introduction}

Measurement error is a ubiquitous problem in fields ranging from epidemiology
and public health \cite{rothman2008modern,adams2019learning} to economics
\cite{horowitz1995contaminated,imai2010treatment,molinari2008misclassified} to
ecology \cite{solymos2012conditional,shankar2019three}. If unaccounted for
during analysis, measurement error can lead biased results. Accounting for
measurement error bias requires making assumptions about how errors occur so as
to link the available data to the underlying true values. As such, it is
important to the validity of the analysis and the resulting conclusions that
these assumptions be substantively justifiable.

In practice, however, analysts often make implausibly strong assumptions for
the sake of \emph{exactly identifying} their target of inference. In such
cases, we advocate instead for the use of weaker, credible assumptions to
identify \emph{bounds} on the target of inference, referred to as \emph{partial
identification}. Previous work on partial identifiability under measurement
error has largely focused on deriving analytic bounds for
specific combinations of settings and assumptions. As a result, there remain
many important and common settings for which no known bounds exist.

In this work, we adopt a latent variable formulation of the measurement error
problem. That is, we are interested in estimating a \textbf{target parameter}
that involves the distribution of a discrete unobserved variable $X$ using
observations of a discrete observed proxy variable $Y$. We show how sharp
partial identification bounds for the target parameter can be computed
numerically and without extensive derivations for a class of models, including
several models for which no analytical bounds are currently known. Our
approach, which is similar in spirit to that of \cite{balke93ivbounds}, is to
encode the target parameter and constraints imposed by the model as a
linear program which can be maximized and minimized to produce sharp bounds. We
show that this approach can be used to compute bounds for factual and
counterfactual parameters of a discrete mismeasured variable, including its
marginal distribution and its moments, and the average treatment effect (ATE)
of an intervention on the mismeasured variable.

The primary contribution of this paper is a collection of modeling assumptions
that can be encoded as linear constraints on a target parameter and, thus, are
amenable to the linear programming approach. These include several common
\textbf{measurement error} constraints (Section \ref{sec:base}),
\textbf{graphical} constraints encoded by hidden variable Bayesian networks
(Section \ref{sec:iv}), and \textbf{causal} constraints relating potential
outcomes under different interventions (Section \ref{sec:causal-bounds}). Our
main result, presented in Section \ref{sec:iv}, draws on results from
\cite{fine} and \cite{evans} to show how an extended class of instrumental
variable-type models produce linear constraints on a target parameter and
present a simple procedure for obtaining outer bounds for graphs outside this
class. 

% Using a result about
% equivalence in hidden variable Bayesian network models \cite{evans}, we expand
% an instrumental-style class of graphs known to have linearly-constrained
% conditional distributions \cite{fine} to include graphs with non-randomized
% instruments. Additionally, we present a simple procedure for for obtaining
% outer bounds for graphs outside this class.
% Sharp bounds in the linear
% super-model will be valid outer-bounds in the original model.

Our approach allows a practitioner to mix and match any combination of the
modeling assumptions described in this paper with little effort. This
flexibility means that it is trivial to compute bounds under new models and to
test the sensitivity of the resulting bounds to different combinations of
assumptions. Throughout, we demonstrate the utility of this approach on
synthetic examples representing important use cases. Our goal in these examples
is to demonstrate how to apply the approach to commonly occurring models so that
practitioners may adapt them to their specific data scenario. These cases
include:

\begin{enumerate}
\item Bounding the mean of a variable using a single noisy proxy
\item Bounding the mean of a variable using two noisy proxies with an exclusion
  restriction
\item Bounding the ATE in a randomized trial with measurement error on the
  outcome
\item Bounding the ATE in an instrumental variable model with measurement error
  on the outcome
\end{enumerate}

These cases represent common scenarios occurring in economics, public health,
political science, and other scientific settings. Among these, cases 2, 3, and 4
all represent settings for which no sharp symbolic bounds are currently known
and previous work suggests that deriving such symbolic bounds automatically for
these settings is infeasible \cite{bonet2001instrumentality}. We begin by
introducing our approach in the context of the simplest possible measurement
error model (Section \ref{sec:base}). Next, we describe a class a class of graphical models that yield linear constraints

% \end{document}