\documentclass[main.tex]{subfiles}

\begin{document}
\section{Causal parameters and constraints}
\label{sec:causalbounds}

Until now, we have focused on bounding functionals of the distribution of full
data distribution $P(\mathbf{A},\mathbf{B})$. In this section, we extend the
linear programming approach to \emph{causal} parameters involving the
distribution of one or more potential outcome variables. Suppose we are
interested in the effect of treatment $\bf T$ on outcome $\bf Z$, i.e. in the
parameters of the potential outcome distribution $P({\bf Z(t))}$. Measurement
error on the outcome, treatment, or observed confounders can all lead to
biased parameter estimates if unaccounted for \citep{rothman2008modern}. In
this section, we show how the constraints presented in the previous sections
can be used to bound causal parameters in the presence of measurement error and
introduce additional linear constraints that apply specifically to causal
inference settings.

Because Procedure \ref{proc:randomize} \emph{does} alter the causal model of a
graph, we cannot use \emph{any} general IV graph, as we did in the previous
section. Instead, to make use of Proposition \ref{prop:marginalization}, we
limit our attention in this section to general IV graphs \emph{with randomized
instruments}. By Proposition \ref{prop:marginalization}, $P({\bf Z(t)})$ is
linear in $\psi$, and thus any linear functional of $P({\bf Z(t)})$ can be
bounded by employing the constraints on $\psi$ described in the previous
sections. This leads directly to the following corollary.
%
\begin{corollary}
  \label{cor:causal}
  %
  For any model $\mathcal{M}$ comprised of a discrete general IV model $\mathcal
  G$ with randomized instruments and any number of linear measurement error
  constraints, sharp bounds may be obtained for any linear function of $P({\bf
  Z(t)})$ by solving an LP.
  %
\end{corollary}
%
A number of important causal parameters can be written as linear functions of
$P({\bf Z(t)})$, most notably the average treatment effect (ATE) defined as
$\mathbb{E}[{\bf Z(t) - Z(t')}]$ and the probability of a non-zero treatment
effect defined as $P({\bf Z(t) \neq Z(t')})$.
%
\begin{remark}
Corollary \ref{cor:causal} makes no mention of whether the variables in $\bf Z$
and $\bf T$ are observed. As a result, sharp bounds on causal
parameters like the ATE can be obtained even when treatment, outcome, or both
are subject to measurement error.
\end{remark}
%
In addition to the graphical and measurement error assumptions discussed so
far, it often makes sense to encode further \textbf{causal assumptions} into
the model. One especially important causal assumption is the causal
\textbf{monotonicity assumption}, which relates potential outcomes under
different interventions. For intervention variable $T$ and potential outcome
$Z(t)$, the general monotonicity assumption can be written as
%
\begin{enumerate}
  \addtolength{\itemindent}{0.25cm}
  \item[(A5)] Monotonicity:
    {\small
    $P(Z(t) = z, Z(t') = z') = 0\,\, \forall t' > t, z' < z$.
    }
\end{enumerate}
%
Assumption (A5) can be applied to cases when it is believed that receiving a
binary treatment cannot decrease the outcome; however, it can also be applied
to the measurement error setting to encode the assumption that increasing in
the true value cannot lead to a decrease in the measurement.
% For
% example, if the true value $X$ is an ordered categorical variable like a Likert
% scale and $Y$ is a self-reported value, we might assume that, all else equal,
% increasing $X$ cannot lead to a lower self-reported value.
Additional causal constraints -- such as limits on the effect size or the
proportion affected, or the assumption of decreasing returns to treatment --
may be similarly imposed. As with the measurement error assumptions, these
equality constraints can be relaxed by specifying that the sums are bounded
from above, rather than identically equal to zero.

% Many other assumptions can also be encoded linearly, such as the
% \textbf{decreasing returns to treatment} assumption, stated below. 
% \begin{enumerate}
%   \item[(A6)] 
%     $P(Z(t') - Z(t) < Z(t'') - Z(t') ) = 0\,\,~ \forall~t'' > t' > t$.
% \end{enumerate}

% % In order to use the constraints from the previous sections to bound
% % these parameters, we first need to show how the distribution $\psi$, defined in
% % the previous section, can be linearly mapped to $P({\bf B(t)})$. Assume a latent
% % variable Bayesian network $\mathcal{G}$ that meets the conditions of Definition
% % \ref{def:gen_iv} and the corresponding joint distribution $\psi$ over potential
% % outcomes $\mathbf{\tilde{X}}$ is defined as in Section \ref{sec:iv}. Then, for
% % an arbitrary outcome variable $B \in \mathbf{A} \cup \mathbf{B}$, treatment
% % variable $T \in \mathbf{A} \cup \mathbf{B}$, and value $t$, we can calculate the
% % distribution over $B(t)$ as in a structural equation model, by intervening on
% % $T$ in the graph and repeatedly appealing to the consistency assumption to
% % marginalize out all variables other than $B(t)$ \cite{pearl2009causality}. For
% % example, assuming the classic IV model in Figure \ref{fig:iv} (a), the
% % distribution $P(X(a))$ is just a marginal of $\psi$ and the distribution
% % $P(Y(a))$ can be derived as

% % In the previous section, we used potential outcomes to reason about the
% % distribution of a mismeasured variable $X$. Suppose instead that we observe a
% % treatment variable $T$ and are directly interested in the potential outcome
% % $X(t)$ under some intervention $T = t$. As before, we do not observe $X$, but
% % instead observe a proxy $Y$.
% % % In this setting, our goal is to obtain bounds for
% % % parameters involving the potential outcome distribution $P(X(t))$ for various
% % % settings $t$ of $T$.
% % This scenario is common in fields like economics and epidemiology, in which the
% % treatment is exactly known, but the outcome is measured through inexact tools
% % such as surveys. In this section, we will show how the constraints presented in
% % the previous sections can be applied to target parameters involving the
% % distribution of $X(t)$ and will introduce additional constraints that apply
% % specifically to causal inference settings. We demonstrate this approach in two
% % important settings: a clinical trial with measurement error on the outcome, and
% % an IV model with measurement error on the outcome.

% \subsubsection*{Causal parameters}

% Proposition \ref{prop:marginalization} demonstrates that $P({\bf B(t)})$ is a
% linear functional of $\psi$ for any general IV model. Several useful causal parameters can be written as
% linear functions of $P({\bf B(t)})$, most notably the average treatment effect
% (ATE) defined as $\mathbb{E}[{\bf B(t) - B(t')}]$ and the probability of a
% non-zero treatment effect defined as $P({\bf B(t) \neq B(t')})$. In order to use
% the constraints from the previous sections to bound these parameters, we first
% need to show how the distribution $\psi$, defined in the previous section, can
% be linearly mapped to $P({\bf B(t)})$. Assume a latent variable Bayesian network
% $\mathcal{G}$ that meets the conditions of Definition \ref{def:gen_iv} and the
% corresponding joint distribution $\psi$ over potential outcomes
% $\mathbf{\tilde{X}}$ is defined as in Section \ref{sec:iv}. Then, for an
% arbitrary outcome variable $B \in \mathbf{A} \cup \mathbf{B}$, treatment
% variable $T \in \mathbf{A} \cup \mathbf{B}$, and value $t$, we can calculate the
% distribution over $B(t)$ as in a structural equation model, by intervening on
% $T$ in the graph and repeatedly appealing to the consistency assumption to
% marginalize out all variables other than $B(t)$ \cite{pearl2009causality}. For
% example, assuming the classic IV model in Figure \ref{fig:iv} (a), the
% distribution $P(X(a))$ is just a marginal of $\psi$ and the distribution
% $P(Y(a))$ can be derived as
% % 

% \begin{align}
% 	P(Y(a) = y) = \sum_x P(Y(x) = y, X(a) = x).
% \end{align}
% %
% In general, for variable $B$ and intervention $T=t$, this expression
% can be constructed as follows. Let $\bf \tilde V$ represent all potential
% outcomes for variables other than $X$. The general expression is then
% %
% \begin{equation}
% % <<<<<<< HEAD
% P(X(t) = x) = \sum_{\mathbf{v} \in \text{support}(\mathbf{V})} P(V(g_V(\mathbf{v},t)): V \in \mathbf{V}),
% % =======
% % P(&X(a) = x) &= \sum_{{\bf v}} P(X(g({\bf v}, A=a)) = x, {\bf \tilde V = v}),
% % >>>>>>> 1b39f0ebf1ca9bf21a04821e15ca097983d6f858
% \end{equation}
% %
% % <<<<<<< HEAD
% % where, $V(a) = V$ if $V$ is not a descendant of $A$ and $Dom(Pa(X))$
% % is the domain of the parents of $X$. This recursive expression is linear in
% % $\psi$, and thus any causal parameter that can be written as a linear function
% % of $P(X(a))$ can also be written as a linear function of $\psi$. This includes
% % the average treatment effect (ATE) which is defined as $E[X(a) - X(a')]$ as
% % well as the probability of non-zero treatment effect, which can be written as
% % $P(X(a) \neq X(a'))$. As in the previous sections, we can express observed
% % data, measurement error, and graphical constraints covered by Proposition
% % \ref{prop:general_iv} as linear constraints on $\psi$, allowing us to compute
% % bounds on target parameters involving $P(X(a))$ under these constraints.
% % =======
% where $g_V({\bf v}, t)$ is defined as
% \begin{align}
%   g_V({\bf v}, t) =
%   \begin{cases}
%     \mathbf{v}_{Pa(V)\setminus\{T\}}, t &\text{if~} V \in Ch(T)\\
%     \mathbf{v}_{Pa(V)} &\text{otherwise}
%   \end{cases}
% \end{align} 
% %
% and for joint setting $\mathbf{v} \in \text{support}(\mathbf{V})$ and set of variables
% $\mathbf{S} \subseteq \mathbf{V}$, $\mathbf{v}_{\mathbf{S}}$ is the value of $\mathbf{S}$
% in $\mathbf{v}$. Note that the RHS is summing over marginals of $\psi$ and thus any
% causal parameter that can be written as a linear function of $P(X(t))$ can also be
% written as a linear function of $\psi$. As in the previous sections, we can express
% observed data, measurement error, and graphical constraints covered by Proposition
% \ref{prop:general_iv} as linear constraints on $\psi$, allowing us to compute bounds on
% target parameters involving $P(X(t))$ under any combination of general IV model and
% measurement error assumptions.

% % Importantly, the above mapping applies to the full class of graphs defined by
% % Proposition \ref{prop:general_iv}, but applying Proposition
% % \ref{prop:confounded_instrument} to this setting requires a bit more care. If
% % $T$ is in the set of instruments $\mathbf{A}$, then augmenting the graph as
% % described in Proposition \ref{prop:confounded_instrument} does, in fact, change
% % the constraints on $P(X(t))$. If, however, $T$ is in $\mathbf{X}$, then
% % augmenting the graph according to Proposition \ref{prop:confounded_instrument}
% % leaves the constraints on $P(X(t))$ unchanged, as all instruments still satisfy
% % the conditions of Proposition \ref{prop:confounded_instrument}.

% % In this setting, any of the measurement error constraints
% % described in Section \ref{sec:base} may be applied to each potential outcome
% % and its proxy under each As in the case with multiple proxies,
% % all variables irrelevant to the assumption should be marginalized out in its
% % expression. These constraints should now be interpreted as counterfactual
% % assumptions about what the distributional relationship between the true
% % variable and its proxies would be in a world in which a particular intervention
% % was applied.

% % \subsubsection*{Observed Data Constraints}
% %
% % We observe all proxies $\bf Y$, but we cannot directly observe the potential
% % outcomes for those proxies, ${\bf Y}(a)$. In some settings, the distribution of
% % ${\bf Y}(a)$ may nevertheless be fully identified from the observed data, with
% % \cite{shpitser2006completeid} providing necessary and sufficient conditions for
% % this to be the case. In such cases, we may simply encode the observed data
% % constraints on the margins of $\psi$ as before.
% %
% % In other cases the distribution of ${\bf Y}(a)$ may be only partially
% % identified. It may still be possible to relate the potential outcome
% % distribution to the observed data distribution via a linear function, in which
% % case the observed data constraint may be still be encoded as an equality
% % constraint. The instrumental variable setting described below is one such case.
% % Alternatively, bounds on $P({\bf Y}(a))$ may be available through other methods
% % \cite{balke93ivbounds, finkelstein2020generalizediv}, and can be encoded as
% % linear inequality constraints.
% %
% % We indicate the upper bound on $P(Y(a) = x_j)$ as $P^{\max}(Y(a) = x_j)$, and
% % the lower bound by $P^{\min}(Y({a}) = x_j)$. In the single-proxy scenario, the
% % observed data constraints due to the partially identified distribution of
% % $P(Y(a))$ can then be expressed as
% % %
% % \begin{align*}
% %   P^{\min}(Y({a}) = x_j)
% %   \le
% %   &\sum_{h,i,k} \psi_{hijk}
% %   \le
% %   P^{\max}(Y({a}) = x_j).
% % \end{align*}
% %
% % \subsubsection*{Graphical Constraints for Potential Outcomes}
% %
% % To obtain constraints imposed by the graph on potential outcomes, we make use of
% % the approach developed in \cite{richardson2013single} for converting hidden
% % variable DAGs on observed variables to hidden variable DAGs on potential
% % outcomes under a single intervention. Such graphs are referred to as single
% % world intervention graphs (SWIGs). To construct a SWIG for some intervention
% % $\bf A = a$, each variable in the intervention set is split into two nodes. One
% % of these represents the random value of the variable, and inherits all incoming
% % edges. The other represents the intervention value, and inherits all outgoing
% % edges. All nodes topologically after the intervention nodes can now be taken to
% % refer to a potential outcome after intervention. An example SWIG for Figure
% % \ref{fig:iv} (c) under the intervention $Z=z$ and $T=t$ is shown in Figure
% % \ref{fig:iv} (d).
% %
% % To generate a set of graphical constraints the endogenous variables and their
% % potential outcomes, we generate a SWIG under each possible intervention. For
% % each SWIG generated in this way, we obtain the graphical constraints implied by
% % the SWIG exactly as described in Section \ref{sec:iv}. As before, if a SWIG is
% % not in the class of linear models, its linear relaxation can be obtained
% % according to the procedure described above to obtain outer-bounds.
% %
% % Suppose we have a graph $\mathcal G$ in the instrumental class described by
% % Proposition \ref{prop:general_iv}, which represents either the model of interest
% % or its linear relaxation, and that we are interested in the potential outcome
% % of one child of $\Lambda$ after intervention on another (intervention on the
% % instruments is uninteresting, as they are randomized). In $\mathcal G$, and in
% % the SWIG obtained after intervention on any children of $\Lambda$, the
% % instruments satisfy the conditions of Proposition
% % \ref{prop:confounded_instrument}, such that at any point the edge between each
% % instrument and its child may be replaced or augmented with a bidirected edge.
% % The only SWIGs for which this does not hold are those obtained after
% % intervention only the instruments themselves; however these SWIGs are irrelevant
% % to the distribution of potential outcomes given intervention on children of
% % $\Lambda$, as those potential outcomes do not appear in such SWIGs.
% %
% % The interesting consequence of this observation is that, so long as we are
% % interested in a potential outcome after intervention on a child of $\Lambda$,
% % Proposition \ref{prop:confounded_instrument} applies in the causal setting, just
% % as it does in the observational setting described above. Thus, for example,
% % Figures \ref{fig:gen_iv} (c) and (d) lead to exactly the same bounds on $P(Y(x)
% % = y)$ as does the classic IV model of Figure \ref{fig:iv} (a). The bounds
% % developed in \cite{finkelstein} for models with traditional instruments can
% % likewise be seen to apply to a larger set of graphs due to this result.
% % Similarly, the measurement scenario depicted in Figure \ref{fig:iv} (c), which
% % is explored in detail below, would yield the same bounds on $P(X(a) = x)$ were
% % the edge between $Z$ and $A$ replaced or augmented with a bidirected edge.
% %
% % One intuition for partial identification results in the IV model is that
% % variation in the treatment $X$ {\em caused} by the instrument $A$ is
% % unconfounded with the outcome $Y$, which allows us to the effect of that part of
% % the variation of $X$ on $Y$. This proposition shows that the same reasoning
% % applies when $A$ and $X$ are also, or only, confounded. Variation in $X$ that
% % participates in {\em covariation} with $A$ is not confounded with the otucome
% % $Y$, again allowing us to use that portion of the variation to partially
% % identify the effect of $X$ on $Y$.


% % This result opens the door to a much larger class of instruments than are
% % traditionally considered -- namely, to those that are confounded with, rather
% % than causes of, treatments -- and we hope it may facilitate innovative new
% % analyses of observational data.


% % Under Robins' finest fully randomized
% % causally interpreted structural tree graph model \cite{robins1986new}, all
% % independence constraints on the distributions of potential outcomes can be
% % represented by some SWIG. By construction, any one such constraint will only
% % ever involve potential outcomes under the same intervention. To obtain sharp
% % bounds under more restrictive causal models, such as Pearl's structural causal
% % model \cite{pearl2009causality} --- which imposes cross-world independence
% % assumptions --- one may need to incorporate additional constraints.

% \subsubsection*{Monotonicity assumptions}

% One final commonly used assumption that can be encoded linearly is the causal
% \textbf{monotonicity assumption}, which relates potential outcomes under different
% interventions. The for intervention variable $T$ and potential outcome $V(t)$, the general monotonicity assumption can be written as
% %
% \begin{enumerate}
%   \item[(A5)] Monotonicity:
%     {\small
%     $P(V(t) = v, V(t') = v') = 0\,\, \forall t' > t, v' < v$.
%     }
% \end{enumerate}
% %
% Assumption (A5) is commonly applied to cases when it is believed that recieving a binary treatment cannot decrease the outcome \citet{}; however, it can also be applied to the measurement error setting to encode the assumption that increasing in the true value cannot lead to a decrease in the measurement. For example, if the true value $X$ is an ordered categorical variable like a Likert scale and $Y$ is a self-reported value, we might assume that, all else equal, increasing $X$ cannot lead to a lower self-reported value. Additional causal constraints, such as limits on the effect size or
% the proportion affected, may be similarly imposed. As with the measurement error
% assumptions, these equality constraints can be relaxed by specifying that the sums are
% bounded from above, rather than identically equal to zero.

% % \begin{figure}[t]
% %     % \centering
% % 	\begin{subfigure}[b]{0.49\textwidth}
% % 			\includegraphics[width=\textwidth]{../output/figures/mrs_rct.pdf}
% % 			\caption{}
% %     \end{subfigure}
% %     \begin{subfigure}[b]{0.49\textwidth}
% % 			\includegraphics[width=\textwidth]{../output/figures/mrs_iv.pdf}
% % 			\caption{}
% % 	\end{subfigure}\hspace{1cm}
% %   \caption{
% %     Bounds on the ATE $E[X(1) - X(0)]$ under different measurement
% %     error assumptions. (a) observed data distributions and bounds under in a
% %     randomized trial setting and (b) shows observed data distributions and
% %     bounds in an instrumental variable setting. Note that in the binary
% %     instrumental variable setting, $Z$ can be thought of as the assigned
% %     treatment and $A$ can be thought of as the observed treatment under
% %     partial compliance. $A=Z$ then corresponds to observed compliance and, in
% %     these cases ($A=Z=0$ and $A=Z=1$), the proxy distributions match the
% %     distributions in the randomized trial.
% %   }
% %   \label{fig:causal_bounds}
% % \end{figure}

% % % \begin{framed}
% % \subsection*{Application: Bounding the ATE in a randomized trial}
% % % \label{sec:casual-examples}
% %
% % To demonstrate computation of bounds for a causal parameter, we will now extend
% % our previous examples to compute bounds on the ATE in a randomized trial where
% % there is measurement error on the outcome variable. Assume that we are
% % interested in the ATE of a binary treatment $A \in \{0,1\}$ on a variable $X$
% % using data from a single noisy proxy $Y$, with $X$ and $Y$ defined as before.
% % We will assume the graphical model shown in Figure \ref{fig:iv} (a) where $U$
% % represents an unobserved confounder. This model trivially satisfies the
% % conditions of Proposition \ref{prop:general_iv} and thus all graphical
% % constraints can be expressed linearly. Figure \ref{fig:causal_bounds} (a) shows
% % the resulting bounds on the ATE as additional constraints are added. With only
% % the graphical constraints, the bounds computed are the trivial bounds, though
% % this will not be the case for all target parameters. Adding the causal
% % assumptions (A5)-(A5), however, we are able to meaningfully bound the ATE away
% % from zero. This bound becomes much tighter when the measurement error
% % constraints are added. \hfill\qedsymbol
% % % \end{framed}
% %
% % % \begin{framed}
% % \subsection*{Application: Bounding the ATE in an instrumental variable model}
% %
% % Assume now that we are interested in the ATE of $A$ on $X$, but that $A$ is no
% % longer randomized. Instead assume that we observe a binary instrumental
% % variable $Z \in \{0,1\}$ and that all variables follow the graphical model
% % shown in Figure \ref{fig:iv} (c) where again $U$ represents an unobserved
% % confounder. This model, which represents the IV model with measurement error on
% % the outcome variable, similarly satisfies the conditions of Proposition
% % \ref{prop:general_iv} and we can again express all graphical constraints
% % linearly. We assume that $P(A=0 \mid Z=0) = P(A=1 \mid Z=1) = 0.8$ and $P(Y
% % \mid A,Z)$ is distributed as shown in Figure \ref{fig:causal_bounds} (b). These
% % distributions were chosen to roughly simulate a trial with partial compliance
% % where $Z$ represents treatment assignment and $A$ represents actual treatment.
% % The resulting bounds on the ATE under different assumptions are also shown in
% % Figure \ref{fig:causal_bounds} (b). As expected, these bounds are wider than
% % those produced in the randomize trial case, but the ATE can nevertheless be
% % meaningfully bounded away from zero with only the causal assumptions (A5)-(A5).
% % \hfill\qedsymbol
% %
% % We re-emphasize that no known symbolic bounds exist for either of these
% % settings and that the various numerical bounds in each setting were computed by
% % simply making small changes to the implied LP. For a full description of the
% % LPs in both of these cases, see Appendix \ref{app:causal_lp}.
% % % \end{framed}

\end{document}
