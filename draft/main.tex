\documentclass{uai2021} % for initial submission
% \documentclass[accepted]{uai2021} % after acceptance, for a revised
                                    % version; also before submission to
                                    % see how the non-anonymous paper
                                    % would look like
% NOTE: Only comment/uncomment the lines above as appropriate,
%       as they will be replaced automatically for papers to be published.
%       Do not make any other change above this note for an accepted
%       version.

%% Choose your variant of English; be consistent
\usepackage[american]{babel}

%% Some suggested packages, as needed:
\usepackage{natbib} % has a nice set of citation styles and commands
    \bibliographystyle{plainnat}
    \renewcommand{\bibsection}{\subsubsection*{References}}
	
% \usepackage{mathtools} % amsmath with fixes and additions
% \usepackage{siunitx} % for proper typesetting of numbers and units
\usepackage{booktabs} % commands to create good-looking tables


\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfiles}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage{tikz}
\usepackage{centernot}
\usepackage{framed}
\usetikzlibrary{shapes,backgrounds,fit}

% \usepackage{xr}
% \externaldocument{appendix}

\usepackage{algorithm}
\usepackage{algorithmicx}

\usepackage{amsthm}       % hyperlinks
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{procedure}{Procedure}
\newtheorem{corollary}{Corollary}
\newtheorem{proposition}{Proposition}
% \newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{assumption}{Assumption}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}


\title{Partial Identifiability in Discrete Data With Measurement Error}

% The standard author block has changed for UAI 2021 to provide
% more space for long author lists and allow for complex affiliations
%
% All author information is authomatically removed by the class for the
% anonymous submission version of your paper, so you can already add your
% information below.
%
% Add authors in order of decreasing contribution
\author[1]{\href{mailto:Harry Q. Bovik <harryq@example.edu>?Subject=Your UAI 2021 paper}{Harry~Q.~Bovik}{}} % Lead author
% \author[2]{Coauthor~One}
% \author[1,2]{Coauthor~Two}
% \author[3]{Further~Coauthor}
% \author[1]{Further~Coauthor}
% \author[3]{Further~Coauthor}
% \author[3,1]{Further~Coauthor}
% Add affiliations after the authors
\affil[1]{%
    Computer Science Dept.\\
    Cranberry University\\
    Pittsburgh, Pennsylvania, USA
}
% \affil[2]{%
%     Affiliation\\
%     Address\\
%     …
% }
% \affil[3]{…}

\begin{document}
\maketitle

\begin{abstract} 
	%
  When data contains measurement errors, it is necessary to make modeling
  assumptions relating the measurements to the unobserved true
  values. Work on measurement error has largely focused on
  models that fully identify the target parameter. As a result, many
  practically useful models that result in \emph{bounds} on the target
  parameter -- known as partial identification -- have been neglected. In this
  work, we present a method for partial identification in a class of
  measurement error models involving discrete variables. We focus on models
  that impose linear constraints on the target parameter, allowing us to
  compute partial identification bounds using off-the-shelf LP solvers. We show
  how several common measurement error assumptions can be composed with an
  extended class of instrumental variable-type models to create such linear
  constraint sets. We further show how this approach can be used to bound
  causal parameters, such as the average treatment effect, when treatment or
  outcome variables are measured with error. Using data from the Oregon Health
  Insurance Experiment, we apply this method to estimate bounds on the effect
  Medicaid enrollment has on later depression when measured
  with error.
  %
\end{abstract}

% Introduction
\subfile{intro}

% Problem setup and definitions
% \subfile{setup}

% Bounds on the marginal of the unobserved variable with no other assumptions
\subfile{base}

% Bounds on the marginal when there is an auxiliary variable
\subfile{iv}

% Bounds on the causal effect
\subfile{causal-bounds}

\subfile{ohie}

% \subfile{symbolic-bounds}

% \subfile{examples}

% Application to IV with measurement error on outcome
% \subfile{application}

% Background
\subfile{background}

% Conclusion
\subfile{conclusion}

% \section*{Broader impact}

% We believe this work will be of practical use to scientists with imperfectly
% measured data, who cannot credibly impose sufficiently strong assumptions to
% obtain full identification. Our results allow for substantive inference in the
% absence of such assumptions. We believe these results will especially be of use
% to public health officials and social scientists, who rely on surveys and other
% means of self-reporting, which is especially susceptible to arbitrary
% measurement error. We also believe our view of measurement error --- as
% inference on a latent variable with known cardinality and one or more proxies
% in a causal model --- will help drive a more general theory of non-parametric
% partial identification of the distributions of variables with measurement
% error. We do not believe that there is much risk that this work will have a
% negative social impact except insofar as any statistical technique can be
% misused.

% \begin{ack}
% \end{ack}
% \bibliographystyle{plain}
\clearpage
\bibliography{references}
\clearpage
%
\appendix
\onecolumn
\subfile{estimation}
\subfile{nonlinear}
\subfile{software}
\subfile{proofs}
\subfile{example-lps}

\end{document}
