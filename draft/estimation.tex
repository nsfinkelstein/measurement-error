\documentclass[main.tex]{subfiles}

\begin{document}

\section{Confidence Intervals for Statistical Uncertainty}
\label{app:estimation}

Due to sampling variation, in real-data applications we know the observed data
distribution only up to statistical uncertainty. For any empirical
distribution, it is possible to construct an \textit{uncertainty set} for the
true observed data distribution. In this section, we present an approach for
propagating this uncertainty through the linear program and into the resulting
bounds. This approach can be used whenever there is statistical uncertainty
about the ``right-hand-side'' of the constraints of a linear program. As a
result, this approach can be used to estimate confidence intervals for the
partial identification bounds described in this paper, as well as for bounds on
the ATE in the classic IV model.

The two major advantages of this approach over existing methods for calculating
uncertainty sets for LP-based estimators \citep{horowitz2000confidence,
  cheng2006three} are its finite-sample validity, and its computational
simplicity. Because the uncertainty is built directly into the linear program,
calculating the uncertainty interval does not require resampling or running
additional optimization procedures. Perhaps the most common approach in such
settings is the bootstrapping method developed in \cite{beran1990bmethod} for
balanced simultaneous confidence sets. However, recent work indicates that the
bootstrap does not yield valid results for functionals that are not smooth when
estimates of the parameters of the distribution are asymptotically Gaussian, as
in discrete data \citep{fang2019nonsmooth}. It is well known that LPs are not
smooth in the RHS coefficients of their constraints \citep{chvatal1983lp}.

% To our knowledge, all existing
% methods in use for computing confidence intervals suffer from one of two issues. 

% \textbf{TODO: Compare to existing
%   methods (e.g. in Cheng and Small).}
% This approach can be used to obtain uncertainty coverage that is valid in
% finite-samples, including for scenarios that do not involve measurement error,
% such as the standard instrumental variable model. This is in contrast to
% alternative approaches to propogating uncertainty through numerical optimization
% procedures \cite{a, b}. In practice, bootstrapping is often used in such
% scenarios, though recent work has shown that this is not valid, even
% asymptotically, for non-smooth functionals, like the outputs of linear programs.

The basic idea of our approach is that rather than insisting that the full data
must marginalize exactly to the empirical distribution, we instead insist only
that the full data must marginalize to some distribution in the uncertainty set
of the observed data distribution. To facilitate incorporation of such
uncertainty sets into the relevant linear programs, we would like to be able to
express them linearly. To that end, we make use of the convex polytope
uncertainty region developed in \citet{garivier2011polytope} and succinctly
stated in \citet{nowak2019tighter}, described by the expression
% 
\begin{align}
  \label{eq:uncertainty-polytope}
  \mathbb{P}(\text{KL}_{bern}(\hat p_i || p_i)
  \le \frac{\log(\frac{2k}{\alpha})}{n} ~~ \forall~i ) \ge 1 - \alpha,
\end{align}
%
\noindent where $\hat p_i$ and $p_i$ represent the empirical and true
probabilities of outcome $i$ respectively, $\text{KL}_{bern}(p || q)$ is the
KL-divergence between Bernoulli distributions with parameters $p$ and $q$, $k$
represents the number of possible outcomes, and $n$ represents the size of the
dataset. In our setting, $k$ is just the product of the cardinalities of the
observed variables.
% Because the $\text{KL}_{bern}(p || q)$ distribution is convex in $q$, the the
% event with bounded probability in Equation (\ref{eq:uncertainty-polytope}) is a
% convex polytope in $\Delta^k$.

For convenience, we let $\mathcal U(\hat P, \alpha, n)$ represent the
uncertainty polytope at level $\alpha$ around $\hat P$ for a sample of size $n$.
The following proposition shows how statistical uncertainty can be incorporated
when no assumptions are made about the relationships among the observed
proxies $\bf Y$, as in Section \ref{sec:measurement_error_constraints}.

\begin{proposition}[Measurement Error Uncertainty]
  \label{prop:uncertain-me}
  Let $\mathcal M$ be a convex polytope model for the distribution $\phi$ of an
  unobserved random variable $X$ and observed proxies $\bf Y$, and
  $\hat P_{\bf Y}$ be the empirical distribution of $\bf Y$. Define
  \[
    \mathcal P \equiv \{Q: \sum_x Q(X = x, {\bf Y}) \in
    \mathcal U(\hat P_{\bf Y}, \alpha, n)\}.
  \]
  Then for any functional $\eta(\phi)$,
  \[
    P(\min_{Q \in \mathcal M \cap \mathcal P} \eta(Q) \le \eta(\phi) \le \max_{Q \in \mathcal M \cap \mathcal P} \eta(Q)) \ge 1 - \alpha.
  \]
\end{proposition}

\begin{proof}
  By the validity of the polytope uncertainty region
  (\ref{eq:uncertainty-polytope}), $P(\phi \in \mathcal P) \ge 1 - \alpha$
  for any $\phi$ -- including any $\phi \in
  \mathcal M$ -- as $\sum_x \phi(X = x,
  {\bf Y}) = P({\bf Y})$. Because $\phi \in \mathcal M$ by assumption, $P(\phi
  \in \mathcal P) = P(\phi \in \mathcal P \cap \mathcal M)$. The conclusion then
  follows trivially.
\end{proof}

Next, we consider uncertainty in IV models, where the observed data
constraints are expressed in terms of conditional distributions.
For that reason, we create uncertainty polytopes around these conditional
distributions, rather than around the full observed data distribution.

\begin{proposition}[Uncertainty w/ Instruments]
  \label{prop:uncertain-iv}
  Let $\mathcal M$ be a convex polytope model for $\psi$, and $\hat P_{\bf B
    \mid a}$ represent the empirical distribution of $P({\bf B} \mid {\bf A =
    a})$, and $\hat P({\bf a})$ represent the empirical probability that $P({\bf
    A = a})$. Define
\[
  \mathcal P \equiv \{Q:
  \sum_{\mathbf{\tilde{b}}} Q({\mathbf{\tilde{b}}})
  \prod_{B \in \mathbf{B}} \mathbb{I}\left(\tilde{b}(\mathbf{a}_{Pa(B)},\mathbf{b}_{Pa(B) \setminus \mathbf{A}})=\mathbf{b}_B\right)
  \in \mathcal
    U(\hat P_{\bf B \mid a}, 1 - (1 - \alpha)^{1 / |{\bf A}|}, n \hat P({\bf a}))
    ~~\forall~{\bf a}\}.
  \]
  Then for any functional $\eta(\psi)$ that is not a function of $P({\bf A})$,
  \[
    P(\min_{Q \in \mathcal M \cap \mathcal P} \eta(Q) \le \eta(\psi) \le
    \max_{Q \in \mathcal M \cap \mathcal P} \eta(Q)) \ge 1 - \alpha.
  \]
\end{proposition}

\begin{proof}
  For a fixed $\bf a$,
  $P({\bf B \mid A = a}) = \sum_{\mathbf{\tilde{b}}} Q({\mathbf{\tilde{b}}})
  \prod_{B \in \mathbf{B}} \mathbb{I}\left(\tilde{b}(\mathbf{a}_{Pa(B)},\mathbf{b}_{Pa(B) \setminus \mathbf{A}})=\mathbf{b}_B\right)$
  as described in Section \ref{sec:iv}. The set $\mathcal P$ is
  therefore the set of distributions over $\bf \tilde B$ such that each observed
  conditional distribution is in its $1 - (1 - \alpha)^{1 / |{\bf A}|}$
  uncertainty polytope, as each such polytope is estimated using a sample of
  size $n \hat P({\bf a})$. Note that estimates of these conditional
  distributions are statistically independent, as each sample subject is used in
  the estimation of only the conditional distribution corresponding to the
  observed value of the instruments in that subject. Therefore the probability that
  all $|{\bf A}|$ of them fall into their $1 - (1 - \alpha)^{1 / |{\bf A}|}$
  uncertainty polytopes is simply $1 - \alpha$. The remainder of the proof is as
  before.
\end{proof}

This approach cannot account for uncertainty when either the objective or the
assumptions are a function of $P({\bf A})$. For example, in linear
program (\ref{eq:base}), we target the factual distribution of the unobserved
random variable $X$, which is as the sum of products of the parameters of $\psi$
and $P({\bf A})$. Because the distribution of the instrument is observed, we can
in theory substitute in its empirical distribution. However, if we are to
account for uncertainty, we must introduce the true parameters of $P({\bf A})$
distribution over the instruments into the program, yielding bi-linear terms. In
the following proposition, we provide a graphical criterion for assumptions and
objectives that are not functions of $P({\bf A})$, and therefore for which
Proposition \ref{prop:uncertain-iv} can be used.

\begin{proposition}
  \label{prop:uncertainty-validity}
  The distribution of a set of potential outcomes $Z_1({\bf T_1=t_1}), \dots,
  Z_n({\bf T_N = t_n})$ is not a function of the distribution of the instruments
  $P({\bf A})$ if and only if there is no index $i$ such that there is an
  instrument $A$ with a directed path to $Z_i$ not through ${\bf T_i}$.
  % For
  % general IV models, the constraints and target functional of Proposition
  % \ref{prop:uncertain-iv} will be linear if and only if the objective and
  % measurement error assumptions are not functions of $P({\bf A})$.
\end{proposition}

\end{document}
