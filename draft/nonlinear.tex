\documentclass[main.tex]{subfiles}

\begin{document}

\section{Computing bounds for non-linear models}
\label{sec:nonlinear}

Unfortunately, many useful models do not fall into the class of general IV
models. For example, the simple Markov chain shown in Figure \ref{fig:iv} (f) is
not a general IV model and, as we will see below, generates non-linear
constraints on $\phi$. In this section, we describe how non-sharp outer bounds
can be derived for such cases. The only known complete procedure for identifying all
constraints implied by Bayesian networks on the distribution of a subset of
their vertices is an application of quantifier elimination \citep{gieger},
which is infeasibly slow for many problems. When constraints are known to
exist, for example by Evans' e-separation criterion
\citep{evans2012eseparation}, their exact form may not be known and may not be
linear. When constraints are known, but are not linear, it may be possible to
derive sharp bounds analytically. For example, the following proposition,
proven in the supplementary material, gives sharp bounds for a three variable
Markov chain (Figure \ref{fig:iv} (f)) over binary variables.
%
\begin{proposition}
    \label{prop:binary_bounds}
	%
    Let $X$ and $Y$ be binary variables such that $X \not\independent Y$, let
    $A$ be a discrete variable such that $A \independent Y | X$ and $P(A=a) >
    0$ for all $a$, and let $p_y = P(Y=y)$ and $p_{y \mid a} = P(Y=y\mid A=a)$.
    Then we have the following sharp bounds on $P(X=1)$:
	%
    \begin{align}
	  %       P(X=1) &\in [l,u] \cup [1-u,1-l]\\ \nonumber
	  % l &= \max_a \frac{P(Y=1) - P(Y=1|A=a)}{1 - P(Y=1|A=a)}\\ \nonumber
	  % u &= \min_a \frac{P(Y=1)}{P(Y=1|A=a)}
	  P(X=1) &\in \bigcup_{y\in\{0,1\}} \left[\frac{p_y - \min_a p_{y \mid a}}{1 - \min_a p_{y \mid a}},\frac{p_y}{\max_a p_{y\mid a}}\right].
    \end{align}
\end{proposition}
%
Such analytical bounds, however, are not typically available. In these cases,
\emph{non-sharp} bounds can be derived for any graph by relaxing independence
assumptions until the model becomes a general IV model. Specifically, any
latent variable Bayesian network $\mathcal{G} = (\mathbf{V},\mathbf{E})$ can be
converted to a general IV model $\mathcal{G}'$ first identifying the set of
instruments and then adding a mutual latent confounder between all
non-instruments. This procedure is formalized in the following steps:
% 
\begin{procedure}[Graphical Relaxation]
  \qquad
\begin{enumerate}
	%
\item Initialize $\mathbf{S}$ as the set of all latent confounders and all
variables $V$ such that $Pa(V) = \emptyset$
	%
\item For any latent confounder $U$ with exactly two children $A$ and $B$
such that $Pa(A) = \{U\}$ and $Ch(A) = \{B\}$ or $Ch(A) = \emptyset$, add $A$ to $\mathbf{S}$
  %
\item Add a latent confounder $\Lambda$, and an edge from $\Lambda$ to each
variable in $\mathbf{V} / \mathbf{S}$.
  %
\end{enumerate}
\label{proc:relax}
\end{procedure}
% 

\begin{figure}[t]
  \centering {\small
    \include{graph-relaxation}
  }
  \caption{A non-linear graphical model $\mathcal G$ and an application of the
    linear relaxation procedure, resulting in the linear relaxed model $\mathcal
    G'$. }
  \label{fig:graph_relaxation}
\end{figure}

An example application of this procedure is shown in Figure
\ref{fig:graph_relaxation}. Because adding an additional latent confound can
only \emph{remove} independencies from the graph, this procedure represents
relaxations of the constraints on $\psi$, and therefore enables the calculation
of outer bounds for mismeasured variables through linear programming.

\begin{proposition}
  Suppose $\mathcal G'$ is the general IV graph obtained after relaxing a
  graph $\mathcal G$ according to Procedure \ref{proc:relax}. Then sharp bounds
  on any linear functional of $P({\bf B_U})$ in $\mathcal G'$ will be valid outer
  bounds in $\mathcal G$.
\end{proposition}

% As a result, applying the LP approach
% to the resulting model will return \emph{outer bounds} on the true partial
% identification set whose tightness will depend on how many edges were added in
% Step 3.
% In the following section, we extend our earlier discussion of
% potential outcomes to consider partial identification bounds for causal
% parameters and constraints relating multiple potential outcomes.

% Bounds obtained through convex optimization are only sharp, then, in models
% where all constraints are known and can be expressed linearly. For example, if
% we assume that $A$, $X$, and $Y$ form a Markov chain, as shown in Figure
% \ref{fig:iv} (b), then the resulting constraints are quadratic and the
% procedure described in this work cannot be used to produce sharp
% bounds\footnote{In fact, no sharp bounds are currently known for this case}.
% In such cases, however, it may be possible to compute non-sharp bounds by
% relaxing certain graphical constraints. Continuing our example, we can compute
% non-sharp bounds for Figure \ref{fig:iv} (b) by relaxing the model to Figure
% \ref{fig:iv} (b). In the following section, we extend our discussion of
% potential outcomes to consider partial identification bounds for causal
% parameters and constraints relating multiple potential outcomes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uncomment to include the markov chain case
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subfile{markov_chain}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





% \subsubsection{Combining Theorem \ref{thm:binary_bounds} with other common
% assumptions}
%
% In the presence of additional assumptions, the bounds in Theorem
% \ref{thm:binary_bounds} can be further refined. In this section we present a
% few such refinements for common measurement error assumptions.
%
% \begin{corollary}[Bounded misclassification probability]
%   If, in addition to the assumptions made in Theorem \ref{thm:binary_bounds},
%   we assume $p(Y=1|X=0) + p(Y=0|X=1) < 1$, we have the following sharp bounds
%
%     \begin{align*}
%         \pi_1 \in \left[\frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}},\frac{p_1}{\max_a p_{1|a}}\right]
%     \end{align*}
%   \end{corollary}
%
% \begin{corollary}[Unidirectional errors]
%   If, in addition to the assumptions made in Theorem \ref{thm:binary_bounds},
%   we assume $p(Y=1|X=0) = 0$, we have the following sharp bounds
%
%     \begin{align*}
%         \pi_1 \in \left[p_1,\frac{p_1}{\max_a p_{1|a}}\right]
%     \end{align*}
%   \end{corollary}
%
% \begin{corollary}[Label independent noise]
%   If, in addition to the assumptions made in Theorem \ref{thm:binary_bounds},
%   we assume $p(Y=1|X=0) = p(Y=1 | X=1)$, we have the following sharp bounds
%
%     \begin{align*}
%       \pi_1 \in \left[
%       p_1,\frac{p_1 - p^*}{1 - 2p^*}
%       \right] \cup \left[
%       1-\frac{p_1 - p^*}{1 - 2p^*},1-p_1
%       \right]
%     \end{align*}
%
%     where $p^* = \min \left\{\min_a p_{1|a},1-\max_a p_{1|a}\right\}$.
%   \end{corollary}

%   TODO: Re-introduce entropic bounds section
%   \subsubsection{Entropic Bounds in Non-binary case}
%
%   In the non-binary case, it is much more difficult to obtain bounds on
%   $P(X=x')$ as solving quadratic programs is, in general, NP-hard [CITATION].
%   However, it is easy to obtain entropy based bounds. We start with the data
%   processing inequality, and obtain an upper bound on $H(A \mid X)$ in terms
%   of the observed data.
%
% \begin{align}
%   I(A:X) &\ge I(A:Y)\\
%   H(A) - H(A \mid X) &\ge I(A:Y)\\
%   H(A) - I(A:Y) &\ge H(A \mid X)
% \end{align}
%
% The left hand side is a function of the observed data distribution. This
% constraint says the entropy of $A$ conditional on $X$ has to be smaller than
% some value. Intuitively, if the value of the left hand side is small, there
% cannot be a lot of entropy left in $A$ once we know $X$, i.e. $X$ must be
% highly predictive of $A$. This means that, modulo the label-switching issue,
% the distributions of $A$ and $X$ must be fairly similar. If, on the other
% hand, the left hand side of the above is large, than the distribution of $X$
% need not be similar to that of $A$.
%
% We note that these results are all valid if $A$ is a child of $X$ rather than
% a parent, so these bounds may be used when a latent variable has two
% measurements. This is because both graphs imply the same statistical
% properties, and no counterfactual reasoning is used in the derivation of these
% bounds.

\end{document}
