\documentclass[main.tex]{subfiles}

\begin{document}

\section{Introduction}

Measurement error is a problem in fields ranging from machine learning to
economics to public health.
% referred to as
% \textbf{measurement error bias}\footnote{Here, we use ``bias'' in the
% statistical sense to refer to inferences that are wrong in a non-random way
% \citep{casella2002statistical}.}. As machine learning is increasingly applied
% to messy observational data in high-stakes applications such as healthcare, it
% is critical that we develop methods to account for measurement error bias in a
% wide range of settings.
%
For example, in machine learning, mislabeled training data may lead to degraded
model performance \citep{frenay2013classification}. In medical research,
incomplete patient histories or misdiagnosed conditions may lead to biased
estimates of treatment--outcome relationships
\citep{rothman2008modern,brakenhoff2018measurement}. In fields such as
economics and political science, which rely heavily on survey data, factors
such as poor question design, social stigma, and recall bias can all lead to
spurious responses \citep{molinari2008misclassified,imai2010treatment}.
Inferences drawn from such data may differ in a systematic way from the truth,
leading to \textbf{measurement error bias}. As messy observational data is
increasingly used to make decisions and inform policy, it is critical that we
develop methods to account for measurement error bias in a wide range of
settings.

% proportion of
% people in a sample who have depression and that we measure depression via a
% commonly used nine question survey called the Patient Health Questionnaire
% (PHQ-9). Unfortunately, PHQ-9 surveys are known to be error-prone measures of
% depression \citep{kroenke2001phq}. If we use the PHQ-9 surveys without
% accounting for these errors, our estimate may differ in a systematic way from
% the truth. This systematic difference is referred to as \textbf{measurement
% error bias} and it can have major consequences if we use the biased estimate to
% inform policy decisions. As machine learning is increasingly applied to messy
% observational data in high-stakes applications such as healthcare, it is
% critical that we develop methods to account for measurement error bias in a
% wide range of settings.

In both statistics and machine learning, methods that account for measurement
error either (a) do not give formal bias guarantees, (b) rely on validation
data containing both true values and error-prone measurements, or (c) rely on
domain-specific assumptions about how measurement errors occur. As validation
data is frequently unavailable and can be costly to gather, we focus on the
latter of these options and refer to the measurement error assumptions,
collectively, as the \textbf{measurement error model}.
%
% If validation data containing both true values and error-prone measurements is
% available, it can be used to correct for measurement error bias
% \citep{carroll2006measurement,rothman2008modern,kuroki2014measurement}.
% However, such data can be costly to gather and, as such, is frequently
% unavailable. In such cases, we must use domain-specific assumptions about how
% errors occur in order to account for measurement error bias. We refer to these
% assumptions, collectively, as the \textbf{measurement error model} and the
% validity of one's conclusions depends on the correctness of this model.
%
In statistics, many such measurement error models have been proposed including
the classical, Berkson's, and mean independent error models, all of which apply
to continuous variables (see \citet{carroll2006measurement} and
\citet{gustafson2003measurement} for reviews of such models). In machine
learning, the majority of work on measurement error has focused on
classification in the presence of label errors
\citep{frenay2013classification}. In this setting, common measurement error
models include bounded error probabilities
\citep{liu2015classification,natarajan2013learning}, label independent errors
\citep{angluin1988learning,ghosh2017robust}, perfect separability of the true
class labels \citep{ghosh2017robust}, or some combination thereof.

Both the machine learning and statistics literatures have focused primarily on
measurement error models that guarantee \textbf{full identification}. That is,
in the limit of infinite data, these models guarantee that the target
parameter(s) can be narrowed to a single value. While full identifiability is a
desirable property, many common measurement error settings, especially those
involving discrete variables, do not satisfy the necessary assumptions. In such
settings, it may instead be possible, using the available assumptions, to
identify \emph{bounds} on the target parameter, referred to as \textbf{partial
identification} \citep{manski1990nonparametric}. Unfortunately, partially
identifiable measurement error models remain under-studied and many useful
models are not covered by current methods.


% Unfortunately, many common
% settings, especially those involving discrete variables\footnote{For variables
% with finite support, if the true value lies at an extreme of the support set,
% errors can only occur in one direction and thus cannot have mean zero.}, do not
% satisfy these classic assumptions and are not covered by current methods.
% This is especially true in settings involving discrete variables which
% cannot satisfy classical measurement error assumptions.

% In the absence of validation data (i.e., data containing both true values and
% error-prone measurements), accounting for measurement error bias requires
% making assumptions about how errors occur. We refer to these assumptions,
% collectively, as the \textbf{measurement error model} and the validity of one's
% conclusions depends on the correctness of this model. Unfortunately, current
% measurement error methods cannot handle a number common measurement error
% models. This is especially true in settings involving discrete variables which
% cannot satisfy classical measurement error assumptions\footnote{The classical
% measurement error model assumes zero-mean additive errors. For variables with
% finite support, if the true value lies at an extreme of the support set, errors
% can only occur in one direction and thus cannot have mean zero.}.

% We address this gap by proposing an easy to implement method to account for
% measurement error in a class of discrete variable measurement error models,
% many of which cannot be handled using current methods. These models are
% generally not fully identified by the observed data and, as such, have
% been largely ignored in the measurement error literature. While it is tempting
% in such settings to rely on overly strong assumptions that allow for full
% identification -- or, as is unfortunately common
% \citep{brakenhoff2018measurement}, to ignore the issue altogether -- we instead
% use weaker, credible assumptions to identify \emph{bounds} on the target of
% inference, referred to as \textbf{partial identification}
% \citep{manski1990nonparametric,balke93ivbounds,horowitz1995contaminated,
% manski2003partial,imai2010treatment,molinari2020microeconomics}.
% Previous work on partial
% identification under measurement error has largely focused on deriving analytic
% bounds for specific measurement error models. As a result,
% there remain many important and common partially identified models for which no
% known bounds exist, and thus, cannot be used in practice.

We address this gap by proposing an easy to implement method to account for
measurement error in a class of partially identifiable discrete variable models,
many of which cannot be handled using current methods. Our approach, which is
similar to that of \citet{balke93ivbounds} and \citet{imai2010treatment}, is to
encode the measurement error model as a set of constraints on the target
parameter and to calculate bounds by maximizing and minimizing the target
parameter over this constraint set. We focus primarily on measurement error
models that produce \emph{linear} constraints, allowing us to write this
optimization problem as a linear program. This approach allows a practitioner
to mix and match any combination of such modeling assumptions with little
effort. This flexibility means that it is trivial to compute bounds under new
measurement error models, enabling sensitivity analysis to different modeling
choices.


% The substantial majority
% of work on measurement error has focused on measurement error models that allow
% \emph{exact identification} of target parameters (e.g., see
% \citep{carroll2006measurement}). Unfortunately, these models exclude a
% significant number of common settings, especially settings involving discrete
% variables which cannot satisfy the classical measurement error
% model\footnote{The classical measurement error model assumes zero-mean errors.
% For variables with finite support, if the true value lies at an extreme of the
% support set, errors can only occur in one direction and thus cannot have mean
% zero.}. While it is tempting in such settings to use the exact identification
% assumptions anyways -- or to ignore the issue altogether, as is unfortunately
% common \citep{brakenhoff2018measurement} -- several authors have advocated for
% the use of weaker, credible assumptions to identify \emph{bounds} on the target
% of inference, referred to as \emph{partial identification}
% \citep{manski1990nonparametric,horowitz1995contaminated,manski2003partial,imai20
% 10treatment,molinari2020microeconomics}. Previous work on partial
% identifiability under measurement error has largely focused on deriving
% analytic bounds for specific combinations of settings and assumptions. As a
% result, there remain many important and common models for which no known
% partial identification bounds exist, and thus these models cannot be used in
% practice.

% As such, it
% is important to the validity of the analysis and the resulting conclusions that
% these assumptions be substantively justifiable.
%
% In practice, however, analysts often make implausibly strong assumptions for
% the sake of \emph{exactly identifying} their target of inference. In such
% cases, we advocate instead for the use of weaker, credible assumptions to
% identify \emph{bounds} on the target of inference, referred to as \emph{partial
% identification}. Previous work on partial identifiability under measurement
% error has largely focused on deriving analytic bounds for
% specific combinations of settings and assumptions. As a result, there remain
% many important and common settings for which no known bounds exist.

% We address this gap by proposing a simple and easy to implement method for
% numerically computing partial identification bounds in a class of discrete
% variable measurement error models. This class includes several models for which
% no analytical bounds are currently known. Our approach, which is similar in
% spirit to that of \citep{balke93ivbounds}, is to encode the target parameter and
% constraints imposed by the model as a linear program which can be maximized and
% minimized to produce sharp bounds. As such, our method can incorporate any
% measurement error assumption that can be written as a linear constraint on the
% distribution of the unobserved true value. Our primary contribution is a
% collection of modeling assumptions that can be encoded as linear constraints on
% a target parameter and, thus, are amenable to this linear programming approach.


Our primary contribution is a general method for partial identification in the
presence of measurement error by means of a collection of modeling assumptions
that can be encoded as linear constraints on the target parameter. In Section
\ref{sec:measurement_error_constraints}, we show that this includes several
common measurement error assumptions arising in settings where, for example,
credible bounds on error rates are known, errors may only occur in one
direction, or errors are more likely between certain categories than others.
Additionally, in certain settings we may have access to auxiliary variables
that give additional information about the error process, resulting in improved
bounds. In our main result (Section \ref{sec:iv}), we show how an extended
class of instrumental variable-type models produce linear constraints allowing
us to incorporate a variety of such variables. This extended class allows us to
use classic instruments -- i.e. variables that affect the measurement only via
the true value -- but also informative variables that are not classic
instruments. This includes variables that affect the measurement directly, such
as question order in a survey or surveyor gender, as well as variables that are
confounded with (rather than a cause of) the truth, such as demographic
information.
% We further show how non-sharp
% bounds can be computed for models not in this class.

In Section \ref{sec:causalbounds}, we show how our method can be applied to causal parameters, such as the average treatment effect (ATE), when
outcome, treatment, or confounding variables are measured with error. Finally,
using data from the Oregon Health Insurance Experiment
\citep{finkelstein2012oregon}, we demonstrate our approach by estimating bounds
on the effect of Medicaid enrollment on depression when using an error prone
measurement for the outcome. We use our approach to test how sensitive the
observed effect is to varying degrees of measurement error under several
different error models. In the following section, we introduce the basic
measurement error problem and the linear programming approach.

% In this work, we adopt a latent variable formulation of the measurement error
% problem. That is, we are interested in estimating a \textbf{target parameter}
% that involves the distribution of a discrete unobserved variable $X$ using
% observations of a discrete observed proxy variable $Y$. We show how sharp
% partial identification bounds for the target parameter can be computed
% numerically and without extensive derivations for a class of models, including
% several models for which no analytical bounds are currently known. Our
% approach, which is similar in spirit to that of \citep{balke93ivbounds}, is to
% encode the target parameter and constraints imposed by the model as a
% linear program which can be maximized and minimized to produce sharp bounds. We
% show that this approach can be used to compute bounds for factual and
% counterfactual parameters of a discrete mismeasured variable, including its
% marginal distribution and its moments, and the average treatment effect (ATE)
% of an intervention on the mismeasured variable.
%
% The primary contribution of this paper is a collection of modeling assumptions
% that can be encoded as linear constraints on a target parameter and, thus, are
% amenable to the linear programming approach. These include several common
% \textbf{measurement error} constraints (Section \ref{sec:base}),
% \textbf{graphical} constraints encoded by hidden variable Bayesian networks
% (Section \ref{sec:iv}), and \textbf{causal} constraints relating potential
% outcomes under different interventions (Section \ref{sec:casual-bounds}). Our
% main result, presented in Section \ref{sec:iv}, draws on results from
% \citep{fine} and \citep{evans} to show how an extended class of instrumental
% variable-type models produce linear constraints on a target parameter and
% present a simple procedure for obtaining outer bounds for graphs outside this
% class.

% Using a result about
% equivalence in hidden variable Bayesian network models \citep{evans}, we expand
% an instrumental-style class of graphs known to have linearly-constrained
% conditional distributions \citep{fine} to include graphs with non-randomized
% instruments. Additionally, we present a simple procedure for for obtaining
% outer bounds for graphs outside this class.
% Sharp bounds in the linear
% super-model will be valid outer-bounds in the original model.


% Throughout, we demonstrate the utility of this approach on synthetic
% examples representing important use cases. Our goal in these examples is to
% demonstrate how to apply the approach to commonly occurring models so that
% practitioners may adapt them to their specific data scenario. These cases
% include:
%
% \begin{enumerate}
% \item Bounding the mean of a discrete mismeasured variable using only assumptions about the degree and type of measurement error
% \item Bounding the mean of a discrete mismeasured using a discrete instrumental variable
% \item Bounding the ATE in a randomized trial with a mismeasured outcome
% \item Bounding the ATE in an instrumental variable model with a mismeasured outcome
% \end{enumerate}
%
% These cases represent common scenarios occurring in economics, public health,
% political science, and other scientific settings. Among these, cases 2, 3, and
% 4 all represent settings for which no sharp symbolic bounds are currently known
% and previous work suggests that deriving such symbolic bounds automatically for
% these settings is infeasible \citep{bonet2001instrumentality}. In the following
% section, we introduce our approach in the context of the simplest possible
% measurement error model.

\end{document}