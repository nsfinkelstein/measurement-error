\documentclass{article}

\usepackage{neurips_2020_author_response}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography

\usepackage{lipsum}

\begin{document}
We want to thank the reviewers for their time and feedback. Before addressing concerns raised by specific reviewers, we would like to address a concern raised by many of the reviews:

Several of the reviewers commented that they would have preferred to see a general solution to this problem. In this desire, we whole-heartedly agree with the reviewers and are actively pursuing such a solution. However, we strongly believe that when fully general solutions are not available, partial solutions can have substantial value. In ML we tend to have a strong preference for general approaches and theorems, but, in practice, simple partial solutions may have significant value to epidemiologists and econometricians. Our goal in this work is to demonstrate how simple methods with ubiquitous implementations can be used in a variety of settings, including those not previously known in the literature. 

We contend that the specific cases considered in this work are of sufficiently high applied relevance that the proposed solution has high practical value. Much has been written about each of the specific models explored in this paper, all of which are widely used. For example, the instrumental variable (IV) model, presented in Section 4 of the paper, is extremely common in various application areas and dozens of papers have been written about partial ID in this specific model without measurement error. Despite this, to the best of our knowledge, no method currently exists for partial ID in the IV model with measurement error, though in practice measurement error is often present. We advance the methodology available for employing several important models, using a common set of tools that can be easily applied by practitioners to improve their analyses. In light of this, we hope that the reviewers will reconsider their assessment of the value of our contribution.

% For example, the instrumental variable (IV) model, presented in Section 4 of the paper, is extremely common in various application areas and dozens of papers have been written about partial ID in this model without measurement error. Despite this, to the best of our knowledge, no method currently exists for partial ID in the IV model with measurement error. In light of this, we hope that the reviewers will reconsider their assessment of the value of such a partial solution.

\textbf{Reviewer 1 --} "it isn't clear that the bounds will be tight enough to be useful": The bounds presented in this paper are sharp, and thus, are the tightest possible bounds for a particular set of application specific assumptions. If the resulting bounds are not tight enough, then this means that data generating mechanism is insufficient to answer the user's question, regardless of the method used to derive the bounds.

\textbf{Reviewer 2 --} "mostly known constraints...": Our goal is not to present new constraints, but to show a new way to derive sharp bounds under known constraints.

"the scalability of such an approach seems problematic": Unfortunately, Bonet (2001) showed that the exponential complexity is unavoidable in this case. 
We agree that an interesting future direction is approximations that reduce this complexity.

"it is a bit unclear to me if the assumptions used...": Assumptions should be judged by plausibility rather than strength/weakness. It is important to have sharp bounds for as many cases as possible so that practitioners can decide which combination of assumptions is most plausible. In certain applications, A2 and A3 may be more plausible than, say, the error-in-variables model. In others, they may not. A strength of our approach is the ease with which appropriate constraints can be mixed-and-matched; assumptions that are implausible in a specific setting should not be included in the corresponding analysis.

"There is also a very limited evaluation": While it is not clear what additional evaluation the reviewer has in mind, we do not believe that a typical ML style evaluation would add much value in this case. In particular, given a set of assumptions, all methods for deriving sharp bounds (of which there are currently none) will give the same solution.

Clarity and related work: Thank you for these suggestions. We will clarify our contributions and give more background on causal formalizations in future versions.

% \textbf{Reviewer 3 --} "I believe that many of the problems the authors address could be addressed via tools...": We appreciate the reviewer directing us to another interesting area of relevant work; however, we do not believe that the existence of a possible alternative solution invalidates the solution in this work. If there exists literature which solves the problems addressed in this paper using methods from imprecise probability, we request that the reviewer provide a citation to that effect.

\textbf{Reviewer 3 --}: "and not restricted to the linear case": By reference to the 2U algorithm and to Cassio de Campus, it appears the primary tools from imprecise probability the reviewer has in mind are those that handle credal networks. However, credal networks can only represent models that are convex polytopes (see _Credal Networks_, Cozman 2000, especially section 5). This is of course \textit{exactly} the set of models that can be represented by linear constraints on some parametrization of the distribution.  

"the results given in Proposition 1 seems to be...": The model in which Proposition 1 applies is nonconvex under any parameterization, as described in the paper, and cannot be represented as a credal network (we ask the reviewer to try to construct a credal network representing this scenario to understand why). The 2U algorithm operates on credal networks, so it cannot apply to this model. To further see simply that the 2U algorithm cannot produce these bounds, we note that the algorithm can only produce bounds that are made up of a single interval of the form [l, u], but the feasible region in Proposition 1 can, for some joint distributions over A and Y, be made up of disjoint intervals. Additionally, the 2U algorithm returns a numeric result, and Prop 1 is an example of a scenario that admits symbolic results.

"I believe that many of the problems the authors address could be addressed via tools...": Because no further concrete points in defense of this idea were made, we now respond in general. As shown, both concrete points made along these lines were overstated or incorrect. We believe the reviewer has overlooked some of the subtlety required in our setting, in which no information is had about the conditional or marginal distribution of one variable of interest and potentially a latent confounder, but perfect information is had about the joint distribution of observed variable. In addition to the issues mentioned above, it is unclear how the tools of imprecise probability can account for latent unobserved confounders with unknown cardinality, which are central to our analysis of the IV model. Even if such a solution were possible, it is far from clear that it is preferable to our own, especially when it comes to the causal reasoning necessary for calculating our bounds on causal effect. In addition, the potential future existence of such a solution cannot be an argument against the merit of our work. In light of these points, we ask the reviewer to reconsider their position about the utility of our work.

\textbf{Reviewer 4 --} "It would be ideal if the authors could treat an example with real data": Our goal was to present the examples as generic; however, we will include examples that ground them to specific problems.

"Under what assumptions would the quantities be identified": This is a great suggestion. While we are not aware of necessary and sufficient conditions for point ID in these settings, we will include examples of such assumptions to directly contrast with the assumptions used in the paper.

"(A4) seems to be a stronger assumption than the exclusion restriction": As described in Swanson et al. (2018), several versions of the exclusion restriction have been presented at various points in the literature. The version in (A4) is referred to as "Individual-level exclusion" in Swanson et al. (2018) and the reviewer is correct to point out that it is more restrictive than some other versions. We note that (A4) is the version inherent to the widely used FFRCISTG and NPSEM-IE causal models of Bayesian networks. We will add discussion of this, as well as other possible versions, to the paper.

"Some of these assumptions...": Thank you for pointing this out. We will make such assumptions explicit.

"why can't all conditional independence relationships...": This is briefly addressed in Section 3.2. The reviewer is correct to observe that the conditional independence assumptions in 3.2 can be written as equality constraints on various conditional distributions; however, when written in terms of the joint distribution, these constraints are quadratic, and the resulting optimization problem is a non-convex quadratic program which cannot be easily solved. We will clarify this in the text.

\end{document}
