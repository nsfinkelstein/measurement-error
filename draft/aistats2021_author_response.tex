\documentclass{article}

\usepackage{aistats2021_author_response}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{xcolor}         % define colors in text
\usepackage{xspace}         % fix spacing around commands


\begin{document}

We thank the reviewers for their time and constructive feedback. We first
address concerns raised by multiple reviewers about the cited literature and
empirical evaluation, and then address specific concerns raised by individual
reviewers.

\paragraph{Regarding cited literature: } R3 raised the concern that ``the
literature references seem outdated'' and R6 expressed concern that ``most of
the discussed examples have appeared elsewhere in one form or another.'' We
believe we have cited all relevant results in the literature and ask that the
reviewers identify specific works that they believe we are missing. As noted in
Imai and Yamamoto (2010), the measurement error literature has largely focused
on non-differential measurement error and, by extension, has largely ignored
partial identification as evidenced by one of the central measurement error
texts, Carroll et al. (2006). With that said, we like R6's idea to compare
(A0) - (A3) with classical measurement error assumptions and will add this to
the paper.

\paragraph{Regarding empirical evaluation: } The primary claim made in this
paper is that the procedure we develop will produce sharp bounds under the
conditions described. This is a theoretical claim that does not, in and of
itself, require empirical validation. R1 and R6 recommended adding one or more
case studies to demonstrate that the types of assumptions described in this
work can be useful. We agree with this suggestion and will add such cases;
however, we would like to emphasize that the usefulness of these types of
assumptions has been demonstrated repeatedly in the literature (see, e.g.,
Horowitz and Manski (2005), Molinari (2008), Imai and Yamamoto (2010)). We will
make these the connections to previous works clearer in the paper as well.

\paragraph{Reviewer 3:} R3 raised the concern that ``the propositions are
special cases of theorems or propositions from the literature.'' This is not
true of all the propositions (see Proposition 3). More generally, our goal was
to compute partial ID bounds in models with no known analytical bounds and to
do so in a flexible and accessible manner. Our main contribution is combining
these results and applying them to the measurement error problem and, from our
perspective, it is \emph{preferable} to be able to solve such problems using
existing, widely known results. Additionally, R3 expressed concern that
``partial identification requires access to interventional distribution.'' This
appears to be a misunderstanding. While we do indeed optimize over the space of
interventional distributions, we do not assume that this distribution is known
or observed. Rather, the interventional distribution is tied to the observed
data distribution via the \emph{observed data constraint}. Finally, R3
expressed that it was not clear how the proposed solution differs from Imai and
Yamamoto (2010) [13]. In [13], Imai and Yamamota derive analytical bounds
\emph{only} for the specific set of assumptions they consider. These bounds do
not generalize to non-binary variables or other measurement error models. We
view [13] as indicative of our claim that existing work has ``largely focused
on deriving analytic bounds for specific combinations of settings and
assumptions.''

\paragraph{Reviewer 6: } R6 expressed concern that it was unclear whether there
exists a ``realistic scenario'' in which there is measurement error on a
discrete variable. This type of measurement error is common in epidemiology
(see e.g., Rothman, Greenland, and Lash (2008) and Gustafson (2003)), economics
(see various citations in the paper and Horowitz and Manski (1995)), ecology
(see e.g., Solymos, Lele, and Bayne (2012)), and many other fields. We will
clarify this in the introduction. R6 further expressed concern that assumptions
(A0) - (A3) are also ``mathematically convenient''. We would like to clarify
that our claim is not that mathematically convenient assumptions are
\emph{never} substantively justifiable (e.g., approximately Gaussian errors
occur in many important practical cases). Our claim is that we need to expand
the toolbox beyond exact identification so that practitioners are not forced
into a set of assumptions that do not match their application and can test the
sensitivity of their conclusions to any particular assumption (see Imai and
Yamamoto (2010) for an example of this sensitivity analysis that is only
possible via partial ID). For example, assumption (A1) may be justifiable in
cases where errors are clearly differential, such as when study participants
self-report drug use (e.g., Adams et al. (2019)) or in species distribution
surveys (e.g., Solymos, Lele, and Bayne (2012)). We agree that this point is
not clearly made and we will add similar examples for the other assumptions to
the paper. We also emphasize that assumptions (A0) - (A3) are, largely,
\emph{strictly weaker} than traditional measurement error assumptions. For
example, the common assumption of Gaussian errors implies (A3), but the reverse
is clearly not the case. The (A3) assumption therefore applies in strictly more
scenarios than the Gaussian errors assumption. Similarly, most of our
assumptions apply in strictly more scenarios than commonly used identifying
assumptions; this is not surprising, because our assumptions are also weaker in
that they do not yield exact identification.

\end{document}
