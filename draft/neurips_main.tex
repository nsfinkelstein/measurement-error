\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
%     \PassOptionsToPackage{numbers, compress}{natbib}
% before loading neurips_2020

% ready for submission
% \usepackage{neurips_2020}

% to compile a preprint version, e.g., for submission to arXiv, add add the
% [preprint] option:
    \usepackage[preprint]{neurips_2020}

% to compile a camera-ready version, add the [final] option, e.g.:
%     \usepackage[final]{neurips_2020}

% to avoid loading the natbib package, add option nonatbib:
     % \usepackage[nonatbib]{neurips_2020}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfiles}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage{tikz}
\usepackage{centernot}
\usepackage{framed}
\usetikzlibrary{shapes,backgrounds}

\usepackage{xr}
\externaldocument{appendix}

\usepackage{algorithm}
\usepackage{algorithmicx}

\usepackage{amsthm}       % hyperlinks
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{corollary}{Corollary}
\newtheorem{proposition}{Proposition}
% \newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{assumption}{Assumption}

\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]

\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\title{Partial Identifiability in Discrete Data With Measurement Error}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to break the
% lines. Using \AND forces a line break at that point. So, if LaTeX puts 3 of 4
% authors names on the first line, and the last on the second line, try using
% \AND instead of \And before the third author name.

\author{%
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \AND
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
  % \And
  % Coauthor \\
  % Affiliation \\
  % Address \\
  % \texttt{email} \\
}

\begin{document}

\maketitle

\begin{abstract} 
	
  When data contains measurement errors, it is necessary to make assumptions
  relating the observed, erroneous data to the unobserved true phenomena of
  interest. These assumptions should be justifiable on substantive grounds, but
  are often motivated by mathematical convenience, for the sake of exactly
  identifying the target of inference. We adopt the view that it is preferable
  to present bounds under justifiable assumptions than to pursue exact
  identification under dubious ones. We develop methods to bound distributional
  and causal parameters of the distribution of a discrete variable that is
  subject to measurement error. Our approach is to translate the available
  modeling assumptions into a set of constraints and to numerically bound the
  parameter of interest by finding its maximum and minimum subject to these
  constraints. We show that this approach yields sharp bounds in a number of
  frequently occurring settings, including several for which no sharp bounds
  are currently known.

\end{abstract}

% Introduction
\subfile{intro}

% Problem setup and definitions
% \subfile{setup}

% Bounds on the marginal of the unobserved variable with no other assumptions
\subfile{base}

% Bounds on the marginal when there is an auxiliary variable
\subfile{iv}

% Bounds on the causal effect
\subfile{causal-bounds}

% \subfile{symbolic-bounds}

% \subfile{examples}

% Application to IV with measurement error on outcome
% \subfile{application}

% Background
\subfile{background}

% Conclusion
\subfile{conclusion}

\section*{Broader impact} 

We believe this work will be of practical use to scientists with imperfectly
measured data, who cannot credibly impose sufficiently strong assumptions to
obtain full identification. Our results allow for substantive inference in the
absence of such assumptions. We believe these results will especially be of use
to public health officials and social scientists, who rely on surveys and other
means of self-reporting, which is especially susceptible to arbitrary
measurement error. We also believe our view of measurement error --- as
inference on a latent variable with known cardinality and one or more proxies
in a causal model --- will help drive a more general theory of non-parametric
partial identification of the distributions of variables with measurement
error. We do not believe that there is much risk that this work will have a
negative social impact except insofar as any statistical technique can be
misused.

% \begin{ack}
% \end{ack}

\bibliographystyle{plain}
\bibliography{references}

% \appendix
% \subfile{proofs}

\end{document}