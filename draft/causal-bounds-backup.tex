\documentclass[main.tex]{subfiles}

\begin{document}
\section{Causal Bounds Under Measurement Error On Outcome}

Let $X$ be an unobserved variables of interest, and $Y$ be a noisy proxies for
$X$. Then let $\bf A$ and $\bf B$ be sets of variables causally irrelevant to
$Y$ given $X$. We are interested in functions of the potential outcomes
$X({\bf a})$ and $X({\bf b})$. Proofs are deferred to Appendix \ref{app:proofs}.

In the IV case, Balke's approach can be easily adapted to bound causal
quantities. This is done for binary variables in \cite{sachs2020linear} under a
monotonicty assumption.

\subsection{Assumption-Free Bounds on the Proportion Affected}

Due to the causal irrelevance of $A$ and $B$ to $Y$ given $X$, we know that $Y$
will only take different values under the interventions $A = a$ and $B = b$ if
$X$ does as well. We will therefore be interested in the mass of the smallest
set required to take different values in the distributions $P(Y(a))$ and
$P(Y(b))$. We note that this problem can also be characterized as a linear
program. Let $\lambda_{ij} = P(Y(a) = x_i, Y(b) = x_j)$. Then

\begin{align}
  \label{eq:omega-lp}
  \min \sum_{i \neq j} \lambda_{ij} \text{~~s.t.~~}
  \sum_{j} \lambda_{ij} =  P(Y(b) = x_j)
  ~~\text{and}~~
  \sum_{i} \lambda_{ij} =  P(Y(a) = x_i)
  ~~\text{and}~~
  \lambda_{ij} \ge 0
\end{align}

In this linear program, we are treating $P(Y(a), Y(b))$ as a stochastic matrix
with known margins. Unlike the linear programs considered above, this one can be
solved analytically, which has two advantages. First, vertex enumeration is
computationally intensive at high cardinalities. Second, it requires a fixed
number of variables and inequalities, meaning that a different vertex
enumeration problem would need to be run for every cardinality. We present the
analytic solution in the following Lemma.

\begin{lemma}
  \label{prop:omega}
  $\min \sum_{i \neq j} \lambda_{ij} =
  \frac{1}{2} \sum_i | P(Y(a) = x_i) - P(Y(b) = x_i)|$
\end{lemma}

We can now use this quantity to obtain a lower bound on the proportion of the
population who have different outcomes under the two treatments.

\begin{proposition}
  \label{prop:proportion-affected}
  $P(X(a) \neq X(b)) \ge \frac{1}{2} \sum_i | P(Y(a) = x_i) - P(Y(b) = x_i)|$
\end{proposition}

This bound may be useful in scenarios in which the analyst is trying to
understand how much change an intervention may cause. For example, an economist
or government policy type may want to obtain a lower bound on how many people
would change behavior in response to a change in policy (in which case $\bf B$
would be the empty set).

\subsection{Monotonicity Assumptions}

There are three commonly made monotonicty-style assumptions that may be of
interest in this scenario. These monotonicy assumptions are simplest when we
consider single outcomes instead of sets, as ``monotonicity'' implies an
ordering.

\begin{enumerate}
\item[(A1)] \textit{No Underreporting}: $Y \ge X$
\item[(A2)] \textit{Positive Effect of Truth on Proxy}: $x \ge x' \implies Y(x) \ge Y(x')$
\item[(A3)] \textit{Positive Effect of Treatment on Truth}: $a \ge a' \implies X(a) \ge X(a')$
\end{enumerate}

In this section, we provide bounds on the average treatment effect under each of
these assumptions. We assume $X$ and $Y$ have the same domain, and $x_0$ is the
lowest value in that domain.

\begin{proposition}
  \label{prop:bounds-a1}
  Under (A1), $E[{X(a) - X(b)}] \in \big[x_0 - E[{Y(b)}], E[{Y(a)}] - x_0\big]$.
\end{proposition}

No distribution ${Y(a)}$ can falsify the statement $P(X(a) = x_0) = 1$ under
(A1), so it always possible that the expected value of $X(a)$ is $x_0$ at its
lowest. Then we note that if there is any over-reporting the value of $E[X(a)]$
subceeds the value of $E[Y(a)]$, from which it follows that the value of
$E[X(a)]$ cannot be above $E[Y(a)]$. This yields the bounds given above.

We now look at Assumption (A2). Under this assumption $Y(a) > Y(b) \implies X(a)
> X(b)$, and $Y(a) < Y(b) \implies X(a) < X(b)$. We let $\delta$ be the smallest
difference bewteen levels of $X$, and $\eta$  indicate the largest. This yields
that under (A2), $Y(a) > Y(b) \implies \delta < X(a) - X(b) < \eta$, and $Y(b) >
Y(a) \implies \delta < X(b) - X(a) < \eta$. It follows that an upper bound on
the ATE can be found by solving the following optimization problem.

\begin{align*}
  \text{max~} &\eta \sum_{i < j} \lambda_{ij} - \delta \sum_{i > j}
\lambda_{ij}\\
  \text{~s.t.~}
  &\sum_{j} \lambda_{ij} =  P(Y(b) = x_j)
  ~~\text{and}~~
  \sum_{i} \lambda_{ij} =  P(Y(a) = x_i)
  ~~\text{and}~~
  \lambda_{ij} \ge 0
\end{align*}

To find the lower bound, we solve a problem with the same constraints, but with
the objective $\min \delta \sum_{i < j} \lambda_{ij} - \eta \sum_{i > j}
\lambda_{ij} $. We can now use these values to bound the ATE under Assumption
(A2). For any specific cardinality, they could be symbolically obtained through
the vertex enumeration procedure discussed earlier. For a specific distribution,
they can be solved numerically. However, we would like bounds that can be
symbolically expressed for arbitrary cardinalities. Note that if $P(Y(a))$
and/or $P(Y(b))$ are only partially identified, this linear program works as
well, except that the marginal equalities are now inequalities, and a constraint
that each margin must sum to $1$ must be added.

We now present the following symbolic bounds. Let $\gamma_{\max}$ be the largest
proportion of the population that can increase in the move from $Y(a)$ to
$Y(b)$, and $\beta_{\min\max}$ be the smallest proportion of the population that
can decrease, subject to $\gamma_\max$ increasing. Then we present the following
bounds.

\begin{proposition}
  \label{prop:bounds-a2}
  Under (A2), $E[X(a) - X(b)] \in \big[
    \delta\gamma_{\min\max} - \eta\beta_{\max},
    \eta\gamma_{\max} - \delta\beta_{\min\max}
  \big]$.
\end{proposition}

The intuition behind these bounds is as follows. If we see an increase between
$Y(a)$ and $Y(b)$, we know there must have been an increase between $X(a)$ and
$X(b)$, but we don't know the size of that increase. To obtain a lower bound, we
must assume the increases are all as small as possible, and the decreases are
all as large as possible. Then we must find the proportion of the population
that increases and decreases the optimizes the objective, under the above
assumption about the sizes of the changes. We note that an increase of $\beta$
always costs, at most, an equal increase in $\gamma$. Because $\eta > \delta$,
it is always worth increasing $\beta$ by as much as possible to obtain the lower
bound, as any increase in $\beta$ decreases the objective. Then, clearly we want
to increase the objective by as little as possible, and hence hold $\gamma$ to
as low a value as possible. Details are found in the proof.

Below, we derive a fast symbolic procedure that works for arbitrary
cardinalities for obtaining $\gamma_\max$ and $\beta_{\min\max}$. It can be
flipped to obtain $\beta_\max$ and $\gamma_{\min\max}$.

\begin{algorithm}[h!]
	\caption{Calculating $\gamma_{\max}, \beta_{\min\max}$}
  \label{alg:lambda-values}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $P(Y(a)), P(Y(b))$
		\Statex \hspace{-6.5mm} \textbf{Output:} $\gamma_{\max}, \beta_{\min\max}$
    \State \textbf{for} $j$ \textbf{in} $cardinality(X) - 1, \dots, 1$
    \State \hspace{3mm} \textbf{for} $i$ \textbf{in} $j - 1, \dots, 0$
    \State \hspace{6mm} $\lambda_{ij} = \min\{
      P(Y(b) = x_j) - \sum_{i < i' < j} \lambda_{i'j},
      P(Y(a) = x_i) - \sum_{j < j'} \lambda_{ij'}
    \}$
    \State \textbf{for} $i$ \textbf{in} $0, \dots, cardinality(X) - 1$
    \State \hspace{3mm} \textbf{for}
               $j$ \textbf{in} $cardinality(X), \dots, i + 1$
    \State \hspace{6mm} $\lambda_{ij} = \min\{
      P(Y(b) = x_j) - \sum_{j < i < i'} \lambda_{i'j} - \sum_{j > i} \lambda_{ij},
      \newline ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      P(Y(a) = x_i) - \sum_{j' < j} \lambda_{ij'} - \sum_{j > i} \lambda_{ij}
    \}$
    \State $\gamma_{\max} = \sum_{i < j} \lambda_{ij}$
    \State $\beta_{\min\max} = \sum_{i > j} \lambda_{ij}$
	\end{algorithmic}
\end{algorithm}

The intuition behind this procedure is as follows. When increasing, we increase
everything as little as possible, so that as much as possible can be increased.
When decreasing the opposite holds, to increase everything as little as
possible.

Then we note the following. For any fixed allocation of $\lambda_{ij}$ for $i <
j$, the $\beta_{\min\max}$ procedure obtains the right value for $\beta$. Then
we note that any other allocation for $\gamma_\max$ that also attains
$\gamma_\max$ will decrease $\beta_{\min\max}$ (see sheet) leading to a worse
result. Therefore this procedure finds the optimum. TODO: check for cardinality
3 that this procedure returns sharp bounds.

We now consider (A3).

\begin{proposition}
  \label{prop:bounds-a3}
  Under (A3), $E[X(a) - X(a')] \ge \max_{a' \le a_1 < a_2 \le a} \delta
  \omega(Y(a_1), Y(a_2))$
\end{proposition}

Under this assumption, we know that any change observed in $Y$ corresponds to an
increase in $X$. Because we don't know the size of the increase, to obtain a
lower bound we must assume it is as small as possible. We must try all values of
$a_1$ and $a_2$ between $a'$ and $a$ because, without assumptions about the
measurement error, we may see a larger $\omega$ distance for an intermediate
value of the treatment $A$. Due to the monotonicity assumption, the contrast
between $a'$ and $a$ will be larger than for any intermediate values.

We will now examine bounds obtained under multiple assumptions. When (A1) is
combined with (A2), we will in some cases be able to place bounds on the
\textit{amount} of an increase or decrease, rather than simply using the
extremes of $\delta$ and $\eta$ as in Proposition \ref{prop:bounds-a2}. The
lower (upper) bound obtained in Proposition \ref{prop:bounds-a2} is obtained
under the assumption that all individuals who decrease (increase) between $Y(a)$
and $Y(b)$ do so by the maximum possible amount, $\eta$. Under assumption (A1),
we can falsify this assumption for some portion of the population. For example,
anyone with $Y(b) < x_{\max}$ cannot have increased by $\eta$, as under (A1)
$Y(b) < x_{\max} \implies X(b) < x_{\max}$. Likewise, anyone with $Y(a) <
x_{\max}$ cannot have decreased by $\eta$.

To take advantage of this additional information, we introduce the following
notation. Let $\xi^k = x_k - x_0$. Then for any portion of the population that
has $Y(a) < x_k \land Y(b) = x_k$, we know $X(b) - X(a) \le \xi^k$. This is
because, due to assumption (A1), $Y(b) = x_k \implies X(b) \le x_k$, so the
greatest possible increase - which occurs when $X(a) = x_0$ - is $\xi^k$.

We can now express the upper bound in this setting as the following linear
program:

\begin{align*}
  \text{max~} & \sum_k \xi^k \sum_{i < k} \lambda_{ik} -
                \delta \sum_{i > j}
\lambda_{ij}\\
  \text{~s.t.~}
  &\sum_{j} \lambda_{ij} =  P(Y(b) = x_j)
  ~~\text{and}~~
  \sum_{i} \lambda_{ij} =  P(Y(a) = x_i)
  ~~\text{and}~~
  \lambda_{ij} \ge 0
\end{align*}

The lower bound can be obtained by minimizing the objective $\delta \sum_{i < j}
\lambda_{ij} - \sum_k \xi^k \sum_{j < k} \lambda_{kj}$ over the same constraints.

We let $\gamma_{\max}^k = \sum_{i < k} \lambda_{ik}$ denote, with $\lambda_{ik}$
as defined in the first algorithm (where $\gamma_{\max}$ is defined).
Intuitively, this gives priority to larger values of $k$, as is necessary to
obtain bounds. $\gamma_{\max}^k$ is the largest proportion of the population
that could have increased by $\xi_k$, conditional on that $\gamma_{\max}^{k'}$
takes its largest possible value for $k > k'$. $\beta_{\max}^k$ can be similarly
defined, noting that $Y(a) = x_k \implies X(a) \le x_k$, such that a decrease of
at most $\xi^k$ is possible for the proportion of the population that decreases
from $x_k$. Then $\beta_{\max}^k = \sum_{k > j} \lambda_{kj}$.

$\gamma_{\max}^k$ can be conceived of as optimizing $\sum_k \xi^k \sum_{i < k}
\lambda_{ik}$, subject to the same constraints we saw earlier, with the
additional constraints that the values of $\gamma^{k'}_\max$ with $k' > k$ take
their maximum values (with recursive conditionality).

We can now use these terms to express bounds under assumptions (A1) and (A2).

\begin{proposition}
  \label{prop:bounds-a1-a2}
  Under (A1) \& (A2), $E[X(a) - X(b)] \in \big[
  \delta\gamma_{\min\max} - \sum_i \xi_i \beta_{\max}^i,
  \sum_i \xi_i \gamma_{\max}^i - \delta\beta_{\min\max}
  \big]$
\end{proposition}

We now define a new quantities, and $\nu_{\max}$, that indicates the largest
proportions of the distribution that could have increased when no decrease is
possible. To illustrate the difference between $\nu$ and $\gamma$, consider the
ternary distributions $[0.2~~0.3~~0.5]$ and $[0.1~~0.3~~0.6]$. Then
$\gamma_{\max} = 0.5$, but $\nu_{\max} = 0.2$. If decreases are allowed, then we
can have the density at $0$ and $1$ move to $2$, and $.3$ move from $2$ to $1$,
and $.1$ move from $2$ to $0$. If no decrease is allowed, then the measure of
the mass that moves from $0$ to $1$ is bounded from above by $.1$. Note that the
minimum portion of the population that increases is the same regardless of
whether decreases are allowed, as in both cases it occurs when no portion of the
population decreases. $\nu_{\max}$ can be found by optimizing the objective
$\sum_{i < j} \lambda_{ij}$, subject to the additional constraint that $\sum_{i
  > j} \lambda_{ij} = 0$.

We present the following calculation of $\nu_{\max}$.

\begin{algorithm}[h!]
	\caption{Calculating $\nu_{\max}$}
  \label{alg:lambda-values}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $P(Y(a)), P(Y(b))$
		\Statex \hspace{-6.5mm} \textbf{Output:} $\nu_{\max}$
    \State \textbf{for} $j$ \textbf{in} $cardinality(X) - 1, \dots, 1$
    \State \hspace{3mm} \textbf{for} $i$ \textbf{in} $j - 1, \dots, 0$
    \State \hspace{6mm} $\lambda_{ij} = \min\{
    P(Y(b) = x_j) - P(Y(a) = x_j) + \sum_{j' > j} \lambda_{ij'} -
    \sum_{i < i' < j} \lambda_{i'j},
    \newline ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      P(Y(a) = x_i) - \sum_{j < j'} \lambda_{ij'}
    \}$
    \State $\nu_{\max} = \sum_{i < j} \lambda_{ij}$
	\end{algorithmic}
\end{algorithm}

And the following for calculation of $\nu_{\min}$.

\begin{algorithm}[h!]
	\caption{Calculating $\nu_{\min}$}
  \label{alg:lambda-values}
	\begin{algorithmic}[1]
		\Statex \hspace{-6.5mm} \textbf{Input:} $P(Y(a)), P(Y(b))$
		\Statex \hspace{-6.5mm} \textbf{Output:} $\nu_{\min}$
    \State \textbf{for} $j$ \textbf{in} $cardinality(X) - 1, \dots, 1$
    \State \hspace{3mm} \textbf{for} $i$ \textbf{in} $cardinality(X), \dots, j +
    1$
    \State \hspace{6mm} $\lambda_{ij} = \min\{
      P(Y(b) = x_j) - P(Y(a) = x_j) - \sum_{i' > i} \lambda_{i'j},
      \newline ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      P(Y(b) = x_i) - P(Y(a) = x_i) - \sum_{j' > j} \lambda_{ij'}, 0
    \}$
    \State \hspace{3mm} \textbf{for} $i$ \textbf{in} $0, \dots, j - 1$
    \State \hspace{6mm} $\lambda_{ij} = \min\{
      P(Y(b) = x_j) - P(Y(a) = x_j) - \sum_{i' < i \lor i' > j} \lambda_{i'j},
      \newline ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      P(Y(b) = x_i) - P(Y(a) = x_i) - \sum_{j' > j} \lambda_{ij'}, 0
    \}$
    \State $\nu_{\min} = \sum_{i < j} \lambda_{ij}$
	\end{algorithmic}
\end{algorithm}

This allows us to express the bounds under asumptions (A1) and (A2). Note that
this problem can also be expressed as a linear program:

\begin{align*}
  \text{max~} & \eta \sum_{i < j} \lambda_{ij}\\
  \text{~s.t.~}
  &\sum_{j} \lambda_{ij} =  P(Y(b) = x_j)
  ~~\text{and}~~
  \sum_{i} \lambda_{ij} =  P(Y(a) = x_i)
  ~~\text{and}~~
  \sum_{i > j}\lambda_{ij} = 0
  ~~\text{and}~~
  \lambda_{ij} \ge 0
\end{align*}

Which yields the following bounds:

\begin{proposition}
  \label{prop:bounds-a2-a3}
  Under (A2) \& (A3), $E[X(a) - X(b)] \in \big[
  \delta \nu_{\min}, \eta \nu_{\max}
  \big]$
\end{proposition}

This bound comes from the idea that if there is any change in $X$ due to
difference in treatments, that change must be reflected by positive in $Y$ under
these two assumptions.

We now define $\nu_{\max}^k$ analogously to $\gamma_{\max}^k$, and present the
final bound of the section. We note, finally, that $\sum_i\xi_i\nu_{\max}^k$ can
also be obtained through optimization of $\sum_k \xi^k \sum_{i < k}
\lambda_{ij}$, under the same constraints as could be used for $\nu_{\max}$.

This problem can also be expressed as a linear program.

\begin{align*}
  \text{max~} & \sum_k \xi^k \sum_{i < k} \lambda_{ik}\\
  \text{~s.t.~}
  &\sum_{j} \lambda_{ij} =  P(Y(b) = x_j)
  ~~\text{and}~~
  \sum_{i} \lambda_{ij} =  P(Y(a) = x_i)
  ~~\text{and}~~
  \sum_{i > j}\lambda_{ij} = 0
  ~~\text{and}~~
  \lambda_{ij} \ge 0
\end{align*}

Which yields the following bounds.

\begin{proposition}
  \label{prop:bounds-a1-a2-a3}
  Under (A1)(A2) \& (A3), $E[X(a) - X(b)] \in \big[
  \nu_{\min}\delta, \sum_i\xi_i\nu_{\max}^i
  \big]$
\end{proposition}

\subsection{Limited Errors Assumption}

It is commonly assumed that at least $\lambda$ of the population is measured
without error. If we extend this assumption to be that at least $\lambda$ of the
population will be measured without error under any intervention, we can use it
to bound $E[X(a)]$ given the distribution $P(Y(a))$. This too can can be
expressed as a linear program. In this case, we treat $P(Y(a), X(a))$ as a
stochastic matrix with only one margin, $P(Y(a))$, known. In this case, let
$\phi_{ij} = P(X(a) = x_i, Y(a) = x_j)$. Then the linear programming problem can
be expressed as follows:

\[
\text{optimize~~} \sum_i x_i \sum_{j} \phi_{ij}
\text{~~s.t.~~}
  \sum_{i} \phi_{ij} =  P(Y(a) = x_j)
  ~~\text{and}~~
  \phi_{ij} \ge 0
\]

The same can be done for the intervention $\bf B = b$. Note that these two
optimization problems include none of the same parameters, and can be solved
seperately. Then the bounds under this assumption can be written simply as:

\[
  E[X(a) - X(b)] \in \big[
  E^{ \min }[Y(a)] - E^{ \max }[Y(b)],
  E^{ \max }[Y(a)] - E^{ \min }[Y(b)]
  \big]
\]

Finally, we note that under assumption (A3), the optimization problems for
$E[X(a)]$ and $E[X(b)]$ are no longer separate, as we also have the constraints
$\sum_{i \le k} P(X(a) = x_i) \ge \sum_{i \le k} P(X(b) = x_i)$ for all $k$. This
yields the following linear program that can be optimized directly to find
bounds on the ATE:

\begin{align*}
  \text{optimize~~} &\sum_i x_i
                      \sum_{j} \phi^a_{ij} -
                      \sum_i x_i \sum_{j} \phi^b_{ij}\\
  \text{~~s.t.~~} &\sum_{i} \phi^a_{ij} =  P(Y(a) = x_j)
  ~~\text{and}~~
  \sum_{i} \phi^b_{ij} =  P(Y(b) = x_j)
  ~~\text{and}~~
  \phi^b_{ij}, \phi^a_{ij} \ge 0\\
  &\sum_{i \neq j} \phi^a_{ij} \le \lambda
  ~~\text{and}~~
  \sum_{i \neq j} \phi^b_{ij} \le  \lambda \\
  &\sum_{i \le k, j} \phi^a_{ij} \ge \sum_{i \le k,j} \phi^b_{ij}
    \text{~~ for all~} k
\end{align*}

Where the last constraint corresponds to assumption (A3). Assumption (A1) can be
built into this linear program. (A1) says $\sum_{i > j} \phi^c_{ij} = 0$ for $c
\in \{a, b\}$ (no under-reporting). (A2) cannot be built into this linear
program, as it requires linear constraints on the joint $P(Y(a), Y(b))$, which
cannot be expressed linearly in terms of the joints $P(X(a), Y(a))$ and $P(X(b),
Y(b))$, which are the joints we're dealing with here, as they are necessary to
express the limited errors assumption linearly.

Can we define a procedure to solve this program? Or perhaps it cannot be done
because we have but one fixed margin instead of two.

\subsection{Partially Identified Causal Effects}

We note that all the bounds developed above apply equally well when the effect
of $\bf A$ and $\bf B$ are partially identified, rather than fully identified.
In such cases, we simply find the maximum and minimum distance ($\omega, \gamma,
\beta$) between the partially identified distributions. We consider the
following case:

\[
  Z \rightarrow A \rightarrow X \rightarrow Y,
  A \leftrightarrow Y,
  A \leftrightarrow X,
  X \leftrightarrow Y
\]


\subsection{Proofs}

\begin{lemma}[Moving Mass in a Stochastic Matrix w/ Fixed Margins]
  Let $A$ and $B$ represent any two stochastic matrices with the same margins.
  Let $\Delta(\epsilon, i, j, i', j')$ represent a procedure whereby
  $\lambda_{ij}$ and $\lambda_{i'j'}$ are increased by $\epsilon$, and
  $\lambda_{i'j}$ and $\lambda_{ij'}$ are decreased by $\epsilon$.

  Then it is possible to move from $A$ to $B$ using only $\Delta$ operations
  that result in another stochastic matrix $C$ with the same margins again, such
  that $C$ and $B$ are strictly ``closer'' than $A$ and $B$, in terms of their
  summed elementwise absolute difference.
\end{lemma}

\begin{proof}
  First, we note that any $\Delta$ operation must maintain the margins, as the
  same amount of mass is being added and subtracted from rows $i, i'$ and
  columns $j, j'$.

  Consider any matrices $A$ and $B$. Then there must be $i, i', j, j'$ such that
  $\lambda^B_{ij}> \lambda^A_{ij}$, $\lambda^B_{ij'} < \lambda^A_{ij'}$,
  and $\lambda^B_{i'j} < \lambda^A_{i'j}$. If the first condition does not hold,
  the matrices (both of whose elements all sum to 1) must be the same matrix,
  and if the second does not hold they cannot have the same margins. Then we
  find the smallest such difference, such that $\lambda^B_{ij} > \lambda^A_{ij}
  = \epsilon$. Then, a delta move with $\Delta(\epsilon, i, j, i', j')$ will
  reduce the distance ($\sum_{ij} | \lambda^A_{ij} - \lambda^B_{ij}|$) by at
  least $3 \epsilon$, and increase it by at most $\epsilon$ (for
  $\lambda_{i'j'}$). To see that this procedure terminates for finite matrices,
  we note that one of two things must hold for each $\Delta$ move as described.
  Either the move reduces the number of elements for which the matrix are not
  equal by at least one (if the $ij$ distance is now $0$, and none of the other
  pairs were brought away from $0$), or the largest positive distance from an
  element in $A$ to an element in $B$ remains \textit{the same} $\epsilon$ (if
  the $i'j'$ distance started out at $0$, and was increased to $\epsilon$). Thus
  convergence is achieved in a finite number of steps for finite matrices.
\end{proof}

\begin{proof}[Proof for $\omega$]
  We consider the $m \times m$ stochastic matrix $\Lambda$ with fixed margins.
  We seek to minimize the sum of the off-diagonal elements, which is equivalent
  to maximizing the sum of the diagonal elements. The diagonal $\lambda_{ii}$
  attains its maximum as follows: $\lambda_{ii} = \min\{P(Y(a) = x_i), P(Y(b) =
  x_i)\}$; it cannot be larger without violating the margins, due to the
  positivity of all $\lambda_{ij}$. It need not be larger because only the
  margins are fixed, and the margins are always attainable at that value.
  Therefore:

  \[\omega = 1 - \sum_i \min\{P(Y(a) = x_i), P(Y(b) = x_i)\} =
    \frac{1}{2}\sum_i |P(Y(a) = x_i) - P(Y(b) = x_i) | \]
\end{proof}

\begin{proof}[Proof for Proposition \ref{prop:bounds-a1}]
  First we note that through the exclusion restriction, $A$ can only affect $Y$
  through $X$, such that if $Y$ changes it must be due to a change in $X$.

  \[
    Y(a) \neq Y(b) \implies X(a) \neq X(b)\\
  \]

  This immediately yields the following, through the property that antecedent
  events are by definition subsets of consequent events.

  \[P(X(a) \neq X(b)) \ge P(Y(a) \neq Y(b))\]

  Finally, by the definition of $\omega$ and the above, we have

  \[P(X(a) \neq X(b)) \ge P(Y(a) \neq Y(b)) \ge \omega\]
\end{proof}

\begin{proof}[Proof for Proposition \ref{prop:bounds-a3}]
  Under (A3), any change between $X(a)$ and $X(b)$ is an increase. By earlier
  results, the change is at least $\omega$, and by our knowledge of the domain
  of $X$, any change in $X$ must be a change of at least $\delta$. Moreover, if
  we have knowledge of interventions $c$ and $d$, such that the effect of
  treatment is known to be ordered $b \le d < c \le a$, then any change observed
  between $d$ and $c$ must be smaller than the change observed between $b$ and
  $a$, yielding the result

  \[E[X(a) - X(b)] \ge \max_{b \le d < c \le a}\delta \times \omega(Y(d),
    Y(c))\]
\end{proof}

\begin{proof}[Proof of Proposition \ref{prop:bounds-a2}]
  First, we note that this linear program takes its maximum value when
  $\sum_{i < j} \lambda_{ij}$ takes its maximal value, which we denote
  $\gamma_{\max}$. This is a result of two points. First, we note that $\eta$ is
  necessarily greater than $\delta$, so if it turns out to be the case that any
  increase of $\gamma$ comes at the cost of at most an incresae of the same
  amount in $\beta$, it will always be worth making that trade. We demonstrate
  that this is in fact the case in the paragraph below.
  
  We examine how mass can be moved around the stochastic matrix in order to
  demonstrate that a marginal increase in $\gamma$ costs at most the same
  increase in $\beta$. An increase in $\lambda_{ij}$ requires a decrease in the
  row $i$ and the column $j$, each equal to the increase, to offset the change
  and maintain the fixed marginals. We suppose the decrease in row $i$ happens
  in column $j'$, and the increase in column $j$ happens in row $i'$. Then, row
  $i'$ and column $j'$ each need an increase of the same quantity, which is
  resolved by increasing $\lambda_{i'j'}$. We now do a case analysis of how this
  tradeoff can be conducted, if $j > i$. If $\lambda_{i'j'}$ is in the upper
  right triangle as well, then so are $\lambda_{ij'}$ and $\lambda_{i'j}$, and
  we have not changed $\gamma$. If $\lambda_{i'j'}$ is on the diagonal, it
  follows that $\lambda_{ij'}$ and $\lambda_{i'j}$ are in the upper right
  corner, and we have decresed $\gamma$ by $\epsilon$ without changing $\beta$.
  Now suppose that $\lambda_{i'j'}$ is in the bottom left triangle. Then
  $\lambda_{ij'}$ and $\lambda_{i'j'}$ may both be on the diagonal, in which
  case $\gamma$ and $\beta$ have each been equally incresaed. Now suppose one of
  $\lambda_{ij'}$ and $\lambda_{i'j}$ is on the diagonal and the other is in the
  top right triangle. Then $\beta$ has been increased with no change to
  $\gamma$. Finally, suppose that one is on the diagonal and the other is in the
  bottom left triangle. Then $\gamma$ has been increased at no change to
  $\beta$. Due to the linearity of the problem, any move in mass across the
  matrix can be represented as the sum of the kinds of movements defined here.
  This shows that an increase in $\gamma$ comes at the cost of, at most, an
  equal increase in $\beta$.

  Then, it is also clear that the objective is maximized when $\beta$ is as
  small as it can be, consistent with $\gamma = \gamma_\max$. We denote this
  value as $\beta_{\min\max}$, yielding the stated bounds.
\end{proof}

\begin{proof}[Proof of Proposition \ref{prop:bounds-a1-a2}]
  This proof builds on the above. The only change is as follows. In this
  formulation, we consider $\gamma_\max^K$, i.e. the proportion of the
  population that had any chance of increasing the full $\xi^K$. This is the
  proportion of the population that may have increased to $x_K$. We now need to
  show that the cost of an increase to $\gamma_\max^K$ is at most an increase in
  $\beta$ of equal size, OR a decrease in $\gamma_\max^k$ of equal size, for $k
  \neq K$.

  We use the same case analysis as before. Note that $\gamma^k = \sum_{i < k}
  \lambda_{ik}$. Then the same reasoning applies for trading off mass between
  $\gamma^k$ and $\beta$. Now we consider $\lambda_{ik}$ with $i < k$, i.e. in
  the top right triangle. Now if both $\lambda_{ij'}$ and $\lambda_{i'j}$ are in
  the bottom left triangle, we get a free increase of $\gamma^k$. If
  $\lambda_{i'j'}$ is on the diagonal, then we have no net change in $\gamma^k$,
  but have a decrease in $\gamma^{k'}$ with $k > k'$, which is strictly a bad
  trade. In fact, so long as $\lambda_{i'k}$ is above the diagonal, we have no
  increase in $\gamma^k$. Therefore we now consider cases with $\lambda_{i'k}$
  on or below the diagonal, such that $\lambda_{i'j'}$ must be below the
  diagonal. Then if $\lambda_{i'k}$ is below the diagonal, we have the following
  cases. If $\lambda_{ij'}$ is above the diagonal, we increase $\gamma^k$ at the
  expense of the same amount of $\gamma^{j'}$, with no change in $\beta$. If
  $\lambda_{ij'}$ is on the diagonal, then we increase $\gamma^k$ at no change
  to any other $gamma^{k'}$ or to $\beta$. If $\lambda_{ij'}$ is below the
  diagonal, we increase $\gamma^k$ and decrease $\beta$ by the same amount.

  Now if $\lambda_{i'k}$ is on the diagonal, we have the following cases. If
  $\lambda_{ij'}$ is on the diagonal as well, then we increase $\gamma^k$ at the
  expense of the same increase in $\beta$. If $\lambda_{ij'}$ is below the
  diagonal, then we increase $\gamma^k$ at no cost to $\beta$.

  If $\lambda_{ij'}$ is above the diagonal, we increase $\gamma^k$, but decrease
  $\gamma^{k'}$ for some $k' < k$ \textit{and} increase $\beta$ by the same
  amount each. In the \textit{worst} case, this trade is still ``break-even'',
  because by definition $\xi^k \ge \xi^{k'} + \delta$ for $k' < k$, so the
  increase $\epsilon \xi^k$ is greater than or equal to the decrease $\epsilon
  (\delta + \xi&^{k'})$. If $k' > k$, then an increase of $\gamma^k$ comes at
  the cost of a decrease in $\gamma^{k'}$ for $k' > k$, which is against the
  rules for the definition of $\gamma^k_\max$, which assumes that all larger
  values of $k$ have $\gamma^k_\max$ take their largest value (recursively).
\end{proof}

\begin{proof}[Proof of Proposition \ref{prop:bounds-a2-a3}]
  We have $Y(a) > Y(b) \implies X(a) > X(b)$, so any proportion of the
  population that must have increased from $Y(b)$ to $Y(a)$ must have increased
  in $X$ as well. Then, we have that only increases are possible, through $a \ge
  a' \implies X(a) \ge X(a')$, and $X(a) \ge X(a') \implies Y(a) \ge Y(a')$.
  Thus no proportion of the population may decrease. Therefore the lower bound
  is attained when the smallest possible portion of the population increases by
  the smallest possible amount, and the upper bound is attained when the largest
  possible proportion increases by the largest possible amount.
\end{proof}

\begin{proof}[Proof of Proposition \ref{prop:bounds-a1-a2-a3}]
  Building off of previous proofs, we have the same as the above, but through
  (A1), we have a way of bounding increases from above. In particular, if $Y(b)
  = x_k$, then the increase is at most $\xi^k$. Combining this idea with the
  previous proof yields the bounds.
\end{proof}

Our general strategy for proving the correctness of these procedures will be to
show that at the end of the procedure, no $\Delta$ move can be made to increase
(or decrease) the target. The proof for $\beta_{\min\max}$ will be just slightly
more involved - we will need to show that at the end of the procedure, no move
can be made to decrease the target without also decreasing $\gamma_\max$.

\begin{proof}[Proof of $\gamma_\max$ procedure]
  We demonstrate that at the end of this procedure, no trade is available to
  increase $\gamma$. Were such a trade available, there would exist $i, j, i',
  j'$ such that $i < j$, and both $i' \ge j$ and $i \ge j'$. However, according
  to our procedure, no such indices exist with $\lambda_{i'j}, \lambda_{ij'} >
  0$.
  
  Outline: show which indices must be zero at the end of the procedure
\end{proof}

\begin{proof}[Proof of $\beta_{\min\max}$ procedure]
  First we show that for any given assignment of $\lambda_{i < j}$, our
  procedure fo $\beta_{\min\max}$ yields the lowest $\beta$. Then we show that
  for any other allocation of $\lambda_{i < j}$ that achieves $\gamma_\max$,
  $\beta_{\min\max}$ will increase. 

  The reason is that some mass that used to be capable of staying on the
  diagonal, $i < i^*$, is now increasing, so it can no longer stay on the
  diagonal. In the best case, it frees an equal amount of new mass to stay on
  the diagonal; in the worst case, $\epsilon$ mass is removed from the diagonal
  and necessarily added to $\beta$, because $\gamma$ is already at its max
  value.
\end{proof}

\begin{proof}[Proof of $\nu_\max$ procedure]
\end{proof}

\begin{proof}[Proof of $\nu_\min$ procedure]
\end{proof}
\end{document}
