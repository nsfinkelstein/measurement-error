\documentclass[letterpaper]{article}

% \input{includes.tex}
\usepackage[margin=1in]{geometry}

\usepackage{subcaption}
\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{amsmath}       % hyperlinks
\usepackage{amsthm}       % hyperlinks
\usepackage{todo}
\usepackage{amssymb}       % hyperlinks
\usepackage{hyperref}       % hyperlinks
\usepackage{tikz}
\usepackage{tabu}
\usepackage{bbm}
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{subfig}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{microtype}      % microtypography

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{remark}{Remark}
\newtheorem{corollary}{Corollary}
\newtheorem{proposition}{Proposition}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}

\title{A Graphical Approach to Non-Parametric Bounds in the Presence of
  Measurement Error}

\begin{document}
\maketitle

\begin{abstract}
  In many domains, data collection can be inexact. Discrepancies between the
  true value of interest and the observed value are said to be due to
  ``measurement error''. Graphical models are widely used in causal inference to
  represent causal relationships between observed variables, but they cannot
  accommodate the existence of measurement error. In this work, we take a causal
  view of the measurement error problem, and introduce causal graphs that allow
  for the possibility that certain variables are inexactly measured. This view
  of measurement error allows us to develop new graphical methods, based in part
  on existing results from causal inference, to provide non-parametric bounds on
  the true value of interest. We also provide adaptations of these methods that
  accommodate common assumptions in the measurement error literature that posit
  limits on the degree or nature of the measurement error.
\end{abstract}

\section{Introduction}

Lots of data has measurement error. Usually we handle this by making very strong
assumptions about the measurement error itself, or by making strong parametric
assumptions about the underlying data generating process. We probably don't want
to do that.

In general, causal graphs are used to make inference about observed variables or
partially observed variables (i.e. variables with missingness). In the context
of measurement error, we are interested in making inference about a variable
that is unobserved, but for which we have an observed proxy. This requires a
shift in how we think about these graphs.

We begin by deriving non-parametric bounds on functions of the distribution of
the true random variable to the extent possible. We then evaluate which
additional bounds can be inferred in the presence of common assumptions on the
nature of the connection between the true variable and its proxy. In particular,
we explore assumptions about the fidelity of the proxy, which we express as an
assumed upper bound on the conditional entropy of the true variable given the
proxy, and about the direction of measurement error. More references.

\section{Background on Causal Graphical Models}

\begin{figure}[h]
  \centering
  \begin{tikzpicture}[>=stealth, node distance=1.0cm]
    \tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
    \tikzstyle{edge} = [->, blue, very thick]

    \begin{scope}
      \node[vertex, circle] (a) {$A$};
      \node[vertex, gray, circle] (x) [right of=a] {$X$};
      \node[vertex, circle] (y) [right of=x] {$Y$};
      \node[vertex, gray, circle] (h) [above of=x, xshift=0.5cm, yshift=-0.4cm] {$H$};

      \draw[edge] (a) to (x);
      \draw[edge] (x) to (y);
      \draw[edge] (h) to (x);
      \draw[edge] (h) to (y);    

      \node[below of=x, yshift=0.4cm] (l) {$(a)$};
    \end{scope}

    \begin{scope}[xshift=4.0cm]
      \node[vertex, circle] (a) {$A$};
      \node[vertex, gray, circle] (x) [right of=a] {$X$};
      \node[vertex, circle] (y) [right of=x] {$Y$};

      \draw[edge] (a) to (x);
      \draw[edge] (x) to (y);
      \draw[edge, red, <->] (x) [bend left=50] to (y);

      \node[below of=x, yshift=0.4cm] (l) {$(b)$};
    \end{scope}

    \begin{scope}[xshift=8.0cm]
      \node[vertex, circle] (a) {$A$};
      \node[vertex, gray, circle] (x) [right of=a] {$X$};
      \node[vertex, circle] (y) [right of=x] {$Y$};

      \draw[edge] (a) to (x);
      \draw[edge] (x) to (y);

      \node[below of=x, yshift=0.4cm] (l) {$(c)$};
    \end{scope}

    \begin{scope}[xshift=12.0cm]
      \node[vertex, circle] (a) {$A$};
      \node[vertex, circle] (y) [right of=a] {$Y$};

      \draw[edge] (a) to (y);

      \node[below of=a, yshift=0.4cm, xshift=0.5cm] (l) {$(d)$};
    \end{scope}
  \end{tikzpicture}
  \caption{In these graphs, gray nodes represent unobserved variables. We let
    $A$ be an observed treatment, $H$ be an unobserved confounder, $X$ be the
    true variable of interest, and $Y$ be the observed proxy. (a) represents a
    model where the true variable and its proxy have a shared common cause, and
    (b) is the latent projection of (a) onto $A, X, Y$. (c) represents a model
    where the true variable and its proxy share no common cause. (d) represents
    the shared latent projection of (a) and (c) onto the observed variables.}
  \label{fig:latent-projection}
\end{figure}

Directed Acyclic Graphs (DAGs) are commonly used in causal inference to
represent the causal relations between variables in a system, with a directed
edge $A \rightarrow Y$ indicating that $A$ is a cause of $Y$. In early
approaches to causal inference using DAGs \cite{pearl2009causality}, it was
assumed that every variable in the system is either fully observed, or fully
unobserved. More recent work has developed a graphical approach to missing data
\cite{mohan2013missing}, in which variables are permitted to be partially
observed. If a variable is unobserved, an Acyclic Directed Mixed Graph (ADMG)
can be constructed from the DAG through the \textit{latent projection} operation
\cite{richardson2017nested}.

The latent projection of a DAG $\mathcal G$ onto a set of vertices $\bf V$
maintains all directed edges between variables in $\bf V$, replaces any directed
paths between $X, Y \in \bf V$ exclusively through variables not in $\bf V$ with
a directed edge $X \rightarrow Y$, and adds a bidirected edge between observed
vertices that share an unobserved common cause. Fig.\ref{fig:latent-projection}
demonstrates the latent projection operation.

In the context of measurement error, we may view the true variable of interest
as wholly unobserved. Instead, we observe a proxy variable, which is related to
the true variable by two foundational assumptions. These are that $(i)$ there
exists a directed edge from the true variable to the proxy, and $(ii)$ the true
variable has known domain. We are interested in making inferences about the true
variable, which is wholly unobserved.

In Fig.\ref{fig:latent-projection}, we view $A$ as an observed treatment, $X$ as
the unobserved true variable of interest, and $Y$ as the observed proxy. In some
scenarios there may be an unobserved common cause of $X$ and $Y$, represented by
Fig.\ref{fig:latent-projection}(a), and in other scenarios there may not be
(e.g. when measurement error is random), represented by
Fig.\ref{fig:latent-projection}(c). We will see that different bounds on the
distribution of $X$ can be derived under these two scenarios. However, they
share the latent projection onto observed variables represented by
Fig.\ref{fig:latent-projection}(d).

Because our goal is to make inference about unobserved variables for which we
have noisy proxies, these must remain in our graphs. We cannot adopt the typical
practice of projecting out all unobserved random variables; instead we project
out all unobserved random variables that we are not interested in making
inferences about. This work is organized as follows. In Section
\ref{sec:non-param}, we present graphical methods for obtaining non-parametric
bounds on several quantities of interest for unobserved variables with observed
noisy proxies. In Section \ref{sec:assumptions}, we introduce bounds that can be
obtained in the presence of additional assumptions.

\section{Non-Parametric Bounds}
\label{sec:non-param}


\subsection{Setting}
Assume that we are interested in estimating the marginal distribution of a
binary variable $X$; however, we cannot directly observe $X$ and instead observe
a noisy binary proxy $Y$.

For notational simplicity, let $\pi_x = P(X=x)$ be the marginal distribution of
interest, let $q_{y|x} = P(Y=y | X=x)$ be the unknown noise distribution, and
let $p_y = P(Y=y)$ be the distribution of the noisy proxy which is identifiable
from the observed data.

Following Rothman et al. (....), if we assume that the noise distribution $q$ is
known and not degenerate, then $\pi$ can be recovered from $p$ using and matrix
adjustment method. Specifically,

\begin{align}
    \label{eq:marg}
    p &= q \pi
\end{align}

so as long as $q$ is invertible, which in this case simply means that $q_{y|0}
\neq q_{y|1}$ or $Y \not\perp X$, then $\pi$ can be recovered from $p$ and $\pi$
as

\begin{align}
    \pi &= q^{-1} p.
\end{align}

In general, $q$ is not identifiable without assumptions or additional
observations and enumerating such assumptions makes up the bulk of research in
measurement error methodologies. Even in situations where $q$ is not fully
identifiable, it may still be valuable to bound $q$, allowing for various forms
of sensitivity analysis.

In this text, we derive bounds on the $q$ which rely on the presence of an
additional set of features which are conditionally independent of $Y$.
Specifically, assume that in addition to $Y$, we observe a set of discrete
features $A$ such that $A \perp Y | X$. Then, with out making other assumptions,
we can have the following bounds:

\subsection{Probability bounds}

\subsubsection{The Binary Markov Chain Scenario}

\begin{figure}[t]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_1.pdf}
		\caption{}
	\end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_2.pdf}
		\caption{}
    \end{subfigure}
    \caption{Probability bounds on the noise distribution. Gray regions are
      allowed under the bounds.}
    \label{fig:prob_bounds}
\end{figure}

With taking advantage of $A$, we have the following constraints on $q$ which are
derived from Equation \ref{eq:marg} and the constraint that $p_x \in
[0,1]\,\,\forall x$:

\begin{align*}
    &\text{If $q_{1|0} \leq q_{1|1}$:} & q_{1|0} \leq p_1\\
    && p_{1|1} \geq p_1\\
    &\text{If $q_{1|0} > q_{1|1}$:} & q_{1|0} \geq p_1\\
    && q_{1|1} \leq p_1\\
\end{align*}

Additionally, similar bounds must hold when conditioning on $A$. That is, for
all values $a$ we have

\begin{align*}
    &\text{If $q_{1|0} \leq q_{1|1}$:} & q_{1|0} \leq p_{1|a}\\
    && q_{1|1} \geq p_{1|a}\\
    &\text{If $q_{1|0} > q_{1|1}$:} & q_{1|0} \geq p_{1|a}\\
    && q_{1|1} \leq p_{1|a}\\
\end{align*}

where $p_{y|a} = P(Y=y|A=a)$. In particular, because these bounds must hold for
all values of $a$, we have

\begin{align*}
    &\text{If $q_{1|0} \leq q_{1|1}$:} & q_{1|0} \leq \min_a p_{1|a}\\
    && q_{1|1} \geq \max_a p_{1|a}\\
    &\text{If $q_{1|0} > q_{1|1}$:} & q_{1|0} \geq \max_a p_{1|a}\\
    && q_{1|1} \leq \min_a p_{1|a}\\
\end{align*}

Intuitively, these bounds are most useful when there are low levels of
observation error and there is a region of the feature space where $A$ is very
predictive of $X$.

\subsubsection{Probability Bounds through Generalized IV Inequalities}

Proposition 7 and Corollary 4 in the UAI draft present constraints on sums of
functions of the observed data law. When all variables involved in these
functionals are observed, these results present constraints that can falsify the
causal model. When some variables are not observed, they present bounds on
densities involving the unobserved variable of interest.

Let's take a look at the generalized IV inequalities in the classic IV case
(i.e. the non-generalized case, in which they were first derived using linear
programming methods (i.e. the non-generalized case, in which they were first
derived using linear programming methods (i.e. the non-generalized case, in
which they were first derived using linear programming methods (i.e. the
non-generalized case, in which they were first derived using linear programming
methods (i.e. the non-generalized case, in which they were first derived using
linear programming methods (i.e. the non-generalized case, in which they were
first derived using linear programming methods (i.e. the non-generalized case,
in which they were first derived using linear programming methods (i.e. the
non-generalized case, in which they were first derived using linear programming methods)))))))). In the classic IV case, these
inequalities were derived through linear programming techniques.

\subsubsection{Related Probability Bounds}

We can also obtain more direct bounds using Proposition 7 in other ways, I think.

\subsection{Entropic Bounds}

\subsubsection{Data Processing Inequality (No Measurement Confounding)}

\begin{figure}[t]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_3.pdf}
		\caption{}
	\end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_4.pdf}
		\caption{}
    \end{subfigure}
    \caption{Entropy bounds on the noise distribution. Gray regions are allowed
      under the bounds. (a) shows bounds based on a straightforward application
      of the data processing inequality and (b) shows the intersection of DPI
      bounds when conditioning on each possible value $a$ of
      $A$.}
    \label{fig:prob_bounds}
\end{figure}

We can derive similar bounds based on the mutual information between $A$ and
$Y$. Specifically, by the data processing inequality (DPI), we have

\begin{align}
    \label{eq:dpi}
    H(Y|A) &\geq H(Y|X)\\
    &= \sum_x \pi_x \sum_y q_{y|x} \log q_{y|x}
\end{align}

where $H(Y|A)$ is the conditional entropy of $Y$ given $A$ defined for discrete
variables as $-E[\log P(Y|A)]$. This bound can be rewritten entirely in terms of
$q$ and identifiable quantities using $\pi_1 = \frac{p_1 - q_{1|0}}{q_{1|1} -
  q_{1|0}}$. As in the previous section, this bound is most useful when $A$
contains lots of information about $X$ and there is little measurement error
(i.e. when $H(Y|A)$ is low).

These bounds are applicable if there is no confounding between the true variable
and the proxy, which will be the case if the measurement error is random (or
conditionally random). If there is such confounding, the data processing
inequality will not apply. This may happen for example, if the proxy is
``self-reported'', such that unobserved personality traits may cause both the
true variable and the observed value. In such a case, we refer to the recently
derived entropic inequalities through e-seperation constraints.

\subsubsection{Entropic Bounds Through E-Seperation}

See Wolfe et al, forthcoming (added doc with claims and proofs to the
directory). These 

\subsubsection{Entropic Bounds for Falsifying Assumptions About The Error Distribution}

Sometimes folks make what come down to assumptions about the mutual information
between the true variable and its proxy. These assumptions can be falsified
through entropic bounds. Here is an example.

\subsection{Causal Bounds}

\subsubsection{Measurement error on outcome}

Let $\bf X$ be a collection of true unobserved variables of interst, and $\bf Y$
be a corresponding collection of noisy proxies. Then let $\bf A, B$ be sets of
variables causally irrelevant to $\bf Y$ given $\bf X$ in the causal graph such
that $\bf P(Y(a)), P(Y(b))$ are identified.

From the exclusion restriction, it follows that: 

\[\bf Y(a) \neq Y(b) \implies X(a) \neq X(b)\]

Therefore the following constraint is implied by the causal model:

\[P({\bf Y(a) \neq Y(b)}) \le P({\bf X(a) \neq X(b)})\]

Then we note that:

\[\sum_{\bf y \neq y'}P({\bf Y(a) = y, Y(b) = y'}) = P({\bf Y(a) \neq Y(b)})\]

Using the lower bound on probabilities of non-identified cross-world events
$P(\cdot)$ involving observed variables from Proposition 6 of the UAI draft,
represented as $\xi(P(\cdot))$ we therefore have the following bound:

\[\sum_{\bf y \neq y'} \xi\big(P({\bf Y(a) = y, Y(b) = y'})\big) \le P({\bf X(a) \neq X(b)})\]\]

If $\bf B$ is taken to be the empty set, this is a lower bound on the proportion
of the population for whom the true variable $\bf X$ would change under
intervention $\bf A = a$. Probably there are some other interesting
interpretations under other circumstances.

\subsubsection*{Adding Monotonicty-Style Assumptions for Binary Variables}

Assume $\bf X$ and $\bf Y$ contain only one binary variable each. We are
interested in $E[X(a)] - E[X(b)]$, and we make an individual-level
monotonicity assumption (quite strong, but common in some circles).
Then we can reword the average treatment effect as:

\begin{align*}
E[X(a)] - E[X(b)] &= P(X(a) = 1) - P(X(b) = 1)\\
&= P(X(a) = 1, X(b) = 1) + P(X(a) = 1, X(b) = 0) - P(X(b) = 1, X(a) = 0) - P(X(b) = 1, X(a) = 1)\\
&= P(X(a) = 1, X(b) = 1) + P(X(a) = 1, X(b) = 0) - P(X(b) = 1, X(a) = 1)\\
&= P(X(a) = 1, X(b) = 0)
\end{align*}

Where the first equality is through reverse marginalization, the second is
through the monotonicity assumption, and the third is through the narcissus
property. Now we note that under the monotonicity assumption, $P(X(a) \neq
X(b)) = P(X(a) = 1, X(b) = 0)$, enabling us to directly use the bound above to
obtain:

\[E[X(a)] - E[X(b)] \ge \xi(P(Y(a) = 1, Y(b) = 0)) + \xi(P(Y(a) = 0, Y(b) = 1))\]

Now we stack another (!!!) monotonicity assumption on the first, this time that
the only measurement error is under reporting. This means in particular that if
every subject who has $Y(b) = 1$ must also have $Y(a) = 1$, because either they
are the sort of person who says $Y = 1$ even if $X = 0$, in which case $Y$ will
always be $1$, or they have $Y(b) = 1$ because $X(b) = 1$, in which case by the
first monotonicity assumption $X(a) = 1$. This yields the following bound
(probably this is a known bound):

\[E[X(a)] - E[X(b)] \ge P(Y(a) = 1) - P(Y(b) = 1)\]

\subsubsection{Measurement Error on Treatment}

\subsection{Population Bounds}

We are interested in some variable $\tilde X$, about which we have noisy
measurements $X$. We would like to learn about distributions involving the
unobserved  $\tilde X$. We stratify the population into three groups: 

\begin{enumerate}
\item {\bf Truthers:} Always report $\tilde X$ accurately.
\item {\bf Opportunists:} Report $\tilde X$ accurately for some values of
  parents of $X$, and not for others.
\item {\bf Liars:} Never report $\tilde X$ accurately.
\end{enumerate}

In this note, we provide an upper bound on the proportion of the population made
up of truthers (names of strata to be amended).

\subsubsection*{Non-parametric Upper Bound on Proportion of Truthers}

Let $\bf A, A'$ be two sets of variables such that neither have directed paths
to $\tilde X$. The sets may be equal, and one or both may be empty. We take the
individual-level view of a DAG, such that a lack of such a directed path
indicates that there does not exist an individual in the population for whom
$\tilde X$ is a non-trivial function of $\bf A$ or $\bf A'$ (this may be stronger
than necessary). As a result, $\tilde X({\bf a}) = \tilde X({\bf a'}) = \tilde X$.

Next, we note that if we could observe both $X({\bf a})$ and $X({\bf a'})$ for
some individual, and we see that $X({\bf a}) \neq X({\bf a'})$, then we know the
individual is lying either under intervention $\bf A = a$ or $\bf A' = a'$.
This follows from the transitivity of equality. More formally:

\begin{align*}
  X({\bf a}) \neq X({\bf a'}) \land \tilde X({\bf a}) = \tilde X({\bf a'}) = \tilde X
  \implies
  X({\bf a}) \neq \tilde X({\bf a}) \lor X({\bf a'}) \neq \tilde X({\bf a'})
\end{align*}

The second condition of the conjunction in the antecedent is true by choice of
$\bf A, A'$, so for any choice satisfying the exclusion restriction defined
above, the first condition of the conjunction in the antecedent is sufficient
for the consequent, i.e. for appropriately chosen $\bf A, A'$:

\begin{align*}
  X({\bf a}) \neq X({\bf a'})
  \implies
  X({\bf a}) \neq \tilde X({\bf a}) \lor X({\bf a'}) \neq \tilde X({\bf a'})
\end{align*}

Because all individuals who experience the antecedent event also experience the
consequent event, the latter will make up a weakly larger proportion of the
population than the former (similar reasoning is used in UAI draft):

\begin{align*}
P(X({\bf a}) \neq X({\bf a'})) \le
P(X({\bf a}) \neq \tilde X({\bf a}) \lor X({\bf a'}) \neq \tilde X({\bf a'}))
\end{align*}

This tells us that the proportion on the left is a lower bound on the
proportion who have lied in at least one of the two interventions. We note
however $X({\bf a}) \neq X({\bf a'})$ and does not have an identified density.
Therefore we will seek a lower bound on this density, which will then be
transitivity become a lower bound on the proportion of people who have lied.

To obtain this bound, we proceed as follows.

\begin{align*}
  X({\bf a}) \neq X({\bf a'}) \iff &\forall x~ \neg (X({\bf a}) = x \land X({\bf a'}) = x)\\
  \iff &\neg \exists x~ (X({\bf a}) = x \land X({\bf a'}) = x)\\
  \iff &\neg \big(\bigvee_x (X({\bf a}) = x \land X({\bf a'}) = x) \big)\\
\end{align*}

Then, moving into the land of probabilities and applying the Boole-Fr{\'e}chet
inequalities, we have

\begin{align*}
  P(\neg \big(\bigvee_x (X({\bf a}) = x \land X({\bf a'}) = x) \big)) &= 1 - P\big(\bigvee_x (X({\bf a}) = x \land X({\bf a'}) = x) \big) \\
  &\ge 1 - \sum_x P(X({\bf a}) = x \land X({\bf a'}) = x)
\end{align*}

We are still not quite done - $P(X({\bf a}) = x \land X({\bf a'}) = x)$ is not
identified in general, as it is still a cross world event. However, it happens
to be a cross-world event we know how to deal with. In particular, proposition 6
of the UAI draft describes how to derive lower bounds for events of the form
$\bf X(a) = x \land  Y(b) = y$. In this case, we want upper bounds for the event
$X({\bf a}) = x \land X({\bf a'}) = x$, because to obtain a lower bound for $1 -
\sum_x P(X({\bf a}) = x \land X({\bf a'}) = x)$, we must subtract an upper bound
of each term in the sum.

Letting $\xi(E)$ by the set of lower bounds obtained by proposition 6, as in the
UAI draft, we can produce the upper bound:

\begin{align*}
  P(X({\bf a}) = x \land X({\bf a'}) = x) \le
  1 - \sum_{(x_1, x_2) \neq (x, x)} \max \xi(X({\bf a}) = x_1 \land X({\bf a'}) = x_2)
\end{align*}

In other words, one less the sum of lower bounds of densities of all other
events is a valid upper bound. Note that in general, $\xi(E)$ produces rather
complex bounds, as seen in section 6 of the UAI draft. Putting this all together, we have:

\begin{align*}
P(X({\bf a}) \neq \tilde X({\bf a}) \lor X({\bf a'}) \neq \tilde X({\bf a'})) \ge 
  1 - \sum_x \bigg(
  1 - \sum_{(x_1, x_2) \neq (x, x)} \max \xi(X({\bf a}) = x_1 \land X({\bf a'}) = x_2)
  \bigg)
\end{align*}

This provides a lower bound on the proportion of subjects who have lied under
some intervention. Because at least this proportion of subjects have lied, and
subjects who would have lied under any distribution cannot be truthers, we have
the following upper bound on the proportion of truthers. This is one minus the
lower bound on people who have lied.

\begin{align*}
P(truthers) \le
  \sum_x \bigg(
  1 - \sum_{(x_1, x_2) \neq (x, x)} \max \xi(X({\bf a}) = x_1 \land X({\bf a'}) = x_2)
  \bigg)
\end{align*}

In the UAI draft, we take advantage of additional comparisons between
interventions by partitioning the event of interest. There may be a similar
avenue we could take here.

\section{Bounds in the Presences of Assumptions}
\label{sec:assumptions}

\subsection{Assume we know $P({\bf Y = y \mid X = x})$ for some possible $x, y$ pairs}

i.e. monotonity - like assumptions

\subsection{Assume $H({\bf Y \mid X})$ is less than or equal to some constant}

i.e. lower bound on mutual information between variable and proxy (because
entropy of proxy is known from observed data).


\bibliographystyle{plain}
\bibliography{references}	

\end{document}
