\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{graphicx}
\usepackage{subcaption}

\input{includes.tex}

\title{Measurement Error Bounds}
\author{}
\date{}

\begin{document}

\maketitle

\section{Introduction}
Assume that we are interested in estimating the marginal distribution of a
binary variable $X$; however, we cannot directly observe $X$ and instead observe
a noisy binary proxy $Y$.

For notational simplicity, let $\pi_x = P(X=x)$ be the marginal distribution of
interest, let $q_{y|x} = P(Y=y | X=x)$ be the unknown noise distribution, and
let $p_y = P(Y=y)$ be the distribution of the noisy proxy which is identifiable
from the observed data.

Following Rothman et al. (....), if we assume that the noise distribution $q$ is
known and not degenerate, then $\pi$ can be recovered from $p$ using and matrix
adjustment method. Specifically,

\begin{align}
    \label{eq:marg}
    p &= q \pi
\end{align}

so as long as $q$ is invertible, which in this case simply means that $q_{y|0}
\neq q_{y|1}$ or $Y \not\perp X$, then $\pi$ can be recovered from $p$ and $\pi$
as

\begin{align}
    \pi &= q^{-1} p.
\end{align}

In general, $q$ is not identifiable without assumptions or additional
observations and enumerating such assumptions makes up the bulk of research in
measurement error methodologies. Even in situations where $q$ is not fully
identifiable, it may still be valuable to bound $q$, allowing for various forms
of sensitivity analysis.

In this text, we derive bounds on the $q$ which rely on the presence of an
additional set of features which are conditionally independent of $Y$.
Specifically, assume that in addition to $Y$, we observe a set of discrete
features $A$ such that $A \perp Y | X$. Then, with out making other assumptions,
we can have the following bounds:

\section{Sharp bounds on $p(X)$}

\begin{figure}[t]
    \centering
	\includegraphics[width=0.49\textwidth]{figures/marginal_objective.pdf}
    \caption{Example of $\pi_1$ as a function of the noise distribution $q$. This is the objective we are optimizing in Theorem \ref{thm:marg_bounds}}
    \label{fig:marg_obj}
\end{figure}

We can derive sharp bounds on $\pi = p(X)$ using a method similar in principle to that of Balke and Pearl (1993?). Specifically, we have the following

\begin{theorem}
    \label{thm:marg_bounds}
    Let $X$ and $Y$ be binary variables such that $X \not\perp Y$ and $A$ be a discrete variable such that $A \perp Y | X$.  Then we have the following sharp bounds on $\pi$:
    \begin{align}
        \pi_1 \in \left[\frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}},\frac{p_1}{\max_a p_{1|a}}\right] \cup \left[1-\frac{p_1}{\max_a p_{1|a}},1-\frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}}\right]
    \end{align}
\end{theorem}

\begin{proof}
    Following the approach in Balke and Pearl, we derive bounds on $\pi$ by translating our assumptions into constraints on $\pi$ and then find sharp upper (lower) bounds by maximizing (minimizing) $\pi_1$ subject to these constraints (an example of this objective is shown in Figure \ref{fig:marg_obj}). Based on Equation \ref{eq:marg} we have
    
    \begin{align*}
        \pi_1 = \frac{p_1 - q_{1|0}}{q_{1|1} - q_{1|0}}
    \end{align*}
    
    This function is discontinuous at $q_{1|1} = q_{1|0}$ (which is disallowed by assumption), but continuous above and below this line. To derive the bounds in Theorem \ref{thm:marg_bounds}, we take the union of the sharp bounds when $q_{1|1} > q_{1|0}$ and when $q_{1|1} < q_{1|0}$. Consider first the case when $q_{1|1} > q_{1|0}$. For each value $a$ of $A$ we have
    
    \begin{align*}
        p_{1|a} = q_{1|0}(1-\pi_{1|a}) + q_{1|1}\pi_{1|a}
    \end{align*}
    
    Combining this with Equation \ref{eq:marg}, we can find the sharp upper bound by solving the following (non-linear) optimization problem:
    
    \begin{align*}
        \max_{q_{1|1} > q_{1|0}} \quad & \frac{p_1 - q_{1|0}}{q_{1|1} - q_{1|0}}\\
        \textrm{s.t.} \quad & p_{1|a} = q_{1|0}(1-\pi_{1|a}) + q_{1|1}\pi_{1|a}\,\,\,\forall a\\
        & 0 \leq q_{1|0},q_{1|1},\pi_{1|a} \leq 1\,\,\,\forall a
    \end{align*}
    
    To solve this optimization problem, we will fix $q_{1|1}$ and optimize with respect to $q_{1|0}$ and then optimize the resulting function with respect to $q_{1|1}$. That is, let
    \begin{align*}
        g(q_{1|1}) = \max_{q_{1|0}} \quad & \frac{p_1 - q_{1|0}}{q_{1|1} - q_{1|0}}\\
        \textrm{s.t.} \quad & p_{1|a} = q_{1|0}(1-\pi_{1|a}) + q_{1|1}\pi_{1|a}\,\,\,\forall a\\
        & 0 \leq \pi_{1|a} \leq 1\,\,\,\forall a\\
        & 0 \leq q_{1|0} < q_{1|1}
    \end{align*}

    In this case, all constraints are satisfied so long as $0 \leq q_{1|0} \leq \min_a p_{1|a}$ and the maximum is achieved when $q_{1|0} = 0$. Thus, $g(q_{1|1}) = \frac{p_1}{q_{1|1}}$. Next, we solve
    
    \begin{align*}
        \max_{q_{1|1}} \quad & g(q_{1|1}) = \frac{p_1}{q_{1|1}}\\
        \textrm{s.t.} \quad & p_{1|a} = q_{1|1}\pi_{1|a}\,\,\,\forall a\\
        & 0 \leq \pi_{1|a} \leq 1\,\,\,\forall a
    \end{align*}
    
    In this case, all constraints are satisfied so long as $\max_a p_{1|a} \leq q_{1|1} \leq 1$ and the maximum value that satisfies this constraint is $\frac{p_1}{\max_a p_{1|a}}$. Applying similar reasoning to the minimization problem, we get a minimum value of $\frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}}$. Thus, when $q_{1|1} > q_{1|0}$, we have the following sharp bounds on $\pi_1$
    
    \begin{align}
        \frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}} \leq \pi_1 \leq \frac{p_1}{\max_a p_{1|a}}
    \end{align}
    
    Finally, we repeat this derivation for $q_{1|1} < q_{1|0}$ and take the union of these two sets of bounds to get the bounds in Theorem \ref{thm:marg_bounds}.
\end{proof}

\begin{remark}
    So long as the mutual information between $A$ and $Y$ is non-zero, Theorem \ref{thm:marg_bounds} bounds $\pi_1$ away from zero and one; however, when the mutual information between $A$ and $Y$ is sufficiently high, Theorem \ref{thm:marg_bounds} also bounds $\pi_1$ away from $\frac{1}{2}$. For example, consider the extreme case when $A$ uniquely determines $Y$. In this case, $\max_a p_{1|a} = 1$, $\min_a p_{1|a} = 0$, and Theorem \ref{thm:marg_bounds} gives the following bounds
    \begin{align*}
        \pi_1 \in \left\{p_1,1-p_1\right\}
    \end{align*}
    
    More generally, we can bound $\pi_1$ away from $\frac{1}{2}$ any time $2 p_1 < \max_a p_{1|a}$ or $2 p_1 > 1-\min_a p_{1|a}$.
\end{remark}

\begin{remark}
    These bounds can be generalized to continuous $A$ by substituting sup/inf for max/min and densities for masses where appropriate; however, estimating $p(Y|A)$ becomes much harder for continuous (or high-dimensional) $A$ and we leave for future work an investigation of how sensitive estimates of these bounds are to model misspecification.
\end{remark}

\subsection{Combining Theorem \ref{thm:marg_bounds} with other common assumptions}

In the presence of additional assumptions, the bounds in Theorem \ref{thm:marg_bounds} can be further refined. In this section we present a few such refinements for common measurement error assumptions.

\subsubsection{Bounded misclassification probability}
\begin{corollary}
    If, in addition to the assumptions made in Theorem \ref{thm:marg_bounds}, we assume $p(Y=1|X=0) + p(Y=0|X=1) < 1$, we have the following sharp bounds
    
    \begin{align*}
        \pi_1 \in \left[\frac{p_1 - \min_a p_{1|a}}{1 - \min_a p_{1|a}},\frac{p_1}{\max_a p_{1|a}}\right]
    \end{align*}
\end{corollary}

\subsubsection{Unidirectional errors}
\begin{corollary}
    If, in addition to the assumptions made in Theorem \ref{thm:marg_bounds}, we assume $p(Y=1|X=0) = 0$, we have the following sharp bounds
    
    \begin{align*}
        \pi_1 \in \left[p_1,\frac{p_1}{\max_a p_{1|a}}\right]
    \end{align*}
\end{corollary}

\subsubsection{Label independent noise}
\begin{corollary}
    If, in addition to the assumptions made in Theorem \ref{thm:marg_bounds}, we assume $p(Y=1|X=0) = p(Y=1 | X=1)$, we have the following sharp bounds
    
    \begin{align*}
        \pi_1 \in \left[p_1,\frac{p_1 - p^*}{1 - 2p^*}\right] \cup \left[1-\frac{p_1 - p^*}{1 - 2p^*},1-p_1\right]
    \end{align*}
    
    where $p^* = \min \left\{\min_a p_{1|a},1-\max_a p_{1|a}\right\}$.
\end{corollary}


\section{Bounds on the noise distribution}
\begin{figure}[t]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_1.pdf}
		\caption{}
	\end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_2.pdf}
		\caption{}
    \end{subfigure}
    \caption{Probability bounds on the noise distribution. Gray regions are
      allowed under the bounds.}
    \label{fig:prob_bounds}
\end{figure}

Without taking advantage of $A$, we have the following constraints on $q$ which are
derived from Equation \ref{eq:marg} and the constraint that $p_x \in
[0,1]\,\,\forall x$:

\begin{align*}
    &\text{If $q_{1|0} \leq q_{1|1}$:} & q_{1|0} \leq p_1\\
    && p_{1|1} \geq p_1\\
    &\text{If $q_{1|0} > q_{1|1}$:} & q_{1|0} \geq p_1\\
    && q_{1|1} \leq p_1\\
\end{align*}

Additionally, similar bounds must hold when conditioning on $A$. That is, for
all values $a$ we have

\begin{align*}
    &\text{If $q_{1|0} \leq q_{1|1}$:} & q_{1|0} \leq p_{1|a}\\
    && q_{1|1} \geq p_{1|a}\\
    &\text{If $q_{1|0} > q_{1|1}$:} & q_{1|0} \geq p_{1|a}\\
    && q_{1|1} \leq p_{1|a}\\
\end{align*}

where $p_{y|a} = P(Y=y|A=a)$. In particular, because these bounds must hold for
all values of $a$, we have

\begin{align*}
    &\text{If $q_{1|0} \leq q_{1|1}$:} & q_{1|0} \leq \min_a p_{1|a}\\
    && q_{1|1} \geq \max_a p_{1|a}\\
    &\text{If $q_{1|0} > q_{1|1}$:} & q_{1|0} \geq \max_a p_{1|a}\\
    && q_{1|1} \leq \min_a p_{1|a}\\
\end{align*}

Intuitively, these bounds are most useful when there are low levels of
observation error and there is a region of the feature space where $A$ is very
predictive of $X$.

\section{Conditional entropy bounds}
\begin{figure}[t]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_3.pdf}
		\caption{}
	\end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{figures/bound_4.pdf}
		\caption{}
    \end{subfigure}
    \caption{Entropy bounds on the noise distribution. Gray regions are allowed
      under the bounds. (a) shows bounds based on a straightforward application
      of the data processing inequality and (b) shows the intersection of DPI
      bounds when conditioning on each possible value $a$ of
      $A$.}
    \label{fig:prob_bounds}
\end{figure}

We can derive similar bounds based on the mutual information between $A$ and
$Y$. Specifically, by the data processing inequality (DPI), we have

\begin{align}
    \label{eq:dpi}
    H(Y|A) &\geq H(Y|X)\\
    &= \sum_x \pi_x \sum_y q_{y|x} \log q_{y|x}
\end{align}

where $H(Y|A)$ is the conditional entropy of $Y$ given $A$ defined for discrete
variables as $-E[\log P(Y|A)]$. This bound can be rewritten entirely in terms of
$q$ and identifiable quantities using $\pi_1 = \frac{p_1 - q_{1|0}}{q_{1|1} -
  q_{1|0}}$. As in the previous section, this bound is most useful when $A$
contains lots of information about $X$ and there is little measurement error
(i.e. when $H(Y|A)$ is low).

\section{Directions}
\begin{enumerate}
    \item Can Equation \ref{eq:dpi} be solved for $q_{1|0}$ as a function of
      $q_{1|1}$? This would allow easier analysis of the bound. Entropy of a
      binary variable is not invertible, but it is invertible on a restricted
      domain which I believe corresponds to the bounds in Section 2.
    \item As in the Section 2, we can apply the DPI when conditioning on a specific value, $a$, of $A$. This gives us
    \begin{align}
        H(Y|A=a) \geq H(Y|X,A=a) = \sum_x \pi_{x|a} \sum_y q_{y|x} \log q_{y|x}
    \end{align}
    which must hold for all values of $a$. Ideally, we could then rewrite this
    bound as a min/max over $a$, though in either case we should end up with a
    better bound than the one given by marginalizing over $A$ (i.e. $H(Y|A)$).
    \item Re-write the parametric assumptions from Adams et al. (2019) and
      S{\'o}lymos et al. (2012) as assumptions about the conditional entropy.
      Specifically, that there exists a set $\mathbf{A} \subseteq \mathcal{A}$
      such that $P(A \in \mathbf{A}) > 0$ and $H(X|A \in \mathbf{A}) = 0$. This
      combined with the underreporting assumption made in both papers gives
      identifiability of the error distribution $q$.
    \begin{itemize}
        \item What assumptions other than underreporting allow identifiability in this case?
    \end{itemize}
    \item There is extensive literature on bounding mutual information in noisy
      channels that could potentially be applied to this scenario, though I am
      not very familiar with this literature.
\end{enumerate}

\end{document}
