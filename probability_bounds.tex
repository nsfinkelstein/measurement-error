\documentclass{article}

\usepackage[margin=1in]{geometry}
\usepackage{tikz}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\title{Another Approach to Probability Bounds}
\author{}
\date{}

\begin{document}

\maketitle

\begin{figure}[h]
  \centering
  \begin{tikzpicture}[>=stealth, node distance=1.0cm]
    \tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
    \tikzstyle{edge} = [->, blue, very thick]

    \begin{scope}
      \node[vertex, circle] (a) {$A$};
      \node[vertex, gray, circle] (x) [right of=a] {$X$};
      \node[vertex, circle] (y) [right of=x] {$Y$};

      \draw[edge] (a) to (x);
      \draw[edge] (x) to (y);

      \node[below of=x, yshift=0.4cm] (l) {$(a)$};
    \end{scope}

    \begin{scope}[xshift=4.0cm]
      \node[vertex, circle] (a) {$A$};
      \node[vertex, gray, circle] (x) [right of=a] {$X$};
      \node[vertex, circle] (y) [right of=x] {$Y$};

      \draw[edge] (a) to (x);
      \draw[edge] (x) to (y);
      \draw[edge, red, <->] (x) [bend left=50] to (y);

      \node[below of=x, yshift=0.4cm] (l) {$(b)$};
    \end{scope}
  \end{tikzpicture}
  \caption{The unconfounded (a) and confounded (b) instrumental variable
    measurement error (IVME) models}
  \label{fig:ime}
\end{figure}

\subsection*{Setting \& Notation}

We consider two models, corresponding to measurement error and without and with
confounding between the true variable and its proxy. These models are
represented graphically be Fig.~\ref{fig:ime}(a) and Fig.~\ref{fig:ime}(b)
respectively.

Let $\phi_{axy} = P(A = a, X = x, Y = y)$ represent
probabilities of outcomes in the full data sample space, and let $\alpha_{ay} = P(A =
a, Y = y)$ represent probabilities of outcomes in the sample space of the
observed margin.

We would like to find values of $P(X = x) = \sum_{a,y} \phi_{axy}$ consistent
with the observed data under each model. Note that the unconfounded model is a
submodel of the confounded model, so values $P(X = x)$ allowed by the latter
will be allowed by the former.

\subsection*{Constraints}

We begin with trivial probability constraints:

\[\forall a, x, y ~~ 0 \le \phi_{axy} \le 1\]

Then following linear constraints are imposed by the observed data law:

\[\forall a, y ~~\sum_x \phi_{axy} = \alpha_{ay}\]

The unconfounded model imposes the following nonlinear constraints, which
correspond to conditional independence constraints:

\begin{align}
  \label{eq:a}
  &\forall a, x, y ~~ \frac{\phi_{axy}}{\sum_{a',y'} \phi_{a'xy'}} =
    \frac{\sum_{a'} \phi_{a'xy}}{\sum_{a',y'} \phi_{a'xy'}}\frac{\sum_{y'} \phi_{axy'}}{\sum_{a',y'} \phi_{a'xy'}}\\
  \nonumber
  \implies
  &\forall a, x, y ~~ \phi_{axy}\sum_{a',y'} \phi_{a'xy'} =
  (\sum_{a'} \phi_{a'xy}) (\sum_{y'} \phi_{axy'})\\
  \nonumber
  \implies
  &\forall a, x, y ~~ \phi_{axy}\sum_{a',y'} \phi_{a'xy'} -
  (\sum_{a'} \phi_{a'xy}) (\sum_{y'} \phi_{axy'}) = 0
\end{align}

The confounded model imposes additional non-linear constraints as well (cf Balke
\& Pearl).

\subsection*{Convex Subproblems}

We note that each equality constraint given by (\ref{eq:a}) can be reformulated
as the follwing pair of inequality constraints by introducing the reverse slack
variable $\psi_{axy}$:

\begin{align*}
  \psi_{axy} + \phi_{axy}\sum_{a',y'} \phi_{a'xy'} - (\sum_{a'} \phi_{a'xy}) (\sum_{y'} \phi_{axy'}) &\ge 0 \\
  \psi_{axy} &\ge 0
\end{align*}

Then let $x$ be a vector of the function arguments. We note that the function in
the first constraint has a Hessian matrix $H$ such that $x^THx \ge 0$ whenever
$\phi_{axy}\sum_{a',y'} \phi_{a'xy'} \ge (\sum_{a'} \phi_{a'xy}) (\sum_{y'}
\phi_{axy'})$, and $x^THx \le 0$ whenever $\phi_{axy}\sum_{a',y'} \phi_{a'xy'}
\le (\sum_{a'} \phi_{a'xy}) (\sum_{y'} \phi_{axy'})$.

This allows us to partition the domain of the function arguments into two
subdomains, on one of which the constraint is convex and on the other of which
it is concave. We can then perform optimization on each of those subdomains by
introducing a constraint that we are in that subdomain, which can be expressed
as a function taht is convex on the positive reals, as above. If there are $k$
constraints defined by (\ref{eq:a}), then there will be $2^k$ subdomains on
which all constraints are convex.

Thus all -- and only -- feasible values of the objective can be computed as the
result of $2^{k + 1}$ convex optimization problems. Under the assumption of
equation (1) in the other document, these are equivalent to optimization
problems that have simple analytic solutions, which will yield at most $2^k$
windows of permitted values of $P(X = x)$.

\subsection*{The Rank-1 Parameterization}

We observe that the distribution $P(A, X=x, Y)$ must have rank 1 for any $x$, as
$A \independent Y \mid X$ implies $P(A, Y \mid X = x)$ has rank 1, and
$P(A, X = x, Y)$ is just a scaled version of $P(A, Y \mid X = x)$. For
simplicity we assume for now that $A$ and $Y$ both have cardinality $n$. It
follows that each matrix $P(A, X = x, Y)$ can be represented by a single a
single vector, representing the basis for the column space, denoted

\[\Phi_x = \begin{bmatrix}
           \phi_{0x} \\
           \phi_{1x} \\
           \vdots \\
           \phi_{nx}
         \end{bmatrix}\]

in addition to a coefficient vector, representing the coefficient $\Phi_x$ is multiplied
by to obtain each column, i.e. $P(A, X=x, Y) = \Phi_x \Psi_x^T$.

       \[\Psi_x = \begin{bmatrix}1 \\\psi_{x1} \\\vdots \\\psi_{xn} \end{bmatrix} \]

Note that without loss of generality the first coefficient $\psi_{x0}$ is set to
$1$. Now we observe that the full set of constraints can be specified as follows:

\begin{align*}
  \forall a, y ~~ \sum_{x} \phi_{ax} \psi_{xy} = \alpha_{ay}\\
  \forall a, x ~~ \phi_{ax} \ge 0\\
  \forall y, x ~~ \psi_{xy} \ge 0
\end{align*}

This captures the constraints from probability theory, as $\sum_{a, y}
\alpha_{ay} = 1$, and captures the independence constraints through the
parameterization. The probability $P(X = x)$ can be specified as follows:

\[P(X = x) = \sum_{a, y} \phi_{ax} \psi_{xy}\]

This seems to correspond to an optimization problem that is bilinear in the
$\phi_{ax}$ and $\psi_{xy}$ variables. Numerical optimization (or even iterative
symoblic optimization, if the symbolic vertices for each linear program are
known) can be done by picking a set of $\phi$s, finding the best $\psi$s, etc.,
or vice-versa (starting with picking a $\psi$). However, we suspect that which
feasible region we end up in will be a function of which $\psi$ we choose at the
start. One approach we might suggest is, at each iteration, choosing ``the
farthest'' $\phi$s from known boundaries feasible regions as a initial value.
This can be done up to a number of times corresponding to the maximum number of
possible feasible regions for the problem. We suppose there exist better
approaches.

\paragraph{Question For Roy:} It seems to me this problem can be viewed as an
convex optimization problem, even when considered in all the $\psi$s and all the
$\phi$s simultaneously. These are all restricted to be positive, and the
constraints and objective all seem to be convex on positive reals. 
The whole problem then can be reformulated in the ``standard form'' for convex
optimization problems (c.f. \smaller{
  \begin{texttt}https://en.wikipedia.org/wiki/Convex\_optimization#Standard\_form\end{texttt}}).
The equality constraints can be replaced by inequality constraints through the
addition of slack variables, as:

\begin{align*}
  \forall a, y ~~ \sum_{x} \phi_{ax} \psi_{xy} - \alpha_{ay} + s_{ay} \ge 0 \\
  \forall a, y ~~ s_{ay} \ge 0\\
\end{align*}

after which it seems to be exactly in standard form.
However, it would seem we know this is not a convex problem, as the feasibility
in the binary case region is quite non-convex. Can you see what I'm missing?

\end{document}

\begin{align*}
  \forall a, y ~~ \sum_{x} \phi_{ax} \psi_{xy} = \alpha_{ay}\\
  \forall a, x ~~ \phi_{ax} \ge 0\\
  \forall y, x ~~ \psi_{xy} \ge 0
\end{align*}

This captures the constraints from probability theory, as $\sum_{a, y}
\alpha_{ay} = 1$, and captures the independence constraints through the
parameterization. The probability $P(X = x)$ can be specified as follows:

\[P(X = x) = \sum_{a, y} \phi_{ax} \psi_{xy}\]

This seems to correspond to an optimization problem that is bilinear in the
$\phi_{ax}$ and $\psi_{xy}$ variables. Numerical optimization (or even iterative
symoblic optimization, if the symbolic vertices for each linear program are
known) can be done by picking a set of $\phi$s, finding the best $\psi$s, etc.,
or vice-versa (starting with picking a $\psi$). However, we suspect that which
feasible region we end up in will be a function of which $\psi$ we choose at the
start. One approach we might suggest is, at each iteration, choosing ``the
farthest'' $\phi$s from known boundaries feasible regions as a initial value.
This can be done up to a number of times corresponding to the maximum number of
possible feasible regions for the problem. We suppose there exist better
approaches.

\paragraph{Question For Roy:} It seems to me this problem can be viewed as an
convex optimization problem, even when considered in all the $\psi$s and all the
$\phi$s simultaneously. These are all restricted to be positive, and the
constraints and objective all seem to be convex on positive reals. 
The whole problem then can be reformulated in the ``standard form'' for convex
optimization problems (c.f.\newline \smaller{
  https://en.wikipedia.org/wiki/Convex\_optimization#Standard\_form\en}).
The equality constraints can be replaced by inequality constraints through the
addition of slack variables, as:

\begin{align*}
  \forall a, y ~~ \sum_{x} \phi_{ax} \psi_{xy} - \alpha_{ay} + s_{ay} \ge 0 \\
  \forall a, y ~~ s_{ay} \ge 0\\
\end{align*}

after which it seems to be exactly in standard form.
However, it would seem we know this is not a convex problem, as that would imply that the function takes all values between the max and the min over the region specified by the constraints. Do you know what I'm missing here?

\paragraph{Response:}
TLDR: This is a case of bilinear programming (i.e. bilinear objective with bilinear constraints) which is convex neither in the objective nor the constraints and is, in general, NP-hard via a reduction from the pooling problem (see \url{https://nekst-online.nl/what-is-the-pooling-problem/}). It is possible that there is additional structure that we can take advantage of, but I'll need think about it some more. Additionally, we may be able to make some progress via convex relaxations of certain terms. Keep reading for a specific counterexample showing that the constraint set is not convex.

Consider the case where $A$, $X$, and $Y$ are all binary and the observed joint $\mathbf{\alpha} = P(Y,A)$ is

\begin{align*}
    \mathbf{\alpha} = 
    \begin{bmatrix}
        a & 0 \\
        0 & 1-a 
    \end{bmatrix}
\end{align*}

Then, we can write out the full constraint set defined above as:

\begin{align*}
    \alpha_{00} = a &= \phi_{00}\psi_{00} + \phi_{01}\psi_{10}\\
    \alpha_{01} = 0 &= \phi_{00}\psi_{01} + \phi_{01}\psi_{11}\\
    \alpha_{10} = 0 &= \phi_{10}\psi_{00} + \phi_{11}\psi_{10}\\
    \alpha_{11} = 1-a &= \phi_{10}\psi_{01} + \phi_{11}\psi_{11}
\end{align*}

Two things become clear in this example. First, $\psi_{x0} = 1$ for all $x$ implies $\phi_{10} = \phi_{11} = 0$ which cannot be true due to the last equality constraint. Second, consider two feasible solutions

\begin{align*}
    &\mathbf{\Phi} = 
    \begin{bmatrix}
        a & 0 \\
        0 & 1-a 
    \end{bmatrix}
    &\mathbf{\Psi}= 
    \begin{bmatrix}
        1 & 0 \\
        0 & 1 
    \end{bmatrix}\\
    &\mathbf{\Phi'} = 
    \begin{bmatrix}
        0 & a \\
        1-a & 0 
    \end{bmatrix}
    &\mathbf{\Psi'} = 
    \begin{bmatrix}
        0 & 1 \\
        1 & 0 
    \end{bmatrix}
\end{align*}

If the constraint set is convex, then for all $\beta \in [0,1]$ the solution
$\Phi_\beta = \beta \Phi + (1-\beta)\Phi'$ and $\Psi_\beta = \beta \Psi +
(1-\beta)\Psi'$ must be feasible; however, it is easy to verify that this is not
the case. Therefore, the feasible set is not convex.


\subsection*{Lagrangian Multiplier Optimization Based on the Rank-1 Parameterization}

Repeating the problem formulation from the section above, we have the objective
function: 

\[f({\Phi_x, \Psi_x}) = \sum_{a, y} \phi_{ax} \psi_{xy}\]

And the constraints:

\begin{align*}
  \forall a, y ~~ \sum_{x} \phi_{ax} \psi_{xy} = \alpha_{ay}\\
  \forall a, x ~~ \phi_{ax} \ge 0\\
  \forall y, x ~~ \psi_{xy} \ge 0
\end{align*}

We let $\bf \Phi, \Psi$ denote matrices made up all of the vectors $\Phi_x, \Psi_x$.
There are $n = cardinality(A) \times cardinality(Y)$ equality constraints, which
we denote $g_1({\bf \Phi, \Psi }), \cdots, g_n({\bf \Phi, \Psi})$. We let $\Lambda$ denote
a column vector of $\lambda_i$ free variables. Then we define the lagrangian as

\[
  \mathcal L({\bf \Phi, \Psi}, \Lambda)
  = f({\Phi_x, \Psi_x}) - \sum_i \lambda_i g_i({\bf \Phi, \Psi})
\]

We would like to know all the values $f(\Phi_x, \Psi_x)$ can take on the
space defined by the constraints. To this end, we note that $\mathcal L({\bf
  \Phi, \Psi}, \Lambda)$ is differentiable everywhere, and that $f(\Phi_x,
\Psi_x)$ is continuous and monotonic in all variables on ${\bf \Phi, \Psi \ge
  0}$.

\[
  \mathcal L({\bf \Phi, \Psi}, \Lambda)
  = (\sum_a \phi_{ax^*})(\sum_y \psi_{x^*y}) - \sum_{a,y} \lambda_{ay} (\sum_{x} \phi_{ax} \psi_{xy} - \alpha_{ay})
\]

Then we have the following derivatives.

\begin{align*}
  \frac{\partial \mathcal L}{\partial \lambda_{ay}}
  &= \alpha_{ay} - \sum_{x} \phi_{ax} \psi_{xy}\\
  \frac{\partial \mathcal L}{\partial \phi_{ax}}
  &= \mathbb{I}[x = x^*]\sum_y \psi_{x^*y} - \sum_y \lambda_{ay}\psi_{xy}\\
  \frac{\partial \mathcal L}{\partial \psi_{xy}}
  &= \mathbb{I}[x = x^*]\sum_a \phi_{ax^*} - \sum_a \lambda_{ay}\phi_{ay}
\end{align*}

These can be set to zero and solved, yielding all stationary points of $\mathcal
L$. The nature of these extrema can be determined using the second derivative
test for constrainted optimization (cf \newline
https://sites.math.northwestern.edu/\texttildelow clark/285/2006-07/handouts/lagrange-2deriv.pdf).
To identify which min and max points are ``pairs'', to define the ignorance
region, we procede as follows. Start with the lowest minimum. Then the very next
maximum must be paired with that minimum. Remove both from consideration. Then
start with the next minimum, etc.

\end{document}
