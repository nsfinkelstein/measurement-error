\documentclass{article}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subfiles}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage{tikz}
\usepackage{centernot}
\usepackage{framed}
\usetikzlibrary{shapes,backgrounds}

\usepackage{xr}
\externaldocument{appendix}

\usepackage{algorithm}
\usepackage{algorithmicx}

\usepackage{amsthm}       % hyperlinks
\newcommand\independent{\protect\mathpalette{\protect\independenT}{\perp}}
\def\independenT#1#2{\mathrel{\rlap{$#1#2$}\mkern2mu{#1#2}}}

\newtheorem{conjecture}{Conjecture}
\newtheorem{proposition}{Proposition}
\newtheorem*{remark}{Remark}

\title{Converting graphs to constraints via SEMs}
\author{}
\date{}

\begin{document}

\maketitle

\section{Graphical constraints}

Assume a Bayes net $\mathcal{G} = (\mathcal{V},\mathcal{E})$ over a set of discrete random variables $\mathcal{V}$. Let $\mathcal{V}_o$ be the set of observed variables and $\mathcal{V}_u$ be the set of unobserved variables such that $\mathcal{V}_o \cup \mathcal{V}_u = \mathcal{V}$. Denote by $\mathbf{v}$ a value for all variables in $\mathcal{V}$ with analogous definitions for $\mathbf{v}_o$ and $\mathbf{v}_u$. Finally, let $\phi \in \mathbb{R}^{|\mathcal{V}|}$ be the joint distribution over all variables in $\mathcal{V}$. We assume that the joint distribution over observed variables $\phi_{\mathbf{v}_o} = \sum_{\mathbf{v}_u} \phi_{\mathbf{v}}$ is observed. We are interested in the constraints on $\phi$ implied by $\mathcal{G}$ and the observed data. Denote by $\mathcal{I}(\mathcal{G})$ the set of local independencies defining $\mathcal{G}$, which can be defined as:
%
\begin{align}
	\mathcal{I}(\mathcal{G}) = \{V \perp Nd(V) \mid Pa(V): V \in \mathcal{V}\}
\end{align}

Then we have:

\begin{proposition}
	If, for each $V \in \mathcal{V}$, $Pa(V)$ and at least one of $V$ or $Nd(V)$ are observed, then $\mathcal{G}$ is a convex polytope.
\end{proposition}

\begin{proof}
	Each independency in $\mathcal{I}(\mathcal{G})$ can be written as a constraint on $\phi$ as $P(V,Pa(V),Nd(V))P(Pa(V)) = P(V,Pa(V))P(Nd(V),Pa(V))$. Each side of this equality constraint involves two distributions and the individual constraint is linear in $\phi$ if and only if at least one of the distributions on each side is known. This, in turn, occurs only when $Pa(V)$ and at least one of $V$ or $Nd(V)$ are observed.
\end{proof}

\begin{remark}
	The reason that this is condition is not also necessary is that, for each non-linear constraint in $\mathcal{I}(\mathcal{G})$ there may exist as set of linear constraints that imply it and thus, the feasible set may be a polytope even if individual constraints in $\mathcal{I}(\mathcal{G})$ are non-linear. For example consider $\mathcal{G} = (\mathcal{V},\mathcal{E})$ where $\mathcal{V} = \{A,B,C\}$, $\mathcal{E} = \{A \to B\}$, and $A$ is unobserved. Then $\mathcal{I}(\mathcal{G}) = \{(A \perp C), (B \perp C \mid A), (C \perp A,B)\}$. $B \perp C \mid A$ is non-linear, however, it is implied by $C \perp A,B$ and thus $\mathcal{I}(\mathcal{G})$ is a polytope.
\end{remark}
%
% Each independency in $\mathcal{I}(\mathcal{G})$ can be written as a constraint on $\phi$ as $P(V,Pa(V),Nd(V))P(Pa(V)) = P(V,Pa(V))P(Nd(V),Pa(V))$. Each side of this equality constraint is involves two distributions and the constraint if linear in $\phi$ if and only if at least one of the distribution on each side is known. This, in turn, occurs only when $Pa(V)$ and one of $V$ or $Nd(V)$ are observed. Then, we have $\mathcal{G}$ iff $\mathcal{I}(\mathcal{G})$ and $\mathcal{I}(\mathcal{G})$ is linear iff, for each $V \in \mathcal{V}$, $Pa(V)$ and one of $V$ or $Nd(V)$ are observed and thus $\mathcal{G}$ is linear iff, for each $V \in \mathcal{V}$, $Pa(V)$ and one of $V$ or $Nd(V)$ are observed.

\section{\textbf{Causal} graphical constraints}

Given a causal graph $\mathcal{C}$ over a set of discrete variables, Koller and Friedman state without citation or proof that $\mathcal{C}$ can be equivalently represented as response variable graph and the structure of $\mathcal{C}$ gives a Bayes net structure over the response variables. I am increasingly suspicious that this is true. In particular, consider the graph:
%
\begin{figure}[h]
  \centering
  \begin{tikzpicture}[>=stealth, node distance=1.0cm]
    \tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
    \tikzstyle{edge} = [->, black, very thick]
    \tikzstyle{biedge} = [<->, black, thick]

    \begin{scope}
      \node[vertex] (a) {$A$};
      \node[vertex] (b) [right of=a] {$B$};
      \node[vertex] (c) [right of=b] {$C$};
      \node[vertex] (d) [right of=c] {$D$};

      \draw[edge] (a) to (b);
      \draw[edge] (b) to (c);
      \draw[edge] (c) to (d);
	  
      \draw[biedge] (a) to [in=135,out=45] (b);
      \draw[biedge] (b) to [in=135,out=45] (c);
      \draw[biedge] (c) to [in=135,out=45] (d);

      % \node[below of=x, yshift=0.4cm] (l) {$(a)$};
    \end{scope}
  \end{tikzpicture}
\end{figure}
%
Let $R_a$, $R_b$, $R_c$, and $R_d$ be the response variables for $A$, $B$, $C$, and $D$, respectively. The graph above implies $R_a \perp R_c,R_d$, $R_b \perp R_d$, $R_c \perp R_a$, and $R_d \perp R_a, R_b$. This is easy to encode using bidirected edges, but it is unclear how to encode this as a Bayes net. Maybe possible to simply read off the marginal independencies as I have done for this graph? Would need to prove that there are no other conditional independencies that are missed by this; however, if that does work, then it would be easy to determine if a graph is linear.


% If this can be proven, then we get a simple algorithm:
% %
% \begin{enumerate}
% 	\item Convert $\mathcal{C}$ into a Bayes net over response variables.
% 	\item Convert the Bayes net into a set of constraints by enumerating the local independencies. The above section tells us when these constraints are linear.
% \end{enumerate}
% %
% Unfortunately, this does not give a full characterization of the set of linear causal DAGs, because we do not have a full characterization of when the distribution of a response variable (read, potential outcome) is identified by the data. With that said, there are obviously several sufficient conditions which include the cases we consider.

% \begin{figure}[h]
%   \centering
%   \begin{tikzpicture}[>=stealth, node distance=1.0cm]
%     \tikzstyle{vertex} = [draw, thick, ellipse, minimum size=4.0mm, inner sep=1pt]
%     \tikzstyle{edge} = [->, black, very thick]
%
%     \begin{scope}
%       \node[vertex, circle] (a) {$A$};
%       \node[vertex, circle] (x) [right of=a] {$X$};
%       \node[vertex, circle] (y) [right of=x] {$Y$};
%
%       \draw[edge] (a) to (x);
%       \draw[edge] (x) to (y);
%
%       \node[below of=x, yshift=0.4cm] (l) {$(a)$};
%     \end{scope}
%
%     \begin{scope}[xshift=4.0cm]
%
% 	    \node[vertex, circle] (a) {$A$};
% 	    \node[vertex, circle] (x) [right of=a] {$X$};
% 	    \node[vertex, circle] (y) [right of=x] {$Y$};
% 	    \node[vertex, circle] (u) [above of=x, xshift=0.5cm] {$U$};
%
% 	    \draw[edge] (a) to (x);
% 	    \draw[edge] (x) to (y);
% 	    \draw[edge] (u) to (x);
% 	    \draw[edge] (u) to (y);
%
% 	    \node[below of=x, yshift=0.3cm] (l) {$(b)$};
%     \end{scope}
%
%     \begin{scope}[xshift=8.0cm]
%
% 	    \node[vertex, circle] (a) {$A$};
% 	    \node[vertex, circle] (x) [right of=a] {$X$};
% 	    \node[vertex, circle] (y) [right of=x] {$Y$};
% 	    \node[vertex, circle] (u) [above of=a, xshift=0.5cm] {$U$};
%
% 	    \draw[edge] (a) to (x);
% 	    \draw[edge] (x) to (y);
% 	    \draw[edge] (u) to (a);
% 	    \draw[edge] (u) to (x);
%
% 	    \node[below of=x, yshift=0.3cm] (l) {$(c)$};
%     \end{scope}
%
%     \begin{scope}[yshift=-3.0cm,xshift=0.0cm]
%
%     	    \node[vertex, circle] (a) {$A$};
%     	    \node[vertex, circle] (x) [right of=a] {$X$};
%     	    \node[vertex, circle] (y) [right of=x] {$Y$};
%     	    \node[vertex, circle] (u) [above of=x] {$U$};
%
%     	    \draw[edge] (a) to (x);
%     	    \draw[edge] (x) to (y);
%     	    \draw[edge] (u) to (a);
%     	    \draw[edge] (u) to (y);
%
%     	    \node[below of=x, yshift=0.3cm] (l) {$(d)$};
%     \end{scope}
%
%     \begin{scope}[yshift=-3.0cm,xshift=4.0cm]
%
% 	    \node[vertex, circle] (a) {$A$};
% 	    \node[vertex, circle] (x) [right of=a] {$X$};
% 	    \node[vertex, circle] (y) [right of=x] {$Y$};
% 	    \node[vertex, circle] (u) [above of=a, xshift=0.5cm] {$U$};
% 	    \node[vertex, circle] (z) [above of=x, xshift=0.5cm] {$Z$};
%
% 	    \draw[edge] (a) to (x);
% 	    \draw[edge] (x) to (y);
% 	    \draw[edge] (u) to (a);
% 	    \draw[edge] (u) to (x);
% 	    \draw[edge] (z) to (x);
% 	    \draw[edge] (z) to (y);
%
% 	    \node[below of=x, yshift=0.3cm] (l) {$(e)$};
%     \end{scope}
%
%     \begin{scope}[yshift=-3.0cm,xshift=8.0cm]
%
% 	    \node[vertex, circle] (a) {$A$};
% 	    \node[vertex, circle] (x) [right of=a] {$X$};
% 	    \node[vertex, circle] (y) [right of=x] {$Y$};
% 	    \node[vertex, circle] (u) [above of=x] {$U$};
%
% 	    \draw[edge] (a) to (x);
% 	    \draw[edge] (x) to (y);
% 	    \draw[edge] (u) to (a);
% 	    \draw[edge] (u) to (x);
% 	    \draw[edge] (u) to (y);
%
% 	    \node[below of=x, yshift=0.3cm] (l) {$(f)$};
%     \end{scope}
%
%   \end{tikzpicture}
%   \caption{}
%   \label{fig:graphs}
% \end{figure}
%
% \section{Constraints}
%
% After converting to a SEM, each of these graphs has three latent variables, $R_a \in \mathcal{A}$, $R_{x} \in \mathcal{X}^{|\mathcal{A}|}$, $R_{y}\in \mathcal{Y}^{|\mathcal{X}|}$, which relate to $A$, $X$, and $Y$ in the obvious way and can be equivalently interpretted as noise variables in a SEM, repsonse function variables, or potential outcomes. Then, the objective, $P(X=x)$, can be parameterized in terms of the joint over these variables as
% %
% \begin{align}
% 	P(X=x) = \sum_{r_a} \sum_{r_{x}} \sum_{r_{y}} [r_{x}(r_a) = x] P(R_a=r_a,R_{x}=r_{x},R_{y}=r_{y})
% \end{align}
% %
% and the observed data constraints can be written as
% %
% \begin{align}
% 	P(A=a,Y=y) = \sum_{r_a} \sum_{r_{x}} \sum_{r_{y}} [r_a = a][r_{y}(r_{x}(r_a)) = y] P(R_a=r_a,R_{x}=r_{x},R_{y}=r_{y}).
% \end{align}
% %
% Finally, for each graph, the independence assumptions imposed by the graph can be written as:
%
% \subsection{Fig (a)}
%
% \begin{align*}
% 	P(r_a,r_{x},r_{y}) &= P(r_a) \sum_{r_{a}',r_{y}'} P(r_a',r_{x},r_{y}') \sum_{r_{a}',r_{x}'} P(r_a',r_{x}',r_{y})
% \end{align*}
%
% \subsection{Fig (b)}
%
% \begin{align*}
% 	P(r_a,r_{x},r_{y}) &= P(r_a) \sum_{r_a'} P(r_a',r_{x}, r_{y})
% \end{align*}
%
%
% \subsection{Fig (c)}
%
% \begin{align*}
% 	P(r_a,r_{x},r_{y}) &= \sum_{r_{y}'} P(r_a,r_{x},r_{y}') \sum_{r_{a}',r_{x}'} P(r_a',r_{x}',r_y)
% \end{align*}
%
% \subsection{Fig (d)}
%
% \begin{align*}
% 	P(r_a,r_{x},r_{y}) &= \sum_{r_{x}'} P(r_a,r_{x}',r_{y}) \sum_{r_{a}',r_{y}'} P(r_a',r_{x},r_y')
% \end{align*}
%
% \subsection{Fig (e)}
%
% \begin{align*}
% 	\sum_{r_x'} P(r_a,r_x',r_y) = P(r_a) \sum_{r_a',r_x'} P(r_a',r_x',r_y)
% \end{align*}
%
% \subsection{Fig (f)}
%
% Unconstrained
%
% \section{Alg for generating a set of constraints}
%
% \begin{enumerate}
% 	\item For each variable in the graph, generate the corresponding response variable and instantiate the implied BN over these variables (this is possible according to Koller and Friedman, though they don't give an algortihm or cite a proof). Let $\mathcal{G} = (\mathcal{V},\mathcal{E})$ be this graph.
% 	\item $\mathcal{C} = \{\}$
% 	\item For $v \in \mathcal{V}$:
% 	\begin{enumerate}
% 		\item $\mathcal{U}_v = \mathcal{V} \setminus Pa(v) \cup De(v) \cup \{v\}$
% 		\item $\mathcal{C} = \mathcal{C} \cup \{v \perp \mathcal{U}_v \mid Pa(v)\}$
% 	\end{enumerate}
% 	\item Return $\mathcal{C}$
% \end{enumerate}
%
% \begin{conjecture}
% 	The set of constraints implied by $\mathcal{G}$ is linear in the unknown variables if and only if, for each independence constraint $v \perp \mathcal{U}_v \mid Pa(v) \in \mathcal{C}$, only one of $P(v)$, $P(\mathcal{U}_v)$, or $P(Pa(v))$ is not identified from the data.
% \end{conjecture}

% \begin{proof}
% 	Sufficiency: Assume wlog that $Pa(v)$ and $P(v)$ are identified, then the constraint can be written as:
% 	\begin{align}
% 		P()
		

\end{document}
