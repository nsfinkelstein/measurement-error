import sympy as sy
from itertools import product, chain


# follows technique described here:
# https://www.math.ubc.ca/~israel/m340/artif.pdf

def sympy_nonlinsolver(xvalue=0, card=2):
    # convention:
    # ϕ_ax = P(A=a, X=x, Y=0)
    # ϕ_ax * ψ_xy = P(A=a, X=x, Y=y)
    # Note this means ψ_x0 = 1

    probs = list(sy.symbols(' '.join(
        'ϕ_{}{}'.format(a, x) for x, a in product(range(card), repeat=2)
    )))

    coeffs = [
        [1] + list(
            sy.symbols(','.join('ψ_{}{}'.format(x, y) for y in range(1, card)) + ',')
        ) for x in range(card)
    ]

    rows = []
    for a, y in product(range(card), repeat=2):
        row = [
            coeffs[x][y] if a == aprime else 0
            for x, aprime in product(range(card), repeat=2)
        ]
        rows.append(row)

    A = sy.Matrix(rows)
    b = sy.Matrix(list(sy.symbols(','.join(
        'α_{}{}'.format(a, y)
        for a, y in product(range(card), repeat=2)
    ))))
    ex = A * sy.Matrix(probs) - b
    eq = [sy.simplify(sum(ex.row(i))) for i in range(ex.shape[0])]
    sy.pprint(eq)
    free_vars =  [c for c in chain.from_iterable(coeffs) if c != 1]
    sy.pprint(free_vars)
    return sy.simplify(sy.nonlinsolve(eq, free_vars))


def sympy_solver(xvalue=0, card=3):
    # convention:
    # ϕ_ax = P(A=a, X=x, Y=0)
    # ϕ_ax * ψ_xy = P(A=a, X=x, Y=y)
    # Note this means ψ_x0 = 1

    probs = sy.symbols(' '.join(
        'ϕ_{}{}'.format(a, x) for x, a in product(range(card), repeat=2)
    ))

    coeffs = [
        [1] + list(
            sy.symbols(','.join('ψ_{}{}'.format(x, y) for y in range(1, card)) + ',')
        ) for x in range(card)
    ]

    rows = []
    for a, y in product(range(card), repeat=2):
        row = [
            coeffs[x][y] if a == aprime else 0
            for x, aprime in product(range(card), repeat=2)
        ]
        rows.append(row)

    A = sy.Matrix(rows)
    b = sy.Matrix(list(sy.symbols(','.join(
        'α_{}{}'.format(a, y)
        for a, y in product(range(card), repeat=2)
    ))))
    return A * sy.Matrix(probs)
    return probs, sy.simplify(sy.linsolve((A, b), probs)), coeffs


def hessian(probs, sols, coeffs, card=2):
    coeffs = [c for c in chain.from_iterable(coeffs) if c != 1]
    obj = sy.simplify(sum(sols.args[0][:card]))
    h = sy.Matrix([[obj.diff(a).diff(b) for a in coeffs] for b in coeffs])
    return sy.simplify(h)


def jacobian(probs, sols, coeffs, card=2):
    coeffs = [c for c in chain.from_iterable(coeffs) if c != 1]
    obj = sy.simplify(sum(sols.args[0][:card]))
    h = sy.Matrix([obj.diff(a) for a in coeffs])
    return sy.simplify(h)


def zerograd(probs, sols, coeffs, card=2):
    coeffs = [c for c in chain.from_iterable(coeffs) if c != 1]
    [sy.pprint(sy.simplify(f)) for f in coeffs]
    obj = sy.simplify(sum(sols.args[0][:card]))
    print()
    sy.pprint(obj)
    print()
    j = [obj.diff(a) for a in coeffs]
    [sy.pprint(sy.simplify(f)) for f in j]
    return sy.simplify(sy.nonlinsolve(j, coeffs))


def init_matrix(xvalue=0, card=2):
    # convention:
    # ϕ_ax = P(A=a, X=x, Y=0)
    # ϕ_ax * ψ_xy = P(A=a, X=x, Y=y)
    # Note this means ψ_x0 = 1

    probs = ['ϕ_{}{}'.format(a, x) for x, a in product(range(card), repeat=2)]
    slack = ['a_{}'.format(k + 1) for k in range(card**2)]
    other = ['w', 'z', 'b']

    a_cols = [
        [1 if i == j else 0 for i in range(card**2)]
        for j in range(card**2)
    ] + [[0]*card**2, [0]*card**2]
    w_col = ([0] * (card**2)) + [1, 0]
    z_col = ([0] * (card**2)) + [0, 1]
    b_col = [
        'α_{}{}'.format(a, y)
        for a, y in product(range(card), repeat=2)
    ] + [-1, 0]

    coeffs = [
        [1] + list(
            sy.symbols(','.join('ψ_{}{}'.format(x, y) for y in range(1, card)) + ',')
        ) for x in range(card)
    ]

    columns = probs + slack + other
    rows = []
    for a, y in product(range(card), repeat=2):
        row = [
            coeffs[x][y] if a == aprime else 0
            for x, aprime in product(range(card), repeat=2)
        ]
        rows.append(row)

    # w row
    rows.append([-sum(coeffs[x]) for x, a in product(range(card), repeat=2)])

    # z row
    rows.append([
        -sum(coeffs[x]) if x == xvalue else 0
        for x, a in product(range(card), repeat=2)
    ])

    for a, b, c, d, e in zip(rows, a_cols, w_col, z_col, b_col):
        a += b + [c, d, e]

    return columns, sy.Matrix(rows)


def find_starting_vertex(cols, rows):
    pass


# def pivot(rows, objective_col, excluded_col=None):
#     pivot_cols = 
