import sympy as sy
import multiprocess as mp

from itertools import product, chain, combinations
from functools import partial


# convention:
# ϕ_ax * ψ_xy = P(A=a, X=x, Y=y)
# α_ay = P(A=a, Y=y)
# λ_ay: the lagrange multiplier for the constraint implied by α_ay
# μ_ay: the lagrange multiplier for the positivity constraint on ϕ_ax * ψ_xy

def lagrange_pieces(card=2, xvalue=0):
    # returns the objective and the equality constraints, and param symbols
    probs = [
        list(sy.symbols(' '.join('ϕ_{}{}'.format(a, x) for x in range(card))))
        for a in range(card)
    ]

    coeffs = [
        list(sy.symbols(' '.join('ψ_{}{}'.format(x, y) for y in range(card))))
        for x in range(card)
    ]

    params = list(chain(*(probs + coeffs)))

    marginals = [
        list(sy.symbols(' '.join('α_{}{}'.format(a, y) for y in range(card))))
        for a in range(card)
    ]

    objective = sum(
        probs[a][xvalue] * coeffs[xvalue][y]
        for a, y in product(range(card), repeat=2)
    )

    eq_constraints = [(
            sum(probs[a][x] * coeffs[x][y] for x in range(card))
            - marginals[a][y]
        )
        for a, y in product(range(card), repeat=2)
    ]

    # NOTE: later - add other inequality constraints e.g. monotonicity
    ineq_constraints = [-p for p in params]  # \leq 0

    eq_multipliers = list(sy.symbols(
        ' '.join('λ_{}'.format(i) for i in range(len(eq_constraints)))
    ))

    ineq_multipliers = list(sy.symbols(
        ' '.join('μ_{}'.format(i) for i in range(len(ineq_constraints)))
    ))

    nonnegative_params = params + ineq_multipliers + list(chain(*marginals))

    return {
        'params': params,
        'marginals': list(chain(*marginals)),
        'objective': objective,
        'eq_constraints': eq_constraints,
        'ineq_constraints': ineq_constraints,
        'eq_multipliers': eq_multipliers,
        'ineq_multipliers': ineq_multipliers,
        'nonnegative_params': nonnegative_params,
    }


def create_lagrangian(pieces, active_ineq_constraints):
    eq_zipped = zip(pieces['eq_multipliers'], pieces['eq_constraints'])
    eq = [m * c for m, c in eq_zipped]

    ineq_zipped = zip(pieces['ineq_multipliers'], pieces['ineq_constraints'])
    ineq = [
        m * c for i, (m, c) in enumerate(ineq_zipped)
        if i in active_ineq_constraints
    ]

    return pieces['objective'] - sum(eq + ineq), eq, ineq


def find_feasible_region(card=2, xvalue=0):
    pieces = lagrange_pieces(card, xvalue)

    solutions = {}
    for c, k in ((1, -1), ('min', 'max')):
        pieces['objective'] *= c
        solutions[k] = kkt(pieces)

    return solutions


def kkt(pieces):
    candidates = set()
    num_ineq = len(pieces['ineq_constraints'])

    # num_ineq/2 suffices due to the way these parameters are multiplied
    for i in range(0, (num_ineq // 2) + 1):

        print('Assuming {} inequality constraints are active...'.format(i))

        # TODO: many of these combinations are repetative
        # similar to IV inequalities constraints
        # don't go through all of them. there are too many.

        mapper = partial(optimal, pieces=pieces)

        with mp.Pool(mp.cpu_count()) as p:
            solutions = p.map(mapper, combinations(range(num_ineq), i))

        # solutions = list(map(mapper, combinations(range(num_ineq), i)))

        candidates |= set.union(*solutions)

    return candidates


def optimal(active, pieces):
    candidates = set()

    tsymbols = ('δ_{}'.format(i) for i in range(len(pieces['params'])))
    trajectory = sy.symbols(' '.join(tsymbols))

    lagrangian, eq, ineq = create_lagrangian(pieces, active)
    gradient = [lagrangian.diff(v) for v in pieces['params']]

    # karush-kuhn-tucker equality conditions
    # zero gradient, equality constraints hold, complementary slackness
    zipped = zip(pieces['ineq_multipliers'], pieces['ineq_constraints'])
    comp_slackness = [m * i for m, i in zipped]
    system = gradient + pieces['eq_constraints'] + comp_slackness + [p for i, p in enumerate(pieces['ineq_multipliers']) if i not in active]

    solvefor = pieces['params'] + [pieces['ineq_multipliers'][i] for i in active] + pieces['eq_multipliers']
    try:
        solutions = sy.nonlinsolve(system, solvefor)
    except:
        sy.pprint(system)
        sy.pprint(solvefor)
        sy.pprint(set.union(*[set(s.free_symbols) for s in system]))
        stop

    solutions = list(dict(zip(solvefor, s)) for s in solutions)
    print('Initial solutions')
    sy.pprint(solutions)

    # check that solutions satisfy remaining kkt conditions
    # http://www.numerical.rl.ac.uk/people/nimg/course/lectures/raphael/lectures/lec10slides.pdf

    # assumptions cannot be pickled - must be restated in each subprocess
    facts = [sy.Q.nonnegative(p) for p in pieces['nonnegative_params']]
    with sy.assuming(*facts):
        for solution in solutions:

            # check sign conditions on variables and inequality multipliers μ
            pos = pieces['ineq_constraints'] + pieces['ineq_multipliers']
            positivity_violations = (sy.ask(
                sy.Q.negative(expression.subs(solution))
            ) for expression in pos)

            if any(positivity_violations):
                print('First order violation!')
                sy.pprint(solution)
                continue

            # find trajectories into the feasible region
            system = list(chain.from_iterable(
                (
                    d * constraint.diff(v).subs(solution)
                    for d, v in zip(trajectory, pieces['params'])
                )
                for constraint in eq + ineq
            ))

            viable_trajectories = sy.nonlinsolve(system, trajectory)
            viable_trajectories = list(dict(zip(trajectory, s)) for s in viable_trajectories)
            print('Viable Trajectories')
            sy.pprint(viable_trajectories)

            trajectories = [
                sy.Matrix([v.get(t, t) for t in trajectory])
                for v in viable_trajectories
            ]

            print('Viable Trajectories Matrices')
            sy.pprint(trajectories)

            # check if the hessian is positive semi definite over these
            hessian = sy.Matrix([
                [g.diff(v).subs(solution) for v in pieces['params']]
                for g in gradient
            ])

            second_order_violations = (
                sy.ask(sy.Q.nonpositive(
                    t.T * hessian * t
                ))
                for t in trajectories
            )

            if any(second_order_violations):
                print('Second order violation!')
                sy.pprint(solution)
                continue

            print('Viable Solution')
            sy.pprint(solution)
            candidates.add(tuple(solution.items()))

    return candidates


if __name__ == '__main__':
    pieces = lagrange_pieces()
    candidates = kkt(pieces)
    sy.pprint(candidates)
