from itertools import product, combinations

import multiprocessing as mp
import sympy as sp
import numpy as np
import subprocess
# handles case of one unobserved variable with one proxy

template = """
base
H-representation
begin
{num_equations} {num_variables} rational
{equations}
end
"""


def create_linear_program(
    cardinality=2,
    monotonic=False,
    error_proportion=False,
    equal_errors=False,
    ordered_errors=False,
    error_bound=1,
    xvalue=0,
):
    # TODO: probably would run much faster if the system of equations were
    # simplified symbolically before being written to mplrs

    # think of coefficients as coeffs[i, j] = P(X = i, Y = j)
    def coeffs():
        return np.zeros((cardinality, cardinality))

    def pairs():
        return product(range(cardinality), repeat=2)

    # start with form max c^t x s.t. Ax >= b
    # where x is a vector of probabilities P(X, Y)
    b = []
    constraints = []

    # constraints from probability theory
    for i, j in pairs():
        c = coeffs()
        c[i, j] = 1
        constraints.append(c)
        b.append(0)

    marginals = sp.symbols(' '.join(
        'α_{}'.format(j) for j in range(cardinality)
    ))

    constraints.append(np.ones((cardinality, cardinality)))
    b.append(sum(marginals))

    constraints.append(-np.ones((cardinality, cardinality)))
    b.append(-sum(marginals))

    # constraints from observed data: margin of full data is observed data
    for j in range(cardinality):
        c = coeffs()
        c[:, j] = 1
        # this is an equality, so we need both inequalities
        constraints.append(c)
        b.append(marginals[j])

        constraints.append(-c)
        b.append(-marginals[j])

    # constraints from assumptions
    if monotonic:
        c = coeffs()
        for i, j in pairs():
            c[i, j] = i <= j
        constraints.append(-c)
        b.append(0)
        constraints.append(c)
        b.append(0)

    if error_proportion:
        c = coeffs()
        for i, j in pairs():
            c[i, j] = np.abs(i - j) > error_bound
        constraints.append(-c)  # minus, because this is <=
        b.append(-sp.symbols('λ'))

    if equal_errors:  # difference in error rate by direction is <= δ
        for i, j in pairs():
            if i + j >= cardinality or i - j < 0:
                break

            c = coeffs()
            c[i, i + j] = 1
            c[i, i - j] = -1

            # two constraints to constrain absolute difference
            constraints.append(-c)
            b.append(-sp.symbols('ε'))

            constraints.append(c)
            b.append(-sp.symbols('ε'))

    if ordered_errors:
        for i in range(cardinality):
            for j, k in combinations(range(cardinality), r=2):
                if np.abs(i - j) == np.abs(i - k):
                    break

                c = coeffs()
                c[i, j] = 1
                c[i, k] = -1

                direction = 1 if np.abs(i - j) < np.abs(i - k) else -1

                constraints.append(direction * c)
                b.append(0)

    A = np.array([list(c.flatten()) for c in constraints])

    # move to form y^t A <= c^t --> A^t y <= c
    A = np.transpose(A)
    A = np.vstack((A, -np.eye(A.shape[1])))  # y >= 0

    c = coeffs()

    # define objective
    if isinstance(xvalue, list):
        for xv in xvalue:
            c[xv] = 1
    else:
        c[xvalue] = 1

    c = c.reshape((-1, 1))
    c = np.vstack((c, np.zeros((A.shape[1], 1))))  # y >= 0

    min_system = np.hstack((c, -A))
    max_system = np.hstack((-c, -A))

    return b, min_system, max_system


def create_mplrs_programs(min_system, max_system):

    def program(system):
        equations = '\n'.join(' '.join(str(int(c)) for c in s) for s in system)
        return template.format(
            num_equations=system.shape[0],
            num_variables=system.shape[1],
            equations=equations,
        )

    return (program(min_system), program(max_system))


def find_vertices(programs):
    vertices = []
    for program in programs:
        # expects redund and mplrs, from lrslib, and openmpi to be on path
        with open('mplrs.ine', 'w') as f:
            f.write(program)

        # clean up redundant inequalities for efficiency
        r = subprocess.run(
            ['redund', 'mplrs.ine'], stdout=subprocess.PIPE
        ).stdout.decode()

        with open('mplrs.ine', 'w') as f:
            f.write(r)

        # run mplrs
        r = subprocess.run(
            ['mpirun', '-np', str(mp.cpu_count()), 'mplrs', 'mplrs.ine'],
            stdout=subprocess.PIPE,
        ).stdout.decode()

        # parse vertices
        v_polytope = [
            [x.strip() for x in c.split(' ') if x.strip()]
            for c in r.split('\n')
        ]
        vertices.append([c[1:] for c in v_polytope if c and c[0] == '1'])

    return [[[eval(c) for c in h] for h in v] for v in vertices]


def calculate_extrema(b, vertices):
    solutions = dict()

    for m, vertex_set in zip(('min', 'max'), vertices):
        coeff = -1 if m == 'max' else 1
        s = {
            sum(coeff * v * a for v, a in zip(vertex, b))
            for vertex in vertex_set
        }

        solutions[m] = tuple(s)

    return solutions


if __name__ == '__main__':
    # b, min_system, max_system = create_linear_program(
    #     cardinality=6,
    #     monotonic=False,
    #     error_proportion=True,
    #     equal_errors=True,
    #     ordered_errors=True,
    #     error_bound=2,
    #     xvalue=[3, 4],
    # )

    b, min_system, max_system = create_linear_program(
        cardinality=3,
        monotonic=False,
        error_proportion=True,
        equal_errors=True,
        ordered_errors=True,
        error_bound=1,
        xvalue=[0],
    )

    programs = create_mplrs_programs(min_system, max_system)
    # b, programs = create_mplrs_program()
    vertices = find_vertices(programs)
    results = calculate_extrema(b, vertices)
    sp.pprint(results)
